# -*- coding: utf-8 -*-
# author: Felix Repp felix.repp@mpikg.mpg.de
#!/usr/bin/env python
import os
import numpy as np
import pdb
import networkx
import scipy
import scipy.stats
import pandas as pd

#import tina_main
"""@package tinacell
%This module provides the tools to analyze the cells: the size, shape, orientation...
"""


class Cell():

    def __init__(self,  node, inds, w, h, l, r0, dr, order='C', offset=[0, 0, 0]):
        """unfortunately matlab uses a different indexing (use order ='F' and swap h and l
        offset to account for padding
        cell.complete is initialized as None, tinana.cell_based_analysis determines a value
        """
       
        self.complete=None
        self.node = node
        #self.idx = inds

        self.nearest_nodes = []  # 'c'+str(label)]

        self.connected_with = {}

        x_, y_, z_ = np.lib.unravel_index(inds, (w, h, l), order=order)
        if offset != [0, 0, 0]:
            x_.setflags(write=True)
            y_.setflags(write=True)
            z_.setflags(write=True)
            x_ -= offset[0]
            y_ -= offset[1]
            z_ -= offset[2]

        xmi = min(x_)
        xma = max(x_)
        ymi = min(y_)
        yma = max(y_)
        zmi = min(z_)
        zma = max(z_)
        self.spacing = dr
        CA = np.zeros([xma - xmi + 1, yma - ymi + 1,
                       zma - zmi + 1], dtype=np.uint8)
        CA[x_ - xmi, y_ - ymi, z_ - zmi] = 1
        self.data = CA
        self.origin = (np.array(r0).ravel() +
                       np.array([xmi, ymi, zmi]).ravel() * dr)
        # if label==12:
        #    pdb.set_trace()
        self.get_cell_stats()
        self.ioff = [xmi, ymi, zmi]
        if ((np.array(self.ioff) + CA.shape) > [w, h, l]).sum():
            pdb.set_trace()
        """if self.label==816:
            pdb.set_trace()
        """

    def get_cell_stats(self):
        CA = self.data
        stats = {}

        X, Y, Z = np.mgrid[0:CA.shape[0], 0:CA.shape[1], 0:CA.shape[2]]
        X = X.astype(np.float)
        Y = Y.astype(np.float)
        Z = Z.astype(np.float)

        X *= self.spacing[0]
        X += self.origin[0]
        Y *= self.spacing[1]
        Y += self.origin[1]
        Z *= self.spacing[2]
        Z += self.origin[2]
        M = float(np.sum(CA))
        Vol = M * self.spacing[0] * self.spacing[1] * self.spacing[2]
        # pdb.set_trace()
        xm = (CA * X).sum() / M
        ym = (CA * Y).sum() / M
        zm = (CA * Z).sum() / M
        x0 = (X - xm).ravel()
        y0 = (Y - ym).ravel()
        z0 = (Z - zm).ravel()
        rr = [x0, y0, z0]
        CC = np.zeros([3, 3])
        # pdb.set_trace()
        for m in range(3):
            for n in range(3):
                CC[m, n] = np.dot(CA.ravel() * rr[m], rr[n])

        CC /= M
        E = np.linalg.eig(CC)
        E = [E[0].tolist(), E[1].T.tolist()]
        EE = zip(*sorted(zip(*E), key=lambda pair: pair[0]))

        E1 = list(EE[1])
        E0 = list(EE[0])
        e1, e2, e3 = np.sqrt(E0)
        E1.reverse()
        E0.reverse()
        E[0] = np.array(E0)
        E[1] = np.array(E1)

        stats['Volume'] = Vol
        stats['cm'] = np.array([xm, ym, zm]).ravel()
        stats['eigen'] = E
        # pdb.set_trace()
        stats['skew'] = scipy.stats.skew(np.sqrt(E[0]))
        stats['stretch'] = (e3-e1)/e3

        stats['oblateness'] = (2 * (e2 - e1) / (e3 - e1) - 1)
        pil = np.arccos(np.dot(stats['eigen'][1][0], [0, 0, 1]))
        stats['angle_l_z'] = pil
        self.stats = stats

    """def show_axis(self):
        if self.stats['skew']<0:
            c=(1,0,0)
            E=self.stats['eigen']
            uv=(self.stats['cm']-np.sqrt(E[0][0])*E[1][0]).tolist()+(2*np.sqrt(E[0][0])*E[1][0]).tolist()
        else:
            c=(0,0,1)
            E=self.stats['eigen']
            uv=(self.stats['cm']-np.sqrt(E[0][-1])*E[1][-1]).tolist()+(2*np.sqrt(E[0][-1])*E[1][-1]).tolist()
        pdb.set_trace()
        mlab.quiver3d(*uv,color=c)
    """


def export_celldict(celldict, folder, fname):
    allcells = {}
    for id, cell in celldict.iteritems():
        pdb.set_trace()
        series = {}
        series['degree'] = cell.degree
        series['complete'] = cell.complete
        series['x'] = cell.stats['cm'][0]
        series['y'] = cell.stats['cm'][1]
        series['z'] = cell.stats['cm'][2]
        series['eigen0'] = cell.stats['eigen'][0][0]
        series['eigen1'] = cell.stats['eigen'][0][1]
        series['eigen2'] = cell.stats['eigen'][0][2]
        for k in cell.stats.keys():
            if not (type(cell.stats[k]) == list or np.size(cell.stats[k]) > 1):
                series[k] = cell.stats[k]
        series = pd.Series(series)
        allcells[id] = series
    allcs = pd.DataFrame(allcells)
    allcs.to_csv(os.path.join(folder, fname))
