import sys
import os

sys.path.append(os.path.abspath('../source'))

import tinaui
import tinanet
import os

folder = '../test/sampledata/ocn'
t1 = tinaui.load_status(folder, load_imagedata=True)
t1.networkdict['filtered'] = t1.networkdict['initial'].copy()
tinanet.filter_short_branches(
    t1.networkdict['filtered'], shortedge=1.5)  # prun the network
# this is nessecarry as otherwise the orientation would be only
# combination of $e_x$,$e_y$,$e_z$
tinaui.replaceEdgesBySplines(t1, name='initial', copy='smooth', sf=.035)
tinanet.project_orientation(t1.networkdict['smooth'], [
                            0, 1, 0], vec2=[0, 0, 1],project=True)
# reduce alpha to one [0,pi/2] for a nicer visualization
tinanet.reduce_alpha(t1.networkdict['smooth'])
tinaui.plot_graph(t1, 'smooth', edge_color_keyword='theta')
tinanet.get_cylindrical_projection(t1.networkdict['smooth'], [
                                   [50, 50, 930], [50, 50, 960]],project=True)
tinanet.reduce_alpha(t1.networkdict['smooth'])
tinaui.plot_graph(t1, 'smooth', edge_color_keyword='alpha')
