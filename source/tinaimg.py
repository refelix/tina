# -*- coding: utf-8 -*-
# author: Felix Repp felix.repp@mpikg.mpg.de
# !/usr/bin/env python
import os
import numpy as np
import pdb
import tkFileDialog
import tina_main
import scipy
import matplotlib.pyplot as plt
from scipy.ndimage import filters as fi
from scipy.ndimage import morphology as morph
from scipy.ndimage import label
import scipy.ndimage as nd
from scipy import misc

"""@package tinaimg
\brief %This module provides the tools to load and save Image data
"""


def save_stack(stack, folder, name, ftype='png'):
    """saves all image slices of a 3d array as images.
    @param stack 3d array
    @param folder where to save the images. if parent folder exists, creates the new directory
    @param name of the images followed by a 4 digit number
    @param ftype typically 'png', 'tif'
    """
    if not os.path.exists(folder):
        os.mkdir(folder)

    for i in range(stack.shape[2]):
        misc.imsave(os.path.join(folder, name +
                                 '{:04g}.'.format(i) + ftype), stack[:, :, i])


def load_stack(folder=None, ftype='tif', nfilter=None, extent=None, treat_color=None, dtype='uint8'):
    """ %This function loads a dataseries and return is as numpy array
    there might be problems arising with import of tifs with 16bit

    @param    folder in which folder are the images located
    @param    ftype file type of the image 'tif','png' load individual images applying the nfilter
    @param    nfilter string or list of strings that needs to be in filename
    @param    treat_color: while gray value images are preferable color images can be used a value of 0 results in the usage of mean of all color channels, 1 uses the red, 2 the green and 3 the blue channel
    @param    dtype typically images are loaded as 'uint8', this however can result in unpredictable results after manipulating the image. if memory usage is an issue 'uint8' should be used.

    @retval   array
    """
    from scipy import misc
    if folder is None:
        cur = os.getcwd()
        folder = tkFileDialog.askdirectory(
            initialdir=cur, title='choose folder where images are located')

    files = (os.listdir(folder))
    files.sort()
    print 'loading'
    stackfiles = []
    for ffile in files:
        if ftype is None or ftype == ffile[-len(ftype):]:
            if nfilter is None:
                stackfiles.append(ffile)
            elif isinstance(nfilter, str) and nfilter in ffile:
                stackfiles.append(ffile)
            elif isinstance(nfilter, list):
                s = 0
                for i in nfilter:
                    if i in ffile:
                        s += 1
                if s == len(nfilter):
                    stackfiles.append(ffile)

            else:
                continue
            #    print "something is going wrong with filtering filenames"
            #    pdb.set_trace()
    if len(stackfiles) == 0:
        print "no files found, check ftype and nfilter"
        pdb.set_trace()
    else:
        if extent not in [None, 'None']:
            stackfiles = stackfiles[extent[4]:extent[5]]

        for i, ffile in enumerate(stackfiles):
            im = misc.imread(os.path.join(folder, ffile))
            if len(im.shape) == 3:
                if i == 0 and treat_color is None:
                    print "color image, what do you want to do"
                    while True:
                        treat_color = raw_input(
                            '0 results in the usage of mean of all color channels, 1 uses the red, 2 the green and 3 the blue channel')
                        if treat_color in range(4):
                            break
                if treat_color == 0:
                    im = im.mean(axis=-1)
                else:
                    im = im[:, :, treat_color - 1]
            if i == 0:

                if extent not in [None, 'None']:
                    dat = np.zeros(
                        (len(stackfiles), extent[1] - extent[0], extent[3] - extent[2]), dtype=dtype)

                else:
                    dat = np.zeros((len(stackfiles), im.shape[
                                   0], im.shape[1]), dtype=dtype)
                    extent = [0, im.shape[0], 0,
                              im.shape[1], 0, len(stackfiles)]

                # if im.dtype == np.uint16:#m.mode in ['I;16','I;16B']:
                #    pdb.set_trace()

            dat[i, :, :] = im[extent[0]:extent[1], extent[2]:extent[3]]
        print 'done'
        dat = dat.swapaxes(0, 1)
        dat = dat.swapaxes(1, 2)
        return dat


def load_3dtif(ffile=None, extent=None, treat_color=None):
    """not yet implementet
    """
    if ffile is None:
        cur = os.getcwd()
        ffile = tkFileDialog.askfile(
            initialdir=cur, title='choose image stack')
    print 'not yet implementet'


def segment_cells(bin_img):
    """
    This will be the cell segmentation
    """
    pass


def dog(img, s1, s2, thr, ratio=False, retsmooth=False):
    """@param s1 smaller smoorhing radius
    @param s2 bigger smoorhing radius
    @param thr threshold
    @param ratio if True, use the ratio instead of the difference
    @param retsmooth if True, the smoothed images will be returne
    @retval bin thresholded image
    @retval cs1 image smoothend with s1
    @retval cs2 image smoothend with s2
    @retval params parameters

    thresholds the difference of gaussians
    """
    raw = img.astype(np.double)
    cs1 = fi.gaussian_filter(raw, s1)  # just noise reduction
    cs2 = fi.gaussian_filter(raw, s2)  # neighbourhood of canaliculi

    # now threshold
    if ratio:
        bin = (cs1 / cs2) > thr

    else:
        bin = (cs1 - cs2) > thr
    params = {'s1': s1, 's2': s2, 'thr': thr, 'ratio': ratio}
    if retsmooth:
        return bin, cs1, cs2, params
    else:
        return bin, params


def fill_cells(bin, closing_iterations=1, thin_section=True, thin_axis=2):
    """
    closes empty spaces, after morphologically dilating the image, trying to fill cells. as typically many cells are cut at the boundary of the image volume, the volume can be sealed in one direction. Typically the volume is thinner in the imaging axis, which for our data is the last

    @param bin binary image
    @param closing_iterations perform n times dilation before filling and erosion afterwards
    @thin_section if cells are cut, filling won't succeed. If one dimesion is much thinner than the
    other two the image can be "sealed" to fill most of the cut cells, if set True.
    @param  thin_axis which axis is the smallest for sealing
    @retval bin binary image with holes (cells) filled
    @retval params used parameters

    uses morph.binary_fill_holes(bin) to fill cells. Some more steps are taken care of
    """

    if closing_iterations:  # perform dilation, erosion happens at the end
        binc = bin.copy()  # first, make a copy
        bin = morph.binary_dilation(bin, iterations=closing_iterations)

    if thin_section:  # "seal" the image: to do so, pad first
        if thin_axis == 0:
            bin = np.pad(bin, ((1, 1), (0, 0), (0, 0)), mode='constant')
            bin[[0, -1], :, :] = 1
        if thin_axis == 1:
            bin = np.pad(bin, ((0, 0), (1, 1), (0, 0)), mode='constant')
            bin[:, [0, -1], :] = 1
        if thin_axis == 2:
            bin = np.pad(bin, ((0, 0), (0, 0), (1, 1)), mode='constant')
            bin[:, :, [0, -1]] = 1
    # you guessed it, right?
    bin = morph.binary_fill_holes(bin)

    # take away the "seal"
    if thin_section:
        if thin_axis == 0:
            bin = bin[1:-1, :, :]
        if thin_axis == 1:
            bin = bin[:, 1:-1, :]
        if thin_axis == 2:
            bin = bin[:, :, 1:-1]

    if closing_iterations:

        bin = morph.binary_erosion(bin, iterations=closing_iterations)
        # as this removes all voxel on the outer boundary, replave it by copy
        binc[closing_iterations:-closing_iterations - 1, closing_iterations:-closing_iterations - 1, closing_iterations:-closing_iterations -
             1] = bin[closing_iterations:-closing_iterations - 1, closing_iterations:-closing_iterations - 1, closing_iterations:-closing_iterations - 1]
        bin = binc
    params = {'closing_iterations': closing_iterations,
              'thinsection': thin_section, 'thin_axis': thin_axis}
    return bin, params


def remove_network_blobs(bince, cs1, cs2, s2, remove_thr, erosion=3, dilation=2, minsize=150, plot=False):
    """This function can be used if cells can not be thresholded independent from the canaliculi.
    if cells can not be separated from canaliculi, this function tries to remove broadend canaliculi
    @param bince this is a binary image that schould contain the cells
    @param cs1 the slightly smoothened image from the DOG to threshold the canaliculi
    @param cs2 the more smoothened image from the DOG to threshold the canaliculi
    @param s2 the smoothing parameter used to create cs2 that create the mask to remove structures
    @param remove_thr set threshold of what will be removed, a smaller threshold will remove more
    @param erosion erode after removing
    @param dilation dilation after erosion
    @param minsize noise smaller than this size will be removed
    """
    if cs1.shape != bince.shape:
        print 'shapes dont match, make sure to use same extends in both DOG methods'
    if plot:
        fig, axs = plt.subplots(2, 2, figsize=[16, 16])

    # morph.binary_erosion(bince,iterations=first_erode)#make everything a bit thinner
    # if ratio:
    # < the canaliculli+ the minimum around them
    canaliculi = fi.gaussian_filter(abs(cs1 / cs2 - 1), s2) > remove_thr
    # else:this seems to work much worse
    #    canaliculi=fi.gaussian_filter(abs(cs1-cs2),s2)>remove_thr
    if plot:
        axs[0, 0].imshow(canaliculi.sum(axis=2))
        axs[0, 0].set_title('from network vary remove_thr,')
    # further erosion
    # plot, for the sake of this notebook

    cells = bince * (canaliculi == False)
    if plot:
        axs[0, 1].imshow(cells.sum(axis=2))
        axs[0, 1].set_title('combined')

    del canaliculi

    cells = morph.binary_erosion(cells, iterations=erosion)
    # make cells bigger again

    # remove small patches in cells
    cells = remove_small(cells, minsize)

    if plot:
        axs[1, 0].imshow(cells.sum(axis=2))
        axs[1, 0].set_title('eroded,before cleaning')

    cells = morph.binary_dilation(cells, iterations=dilation)
    if plot:
        axs[1, 1].imshow((cells).sum(axis=2))
        axs[1, 1].set_title('after cleaning set minsize, dilated')

    params = {'s2': s2, 'remove_thr': remove_thr,
              'erosion': erosion, 'dilation': dilation, 'minsize': minsize}

    return cells, params


def thr_extreme(bin, cs1, lowf=.5, highf=.98, addl=1, subh=1):
    """
    This step introduces not one, but two global thresholds,
    one below which the image will be definatly identified as background
    and one which will be identified as netwok.
    As images might be imaged with different settings,
    these values can be found dependent on the brightest and darkest X %.
    Setting `lowf` to 0 and `highf`. First a high global threshold is found
    by deterining the lower brightness of the brightes X %,
    and than substracting the an absolute value `subh`.
    To determine the background  threshold, first all bigger bright features are excluded
    by calculating a threshold in the middle between the minimum value and the bright
    global threshold, and using a value slightly above the median of these values.
    @param bin binary image to be changed
    @param cs1 raw (or smoothed) image on which the thresholding will be performed
    @param lowf relative position in the brightness histogram of the dark voxels on which `addl` will be added
    @param highf relative position in the brightness histogram  from which `subh` will be substracted
    @param addl offset
    @param subh offset
    """
    h0 = np.histogram(cs1.ravel(), bins=range(
        256), normed=True)  # histogram of the raw image
    tr = np.where(h0[0].cumsum() > highf)[0][0] - subh  # the high threshold

    h1 = np.histogram((((cs1 > (np.min(cs1) + tr / 2)) == False) * cs1).ravel(),
                      bins=range(256), normed=True)  # histogram of image excluding bright voxels
    tr2 = np.where(h1[0].cumsum() > lowf)[0][0] + addl
    bin2 = bin.copy()
    bin2[cs1 > tr] = 1
    bin2[cs1 < tr2] = 0
    params = {'lowf': lowf, 'highf': highf, 'addl': addl, 'subh': subh}
    return bin2, params


def adaptive_thresholding(img, s1=1, s2=1.4, thr=1.5, cells=True, sc1=4, sc2=9, thrc=30, ratio=False, remove=False, remove_thr=0,
                          erosion=3, dilation=2, minsize=150, fillcells=True, closing_iterations=1, thin_section=False, thin_axis=2, extreme=False, lowf=.5, highf=.98, addl=1, subh=1):
    """
    This algorithm performs a thresholding of network images with varying intensity.  The idea of this method is to compare  the image after smoothing  with Gaussian functions of different kernel size (i.e.\ different standard deviation). The default parameters work well if the network structures have a diameter of 2-3 voxel.
after applying difference of Gaussians (DOG) to threshold the network structure, with smoothing diameters s1, s2, it is applied again with wider diameters sc1, sc2.
To avoid artifacts in regions with bright an thick canaliculi, the `remove_network_blobs` algorithm can be used id `remove` is set to `True`. In addition cells can be filled with the `fill_cells` algortithm.

    have a look at the thresholding notebook for an explanation of the single steps, as well as the phd thesis of Felix Repp 4.2.3.1 for some description

    @param img imagedata to be thresholded
    @param    s1 width of the kernal of the first Gaussian Filter, applied to reduce some noise
    @param    s2 width of the kernal of the second Gaussian Filter, to compare intensity to environment
    @param   thr threshold for ratio a value of 1.05 produces good results, in case of differences a value of 1 seems also reasonable
    @param cells if True, dog is repeated with sc1 and sc2  
    @param    sc1 width of the kernal of the first Gaussian Filter, for cells
    @param    sc2 width of the kernal of the second Gaussian Filter for cells, to compare intensity to environment

    @param   thr2 threshold to retrieve cells

    @param  ratio if True, the ration instead of the difference is calculated
    @param remove if True,(and cells == True) remove_network blobs is on the second DOG to only add cells, using the following parameters 
    @param remove_thr set threshold of what will be removed, a smaller threshold will remove more
    @param erosion erode after removing
    @param dilation dilation after erosion
    @param minsize noise smaller than this size will be removed 

    @param fillcells if True (and cells == True), closes empty spaces, after morphologically dilating the image,
    @param closing_iterations during filling, perform n times dilation before filling and erosion afterwards
    @thin_section if cells are cut, filling won't succed. If one dimesion is much thinner than the
    other two the image can be "sealed" to fill most of the cut cells, if set True.
    @param  thin_axis which axis is the smallest for sealing
    @param extreme global threshold of extreme values (see thr_extreme documentation)
    @param lowf relative position in the brightness histogram of the dark voxels on which `addl` will be added in thr_extrean
    @param highf relative position in the brightness histogram  from which `subh` will be substracted in thr_extrean
    @param addl offset in thr_extrean
    @param subh offset in thr_extrean


    @retval   bin binary/ thresholded image
    @retval params Parameters used during this routine

    """

    bin, cs1, cs2, params = dog(img, s1, s2, thr, ratio=ratio, retsmooth=True)
    if extreme:
        bin, extremeparams = thr_extreme(bin, cs1, lowf, highf, addl, subh)
        params.update(extremeparams)
    params['extreme'] = extreme
    params['cells'] = cells
    if cells :   
        bince, csc1, csc2, dog1params = dog(
        img, sc1, sc2, thrc, ratio=ratio, retsmooth=True)
        params['sc1'] = sc1
        params['sc2'] = sc2
        params['thrc'] = thrc
        

        if extreme:
            bince, extremeparams = thr_extreme(bince, cs1, lowf, highf, addl, subh)
           
        

        if remove:
            bince, removeparams = remove_network_blobs(
                bince, cs1, cs2, s2, remove_thr, erosion=erosion,
                dilation=dilation, minsize=minsize)
            
        if fillcells:
            bin, fillparams = fill_cells(
                bin + bince, closing_iterations=closing_iterations,
                thin_section=thin_section, thin_axis=thin_axis)
        else: 
            bin = bin + bince
    params['remove'] = remove
    params['remove_thr'] = remove_thr
    params['erosion'] = erosion
    params['dilation'] = dilation
    params['minsize'] = minsize

    params['closing_iterations'] = closing_iterations
    params['thin_section'] = thin_section
    params['thin_axis'] = thin_axis

    return bin, params


def remove_small(bin, minsize):
    com = label(bin)  # 'morph.binary_opening(cc2,iterations=2))
    sizes = nd.sum(bin, com[0], range(com[1] + 1))
    mask_size = sizes < minsize
    remove_pixel = mask_size[com[0]]
    com[0][remove_pixel] = 0
    bin = com[0] > 0
    return bin


def adaptive_thresholding_old(img, s1=1, s2=1.6,
                              sc1=5, sc2=9, thr=1.05, thrc=1.1, ratio=True, extreme=False, cells=True, noise=25, test=False):
    """
    This algorithmn performes a thresholding of network images with variing intesity.  The idea of this method is to compare  the image after smoothing  with Gaussian functions of different kernel size (i.e.\ different standard deviation). The default parameters work well if the network structures have a diameter of 2-3 voxel.

As lacunae are on a different length scale, few additional steps following the same line of considerations using different Gaussian kernel sizes were  required to  also properly threshold the lacunae. See phd thesis of Felix Repp 4.2.3.1 for description


to find appropriate parameter, set test=[l,h] and check the results of the steps, by comparing images generated by  imshow(XXX[:,:,l:h].sum(axis=-1))

To remove thresholded features in the image that are not connected to the network arising from noise, a connected component analysis was performed and small islands were removed from the thresholded image.
@param img imagedata to be thresholded
 @param    s1 width of the kernal of the first Gaussian Filter, applied to reduce some noise
    @param    s2 width of the kernal of the second Gaussian Filter, to compare intensity to environment
    @param    sc1 width of the kernal of the first Gaussian Filter, for cells
    @param    sc2 width of the kernal of the second Gaussian Filter for cells, to compare intensity to environment
    @param   thr threshold for ratio a value of 1.05 produces good results, in case of differences a value of 1 seems also reasonable
    @param   thr2 threshold to retrieve cells

    @param extreme: if True the brightest 5 % of the Image get included and a low threshold is used to remove background

    @param cells as homogeneously bright stained regions would not be thresholded this flag allows to include those regions
    @param noise remove small clusters of thresholded voxels smaller than noise.
    @param test a list of two values [l,h] to compare projections  between image slices l and h for diffrent substeps

    @retval   b binary/ thresholded image
    @retval params Parameters used during this routine

    """
    print 'adaptive thresholding'
    img = img.astype(np.float)
    cs1 = fi.gaussian_filter(img, s1)  # just noise reduction
    cs2 = fi.gaussian_filter(img, s2)  # neighbourhood of canaliculi

    csc1 = fi.gaussian_filter(img, sc1)  # cells sharp
    csc2 = fi.gaussian_filter(img, sc2)  # cells smooth
    if test:
        l = test[0]
        h = test[1]
        plt.close('all')
        plt.ion()
        plt.figure()
        plt.imshow(cs1[:, :, l:h].std(axis=-1))
        print 'figure 1 shows projection of raw image'
        plt.figure()
        plt.imshow(cs1[:, :, l:h].std(axis=-1))
        print 'figure 2 shows noise reduced image, features of raw image should be recognizable, otherwise reduce s1'
        plt.figure()
        plt.imshow(cs2[:, :, l:h].std(axis=-1))
        print 'figure 3 shows image smoothed broader, features of raw image should be still recognizable, as starting point try s2=1.6*s1 if all features have similiar diameter'
        plt.figure()
        plt.imshow(csc1[:, :, l:h].std(axis=-1))
        print 'figure 4 cells should be clearly recognisible, otherwise reduce cs1'
        plt.figure()
        plt.imshow(csc2[:, :, l:h].std(axis=-1))
        print 'figure 5 shows smoother image of cells'
        print 'press "c" to continue'
        pdb.set_trace()
        plt.close('all')

    # find thresholds that should separate background and brightest voxel
    h0 = np.histogram(cs1.ravel(), bins=range(256), normed=True)

    tr = np.where(h0[0].cumsum() > .95)[0][0] - 1

    h1 = np.histogram((((cs1 > (np.min(cs1) + tr / 2)) == False)
                       * img).ravel(), bins=range(256), normed=True)
    tr2 = np.where(h1[0].cumsum() > .5)[0][0] + 1
    if ratio:
        b = (cs1 / cs2) > thr

    else:
        b = (cs1 - cs2) > thr
    if test:
        print 'canaliculi should be clearly thresholded, play with thr\n press "c" to continue'
        plt.figure()
        plt.imshow(b[:, :, l:h].sum(axis=-1))
        # plt.imshow(((cs1[:,:,l:h]-cs2[:,:,l:h])>20).max(axis=2))
        pdb.set_trace()
    if extreme:

        b[cs1 > tr] = 1
        b[cs1 < tr2] = 0
        if test:
            print 'does it look better when extreame values in/excluded '
            plt.figure()
            plt.imshow(b[:, :, l:h].sum(axis=-1))
            pdb.set_trace()
    if test:
        plt.close('all')

    if cells:
        if ratio:
            c = (csc1 / csc2) > thrc

        else:
            c = (csc1 - csc2) > thrc

        # remove ovious background
        a = morph.binary_erosion(c * (cs1 > tr2), iterations=3)
        # <.05#[:,:,30]) # the canaliculli+ the minimum around them
        aa = fi.gaussian_filter(abs(cs1 / cs2 - 1), s2)

        aa = morph.binary_erosion(a * (aa < 0.1), iterations=1)
        del a
        aa = morph.binary_dilation(aa, iterations=2)
        if test:
            print 'figure 1: cells should be thresholded, change thrc'
            plt.figure()
            plt.imshow(c[:, :, l:h].sum(axis=-1))
            print 'figure 2: now the canaliculi should be removed, tiny patches will be removed.'
            plt.figure()
            plt.imshow(aa[:, :, l:h].sum(axis=-1))
            pdb.set_trace()
            plt.close('all')
        del c
        # remove small patches in cells

        com = label(aa)  # 'morph.binary_opening(cc2,iterations=2))
        sizes = scipy.ndimage.sum(aa, com[0], range(com[1] + 1))
        mask_size = sizes < 150
        remove_pixel = mask_size[com[0]]
        com[0][remove_pixel] = 0

        cc = com[0] > 0
        b += cc

    # remove noise
    if noise > 0:
        b = remove_small(b, noise)

    ##
    b = morph.binary_fill_holes(b)
    if test:
        plt.figure()
        plt.imshow(img[:, :, l:h].std(axis=-1))
        plt.figure()
        plt.imshow(b[:, :, l:h].sum(axis=-1))
        print ' if result is not satisfactory, maybe some hidden parameters can be adapted. Talk to developer'
        pdb.set_trace()
        plt.close('all')
    params = {'s1': s1, 's2': s2, 'sc1': sc1, 'sc2': sc2, 'thr': thr, 'thrc': thrc,
              'ratio': ratio, 'extreme': extreme, 'cells': cells, 'noise': noise}
    print 'done'
    return b, params


def find_cells(bin, dist_tr=4, dist_tr2=2, CANALICULI_SIZE=15, vc=500, dilate_cells=0, dilate_bin=0):
    """Segment Cells from binary image, by using the distance transform and two distance tresholds. seeds, defined by `dist_tr` are grow in the limits defined by  `dist_tr2`. The later will have a strong effect on the canaliculi but will not get rid of all of them. To avoid cells growing into canaliculi and at the  same time trying to fill the cells into the tips, An anisotropic growth is implemeted that should stop when it grows into remainders of canaliculi. The growth stops locally when the locally connected voxels which are added become smaller than `CANALICULI_SIZE`.
since the cells grow to a maximum  defined by `dist_tr2` they might still be smaller than desired. a later isotropic dilation by `dilate_cells`.
Adaptive thresholding can lead to thresholding artifacts close to cells, resulting in a loss of conectivity. As a counter measure cells can be grown beyond the desired size and added to the binary image. As a last step, the cells are dilated by `dilate_bin` iterattions and the binary image is adapted. this does not affect the cells returned by this function

All values measured in voxels.
    @param bin binary image
    @param dist_tr threshold for seeds
    @param dist_tr2 threshold to grow into
    @param CANALICULI_SIZE (in Voxel)
    @param VC minimal Volume for cell to remain in segmentation (in Voxel)
    @param dilate_cell the amount of iterations performed isotropically to grow them to the desired size. 
    @param dilate_bin the amount of iterations performed as last step to counteract thresholding artifacts.  
    @retval cells segmented cells
    @retval params Parameters used
    """
    print 'find cells'
    bin = scipy.ndimage.morphology.distance_transform_edt(bin)

    C = bin > dist_tr
    bin2 = bin > dist_tr2

    # structure for dilation
    sp = np.zeros((3, 3, 3))
    sp[1, :, :] = 1
    sp[:, 1, :] = 1
    sp[:, :, 1] = 1
    # com=label(C,sp)
    # sizes = scipy.ndimage.sum(C, com[0], range(com[1] + 1))
    # mask_size = sizes < 50
    # remove_pixel = mask_size[com[0]]
    # C[remove_pixel] = 0

    # a=mlab.contour3d(CL[:50,:50,:].astype(float),contours=[0.3])

    if(C.max()):
        print 'growing cells'
        df = C.sum()
        while(df):
            tmp = C.copy()
            imd3 = np.logical_and(morph.binary_dilation(C, sp), bin2)

            deim = imd3 - C
            com = label(deim, sp)
            sizes = scipy.ndimage.sum(deim, com[0], range(com[1] + 1))
            mask_size = sizes > CANALICULI_SIZE
            add_pixel = mask_size[com[0]]
            C[add_pixel] = 1

            # for l in arange(com[1] + 1)[sizes>CANALICULI_SIZE].tolist():
            #   C+=com[0]==l

            df = np.sum(C - tmp)
            # if df<CANALICULI_SIZE+2:
            #    imshow(com[0][:,:,-58])
            # print df
            # if df==0:
            #    pdb.set_trace()

            # plot(t,np.sum(hd[0]>50),'bx')
            # plot(t,np.sum(hd[0]>3),'rx')

            # plot(thr,anz,label=str(i))
            # pdb.set_trace()
        # legend()
        C = remove_small(C, vc)
    if dilate_cells:
        C = morph.binary_dilation(C, sp, iterations=dilate_cells)
    if dilate_bin:
        C2 = morph.binary_dilation(C, sp, iterations=dilate_bin)
        bin[C2]=1    
    print 'done'

    params = {'dist_tr': dist_tr, 'dist_tr2': dist_tr2,
              'CANALICULI_SIZE': CANALICULI_SIZE, 'vc': vc, 'dilate_cells': dilate_cells, 'dilate_bin':dilate_bin}
    return C, params


def project_bin_cells(bin, cells, fname):

    Ic = cells.sum(axis=2).astype(np.float)
    Ic[Ic > 50] = 50
    Ic /= 50.
    Ir = bin.std(axis=2)
    Ir /= Ir.max()
    im = np.array([1 - Ic, 1 - Ir - Ic, 1 - Ir + Ic])

    im[im < 0] = 0
    im *= 255
    im[im > 255] = 255

    fig = plt.figure(
        figsize=(cells.shape[1] / 100., cells.shape[0] / 100.), dpi=100, frameon=False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)

    plt.imshow(im.T.astype(np.uint8).swapaxes(0, 1))
    plt.savefig(fname)

    #


def project_raw(raw, fname, cmap='cubehelix'):
    if cmap == 'cubehelix':
        import colormaps as cmaps
        plt.register_cmap(cmap=cmaps.cubehelix1)
    fig = plt.figure(
        figsize=(raw.shape[1] / 100., raw.shape[0] / 100.), dpi=100, frameon=False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)

    plt.imshow(raw.std(axis=-1), cmap=cmap)
    plt.savefig(fname)


def project_raw_bin(raw, bin, fname):

    fig = plt.figure(
        figsize=(raw.shape[1] / 100., raw.shape[0] / 100.), dpi=100, frameon=False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    Ir = np.zeros_like(raw[:, :, 0])
    Ir = np.zeros_like(raw[:, :, 0])
    S = np.zeros_like(raw)
    S[bin] = raw[bin]
    Ir = S.std(axis=2)
    S = np.zeros_like(raw)
    S[bin == False] = raw[bin == False]
    Ib = S.std(axis=2)
    Ir *= 255 / Ir.max()
    Ib *= 255 / (Ib.max())
    Ir[Ir > 255] = 255
    Ib[Ib > 255] = 255
    I = np.ones((Ir.shape[0], Ir.shape[1], 3), dtype=np.uint8) * 255
    I[:, :, 1:] -= Ir[:, :, np.newaxis].astype(np.uint8)
    I[:, :, :2] -= Ib[:, :, np.newaxis].astype(np.uint8)

    plt.imshow(I)

    plt.savefig(fname)
