var searchData=
[
  ['reduce_5falpha',['reduce_alpha',['../namespacetinanet.html#a4e0f679900be0a6855b3e1d6e859eac4',1,'tinanet']]],
  ['remove_5fcells_5ffrom_5fnetwork',['remove_cells_from_network',['../namespacetinanet.html#a195a1b596d7aeea18ffc82eda69060f1',1,'tinanet']]],
  ['remove_5ffalse_5fnodes',['remove_false_nodes',['../namespacetinanet.html#a02820a42e03c02ae934586950f2d7279',1,'tinanet']]],
  ['remove_5fnetwork_5fblobs',['remove_network_blobs',['../namespacetinaimg.html#a52ed99e1454a9251123976ee388ff690',1,'tinaimg']]],
  ['remove_5fsmall',['remove_small',['../namespacetinaimg.html#aeff18978381c5b73e053ac19088d7cd8',1,'tinaimg']]],
  ['replaceedgesbysplines',['replaceEdgesBySplines',['../namespacetinanet.html#a4400f4171ea70834e8680891f546e0e7',1,'tinanet.replaceEdgesBySplines()'],['../namespacetinaui.html#a73e3faf9d5dc733c7e331e89bdc9a1f4',1,'tinaui.replaceEdgesBySplines()']]]
];
