import sys
sys.path.append('../source')

from tina_main import dataset
import tinaui

t1 = dataset(folder='../test/sampledata/ocn')  # initialize the dataset
# load images with smaller extent, using the information from .xml-files
# for spacing of the image data
tinaui.load_initial_data(t1, ftype='tif', nfilter=[
                         'ch', '0.'], xml=True, extent=(100, 200, 300, 450, 0, 80))
t1.origin = (0, 0, 0)
tinaui.adaptive_thresholding(t1)
tinaui.find_cells(t1)
tinaui.skeletonize_thinvox(t1)
tinaui.get_graph(t1)
tinaui.save_status(t1, save_images=['bin', 'cells'])
tinaui.add_tag_to_filenames(t1, 'test')
