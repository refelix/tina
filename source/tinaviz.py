# -*- coding: utf-8 -*-
# author: Felix Repp felix.repp@mpikg.mpg.de
# !/usr/bin/env python
"""@package tinaviz
%This module provides the tools to visualize the Network as well as the image data
"""
import os
import numpy as np
import pdb
import networkx as nx
import sys
import itertools
from mayavi import mlab
from tvtk.api import tvtk
from mayavi.sources.api import VTKDataSource
from traitsui.api import View, Item, Label, ApplyButton
from traits.api import *  # HasTraits, Range,Int,Property

sys.path.append("../lib")
from scipy import io
import tina_main


def plot_iso(src, Type, extent=None, contours=None, blurr=0, **kwargs):
    """
    Function to visualize volumetric data by using an isosurface.
    @param src mayavi datasource, typically dataset.vtkdata
    @param Type name of imagedata
    @param extent extent of the volume to be visualized
    @param contours a list of contour values e.g. [150] if None is provided a automatically calculated value is used
    @param blurr a gaussion blur can be applied to smoothen the data a little and make the visualizaton mor smooth
    @param kwargs arguments passed to mlab.pipeline.surface
    """
    pd = mlab.pipeline.set_active_attribute(src, point_scalars=Type)
    if extent is None:
        voi = mlab.pipeline.extract_grid(pd)

    else:
        voi = mlab.pipeline.extract_grid(pd)
        voi.filter.voi = extent
    # src.update_image_data = True
    # pdb.set_trace()
    # if blurr !=1:
    if blurr != 0:
        da = user_defined(voi, 'ImageGaussianSmooth')
        da.filter.set_standard_deviation(blurr)
        da.filter.set_radius_factor(max([blurr * 1.5, 5]))
    # else:
    #    diffuse_filter = tvtk.ImageAnisotropicDiffusion3D(
    #                            diffusion_factor=.1,
    #                            diffusion_threshold=30.0,
    #                            number_of_iterations=5, )
    #    blur= mlab.pipeline.user_defined(voi, filter=diffuse_filter)

    # if gradMagnitude:
    #    gm = user_defined(blur, 'ImageGradientMagnitude')
    #    gm.filter.dimensionality=3
    #    da=gm
    else:
        da = voi

    # voi = mlab.pipeline.extract_grid(blur)#src, 'ExtractVOI')
    # src.add_child(blur)
    # voi = mlab.pipeline.extract_grid(da)
    # extend.reverse()
    # voi.filter.voi=extend
    # voi.set(*extend)
    # blur.filter.voi = voi
    # b=mlab.pipeline.iso_surface(da, contours=contours,	colormap='Spectral')
    contour = mlab.pipeline.contour(da)
    if contours:
        contour.filter.contours = contours
        # pdb.set_trace()
    dec = mlab.pipeline.decimate_pro(contour)
    dec.filter.feature_angle = 60.
    dec.filter.target_reduction = 0.7

    smooth_ = tvtk.SmoothPolyDataFilter(
        number_of_iterations=10,
        relaxation_factor=0.1,
        feature_angle=60,
        feature_edge_smoothing=False,
        boundary_smoothing=False,
        convergence=0.,
    )
    smooth = mlab.pipeline.user_defined(dec, filter=smooth_)
    compute_normals = mlab.pipeline.poly_data_normals(smooth)
    compute_normals.filter.feature_angle = 80
    if 'color' not in kwargs.keys():
        kwargs['color'] = (0.9, 0.72, 0.62)
    surf = mlab.pipeline.surface(compute_normals, **kwargs)

    return da, surf
    # pdb.set_trace()

    # ,format='jpg')


def user_defined(parent, filter_name):
    """ A function to add a TVTK filter not already in Mayavi.
    """
    from mayavi.filters.user_defined import UserDefined
    from tvtk.api import tvtk
    filter = UserDefined(filter=getattr(tvtk, filter_name)())
    parent.add_child(filter)
    return filter


def limit_voi_to_nodes(voi, dataset, nodes, Graph, pad=5):
    x = []
    y = []
    z = []
    import networkx as nx

    for n in nodes:
        x.append(nx.get_node_attributes(Graph, 'x')[n])
        y.append(nx.get_node_attributes(Graph, 'y')[n])
        z.append(nx.get_node_attributes(Graph, 'z')[n])
    x = np.array(x)
    y = np.array(y)
    z = np.array(z)
    # pdb.set_trace()
    nx = ((x - voi.outputs[0].origin[0]) / voi.outputs[0].spacing[0]).round()
    ny = ((y - voi.outputs[0].origin[1]) / voi.outputs[0].spacing[1]).round()
    nz = ((z - voi.outputs[0].origin[2]) / voi.outputs[0].spacing[2]).round()
    sp = dataset.data.dimensions
    voi.filter.voi[4] = max(0, np.min(nz) - pad)
    voi.filter.voi[5] = min(sp[2] - 1, np.max(nz) + pad)
    voi.filter.voi[2] = max(0, np.min(ny) - pad)
    voi.filter.voi[3] = min(sp[1] - 1, np.max(ny) + pad)
    voi.filter.voi[0] = max(0, np.min(nx) - pad)
    voi.filter.voi[1] = min(sp[0] - 1, np.max(nx) + pad)
    voi.filter.voi = voi.filter.voi.copy()


class voi_shifter(HasTraits):

    xmin = Property(depends_on=['xm', 'xw'])

    xmax = Property(depends_on=['xm', 'xw'])

    ymin = Property(depends_on=['ym', 'yw'])

    ymax = Property(depends_on=['ym', 'yw'])

    ymin = Property(depends_on=['ym', 'yw'])

    zmax = Property(depends_on=['zm', 'zw'])
    traits_view = View(Item(name='xm'), Item(name='xw'), Item(name='ym'), Item(name='yw'),
                       Item(name='zm'), Item(name='zw'),
                       resizable=True, width=300, height=100, title="voi")

    def __init__(self, voi, full=False):
        HasTraits.__init__(self)

        xm = Range(0, voi.parent.parent.data.dimensions[
                   0] - 1, value=voi.parent.parent.data.dimensions[0] / 2, mode='slider')
        xw = Range(0, voi.parent.parent.data.dimensions[
                   0] / 2, value=voi.parent.parent.data.dimensions[0] / 2, mode='slider')
        ym = Range(0, voi.parent.parent.data.dimensions[
                   0] - 1, value=voi.parent.parent.data.dimensions[1] / 2, mode='slider')
        yw = Range(0, voi.parent.parent.data.dimensions[
                   0] / 2, value=voi.parent.parent.data.dimensions[1] / 2, mode='slider')
        zm = Range(0, voi.parent.parent.data.dimensions[
                   0] - 1, value=voi.parent.parent.data.dimensions[2] / 2, mode='slider')
        zw = Range(0, voi.parent.parent.data.dimensions[
                   0] / 2, value=voi.parent.parent.data.dimensions[2] / 2, mode='slider')

        self.voi = voi
        self.add_trait('xm', xm)
        self.add_trait('xw', xw)
        self.add_trait('ym', ym)
        self.add_trait('yw', yw)
        self.add_trait('zm', zm)
        self.add_trait('zw', zw)
        if not full:
            self.xm = int((voi.filter.voi[0] + voi.filter.voi[1]) / 2)
            self.xw = int(-(voi.filter.voi[0] - voi.filter.voi[1]) / 2)
            self.ym = int((voi.filter.voi[2] + voi.filter.voi[3]) / 2)
            self.yw = int(-(voi.filter.voi[2] - voi.filter.voi[3]) / 2)
            self.zm = int((voi.filter.voi[4] + voi.filter.voi[5]) / 2)
            self.zw = int(-(voi.filter.voi[4] - voi.filter.voi[5]) / 2)
        self.edit_traits()

    def _get_xmin(self):
        xmi = max(0, self.xm - self.xw)
        self.voi.filter.voi[0] = xmi
        self.voi.filter.voi = voi.self.filter.voi.copy()
        return xmi

    def _get_xmax(self):
        x_ma = min(self.xm + self.xw,
                   self.voi.parent.parent.data.dimensions[0] - 1)
        self.voi.filter.voi[1] = x_ma
        self.voi.filter.voi = voi.self.filter.voi.copy()
        return x_ma

    def _get_ymin(self):
        ymi = max(0, self.ym - self.yw)
        self.voi.filter.voi[2] = ymi
        self.voi.filter.voi = voi.self.filter.voi.copy()
        return ymi

    def _get_ymax(self):
        yma = min(self.ym + self.yw, self.xw,
                  self.voi.parent.parent.data.dimensions[1] - 1)
        self.voi.filter.voi[3] = yma
        self.voi.filter.voi = voi.self.filter.voi.copy()
        return yma

    def _get_zmax(self):
        zma = min(self.zm + self.zw,
                  self.voi.parent.parent.data.dimensions[2] - 1)
        self.voi.filter.voi[5] = zma
        self.voi.filter.voi = voi.self.filter.voi.copy()
        return zma

    def _get_zmin(self):
        zmi = max(self.zm - self.zw, v0)
        self.voi.filter.voi[4] = zmi
        self.voi.filter.voi = voi.self.filter.voi.copy()
        return zmi

        # xm=Range(0,voi.parent.parent.data.dimensions[0]-1,value=voi.parent.parent.data.dimensions[0]/2)
        # self.add_trait('X_mean', xm)


def plot_graph(dataset, G,
               node_size=1, node_color=(1, 1, 1), scale_node_keyword=None,
               node_color_keyword=None,
               edge_color_keyword=None, edge_radius_keyword=None,
               edge_color=(1, 1, 1), edge_colormap='jet',
               tube_radius=.4, **kwargs):
    """ 3D plot of a TINA network,edges are drawn as curves in space, nodes as balls. For both, edges and nodes the size and the color can visualize a parameter of the dictionary, for edges this needs to be either a a number per edge, or a sequence with the length equal to the length of coordinates

        @param dataset
        @param node_size
        @param node_color
        @param scale_node_keyword='distance_to_node','degree' scales nodes with many neighbours bigger

        @param edge_color_keyword={None,'distance_to_nearest_cell','growthproj'}
            None, use edgecolor,
            #'distance_to_nearest_cell' each edge same color,
            'growthproj' color coresponds do projection of dendrite and growthfront
            'straightness'
            'distance_to_node'
        @param edge_radius_keyword
        @param node_color_keyword{'distance_to_node',distance_closest_cell}


    """
    print 'plot graph'
    from tvtk.api import tvtk
    cell_array = tvtk.CellArray()
    if node_size is not None:
        node_size *= min(dataset.spacing)
    tube_radius *= min(dataset.spacing)
    # nodes=G.nodes()
    # tracks=nodes
    # pdb.set_trace()
    c_start = []
    c_stop = []
    edge_radii = []
    # edge_color=[]
    pt = []
    i = 0
    xstop = []
    ystop = []
    zstop = []
    xstart = []
    ystart = []
    zstart = []
    xn = []
    yn = []
    zn = []
    start_l = []
    stop_l = []
    # ndic={}
    edge_r = []
    edge_c = []

    cstr = 'weight'

    for node in G.nodes():
        xn.append(G.node[node]['x'])
        yn.append(G.node[node]['y'])
        zn.append(G.node[node]['z'])
        # ndic[node[0]]=i

    edge_c = []
    edge_r = []
    edgecount = 0
    lines = []
    if edge_color_keyword == 'direction':
        edge_dx = []
        edge_dy = []
        edge_dz = []
    # pdb.set_trace()
    for n1, n2, edgedict in G.edges(data=True):
        # if edgedict ['weight']<10:
        #    continue
        edgecount += 1
        # if np.isnan(edgedict [edge_color_keyword]).sum():
        #    continue
        xstart.extend(edgedict['x'])
        xstop.extend(edgedict['x'])
        ystart.extend(edgedict['y'])
        ystop.extend(edgedict['y'])
        zstart.extend(edgedict['z'])
        zstop.extend(edgedict['z'])

        l = len(edgedict['x'])
        line = range(i, i + l)
        line.insert(0, l)
        lines.extend(line)
        i += l
        # start_l.append(i)
        # stop_l.append(ndic[n2])

        if edge_color_keyword and  edge_color_keyword not in edgedict.keys():
            print (n1, n2)
            edge_c += [1] * (l)
            continue

        if edge_color_keyword is None:
            edge_c += [1] * (l)

        if edge_color_keyword in edgedict.keys():
            # pdb.set_trace()
            if len(edgedict[edge_color_keyword]) == 1:
                edge_c.extend([edgedict[edge_color_keyword]] * (l))
            elif len(edgedict[edge_color_keyword]) != len(edgedict['x']):
                pdb.set_trace()
            
            else:
                edge_c.extend(edgedict[edge_color_keyword].tolist())

        if edge_color_keyword == 'direction':
            edge_c += [1] * (l)
            edge_dx.extend(edgedict['dx'])
            edge_dy.extend(edgedict['dy'])
            edge_dz.extend(edgedict['dz'])

        if edge_radius_keyword in edgedict.keys():
            if np.size(edgedict[edge_radius_keyword]) == 1:
                edge_r.extend([edgedict[edge_radius_keyword]] * (l))
            else:
                edge_r.extend(edgedict[edge_radius_keyword])
        else:
            edge_r += [tube_radius] * (l)

    # pdb.set_trace()
    # indices = np.c_start_l, stop_l]

    fig = dataset.fig
    disable_render = fig.scene.disable_render
    fig.scene.disable_render = True

    edge_c = np.array(edge_c)
    if np.isinf(edge_c).any():
        edge_c[np.isinf(edge_c)] = np.nan

    # stop

    xv = np.array([xstart])  # +xstop])
    yv = np.array([ystart])  # +ystop])
    zv = np.array([zstart])  # +zstop])
    # pdb.set_trace()
    edges_src = mlab.pipeline.scalar_scatter(
        xv.ravel(), yv.ravel(), zv.ravel(), edge_c.ravel())

    edges_src.mlab_source.dataset.point_data.add_array(
        np.abs(np.array(edge_r).ravel()))
    edges_src.mlab_source.dataset.point_data.get_array(1).name = 'radius'

    cell_array.set_cells(edgecount, lines)
    # ,np.vstack([np.arange(edge_scalar.shape[0]),arange(edge_scalar.shape[0],2*edge_scalar.shape[0])]).T[:-1,:]
    edges_src.mlab_source.dataset.lines = cell_array
    if edge_color_keyword == 'direction':
        # vectors = numpy.c_[edge_dx.ravel(), edge_dy.ravel(), edge_dy.ravel()].ravel()
        vectorsrc = mlab.pipeline.vector_scatter(np.array(xv).ravel(), np.array(
            yv).ravel(), np.array(zv).ravel(), edge_dx, edge_dy, edge_dz)  # scalars=
        dataset.vecsrc = mlab.pipeline.add_dataset(vectorsrc)

        dotp = tvtk.ImageDotProduct()
        d = tvtk.DotProductSimilarity()
        sd = mlab.pipeline.user_defined(dataset.vecsrc, filter=d)
        # vectors = np.array([edge_dx, edge_dy,edge_dz]).ravel()

        # vectors.shape = (vectors.size/3, 3)

        # edges_src.mlab_source.dataset.                          point_data.vectors=vectors
        # edges_src.mlab_source.dataset.point_data.vectors.name = 'vectors'
        # stop
        pdb.set_trace()
    edges_src.mlab_source.update()

    if tube_radius is not None:
        tubes = mlab.pipeline.tube(mlab. pipeline.set_active_attribute(edges_src,
                                                                       point_scalars='scalars'),
                                   tube_radius=tube_radius,
                                   )

    else:
        tubes = edges_src
    tube_surf = mlab.pipeline.surface(mlab.pipeline. set_active_attribute(
        tubes, point_scalars='scalars'), colormap=edge_colormap, color=edge_color, **kwargs)

    if edge_color_keyword is None:
        tube_surf.actor.mapper.scalar_visibility = False
    else:
        tube_surf.actor.mapper.scalar_visibility = True
    if not node_size:
        return tube_surf, None

    # ------------------------------------------------------------------------
    # Plot the nodes
    if node_size is not None:
        nodes = G.nodes()
        L = len(G.nodes())
        # pdb.set_trace()
        if node_color_keyword is None or scale_node_keyword is None:
            # alte  version
            nodes = G.nodes()
            L = len(G.nodes())

            if scale_node_keyword == 'degree':
                dic = G.degree(nodes)
                s = []
                for n in nodes:
                    s.append(dic[n])
            else:
                s = np.ones(len(xn))
            # pdb.set_trace()
            pts = mlab.points3d(xn, yn, zn, s,
                                scale_factor=node_size,
                                # colormap=node_colormap,
                                resolution=16,
                                color=node_color)

        else:
            if node_color_keyword == 'distance_to_node':
                node_color_scalar = nx.get_node_attributes(
                    G, 'distance_to_node').values()
            if node_color_keyword == 'distance_to_none':
                node_color_scalar = l * [1]

            node_src = mlab.pipeline.scalar_scatter(np.array(xn).ravel(), np.array(
                yn).ravel(), np.array(zn).ravel(), np.array(node_color_scalar).ravel())

            if scale_node_keyword is None:
                node_r += [1] * (L)
            if scale_node_keyword == 'degree':
                s = G.degree().values()

            else:
                s = np.ones(len(xn))
            # pdb.set_trace()
            node_src.mlab_source.dataset.point_data.vectors = np.abs(
                np.array([s, s, s]).T)

            node_src.mlab_source.dataset.point_data.add_array(
                np.abs(np.array(s).ravel()))
            node_src.mlab_source.dataset.point_data.get_array(
                1).name = 'radius'
            # pdb.set_trace()
            pts = mlab.pipeline.glyph(mlab. pipeline.set_active_attribute(
                node_src, point_scalars='scalars', point_vectors='radius'), scale_factor=node_size * 5, resolution=16)
            # mlab.points3d(xn, yn, zn, node_color_scalar, scale_factor=node_size, colormap=node_colormap,  resolution=16,color=node_color)
            pts.glyph.color_mode = 'color_by_scalar'
            pts.glyph.scale_mode = 'scale_by_vector'
    print 'done'
    fig.scene.disable_render = disable_render
    return tube_surf, pts


def plot_cell_axis(celllist, axis, figure=None):
    """plot the orientation of the cells using their eigenvalues
    @param dataset
    @param celllist a list of class cell objects
     @param axis: which axis to plot: 'all', 0,1,2: biggest middle or smallest, 'unique' dependend on skew, the biggest or smallest axis is plotted

    """

    print 'plot cell orientation'
    from tvtk.api import tvtk
    cell_array = tvtk.CellArray()

    xn = []
    yn = []
    zn = []
    lines = []
    scalar = []
    i = 0
    edgecount = 0
    for cell in celllist:
        edgecount += 1
        cm = cell.stats['cm']
        E = cell.stats['eigen']
        if axis == 'unique':
            if cell.stats['skew'] < 0:
                sta = (cm - np.sqrt(E[0][0]) * 2 * sqrt(5) * E[1][0]).tolist()
                sto = (cm + np.sqrt(E[0][0]) * 2 * sqrt(5) * E[1][0]).tolist()
            else:
                sta = (cm - np.sqrt(E[0][-1]) * 2 *
                       sqrt(5) * E[1][-1]).tolist()
                sto = (cm + np.sqrt(E[0][-1]) * 2 *
                       sqrt(5) * E[1][-1]).tolist()
            # pdb.set_trace()
            scalar.append(cell.stats['skew'])
            scalar.append(cell.stats['skew'])
            xn += [sta[0], sto[0]]
            yn += [sta[1], sto[1]]
            zn += [sta[2], sto[2]]
            lines.extend([2, i, i + 1])
            i += 2
        elif axis == 'all':
            for j in range(3):
                sta = (cm - np.sqrt(E[0][j]) * E[1][j]).tolist()
                sto = (cm + np.sqrt(E[0][j]) * E[1][j]).tolist()

            # pdb.set_trace()
                scalar.append(j)
                scalar.append(j)
                xn += [sta[0], sto[0]]
                yn += [sta[1], sto[1]]
                zn += [sta[2], sto[2]]
                lines.extend([2, i, i + 1])
                i += 2
        elif axis in[0, 1, 2]:

            sta = (cm - np.sqrt(E[0][axi]) * E[1][axis]).tolist()
            sto = (cm + np.sqrt(E[0][axis]) * E[1][axis]).tolist()

            # pdb.set_trace()
            scalar.append(cell.stats['skew'])
            scalar.append(cell.stats['skew'])
            xn += [sta[0], sto[0]]
            yn += [sta[1], sto[1]]
            zn += [sta[2], sto[2]]
            lines.extend([2, i, i + 1])
            i += 2
        else:
            print 'axis key not unterstood'

        # ndic[node[0]]=i

    # pdb.set_trace()
    edges_src = mlab.pipeline.scalar_scatter(xn, yn, zn, scalar)

    cell_array.set_cells(edgecount, lines)
    # ,np.vstack([np.arange(edge_scalar.shape[0]),arange(edge_scalar.shape[0],2*edge_scalar.shape[0])]).T[:-1,:]
    edges_src.mlab_source.dataset.lines = cell_array

    if not figure:
        fig = mlab.gcf()
    else:
        fig = figure
    fig = dataset.fig
    disable_render = fig.scene.disable_render
    fig.scene.disable_render = True

    edges_src.mlab_source.update()

    tubes = mlab.pipeline.tube(mlab. pipeline.set_active_attribute(edges_src,
                                                                   point_scalars='scalars'),
                               tube_radius=tube_radius,
                               )
    tube_surf = mlab.pipeline.surface(mlab.pipeline. set_active_attribute(
        tubes, point_scalars='scalars'), colormap=edge_colormap, **kwargs)

    fig.scene.disable_render = disable_render
    return tube_surf


def add_dataset(dataset, name, count=0):
    """
    create a dataset for the visualization using mayavi
    @param dataset TINA dataset
    @param name key of dataset.imgdata
    @param count  if count = 0 a new data source is created. if count >0 imagedata is added to the existing dataset
    """
    data = dataset.imgdict[name]
    # if convert_to_float and data.dtype in ['bool','uint8']:
    #    data=data.astype(np.float32)
    if data.dtype == 'bool':
        data = data.astype(np.uint8) * 255
    # elif np.unique(data)==array([  0, 1], dtype=uint8)
    #    data=data*255

    if count == 0:
        i = tvtk.ImageData(spacing=dataset.spacing, origin=dataset.origin)

        i.point_data.add_array(data.T.ravel())
        # i.point_data.scalars = data.T.ravel()i.point_data.get_array(N).name =
        # name
        i.dimensions = data.shape[::]

        # i.point_data.scalars.name = name
        i.point_data.get_array(0).name = name
        src = VTKDataSource(data=i)
        mlab.pipeline.add_dataset(src)
        dataset.vtkdata = src
    else:
        i = dataset.vtkdata.data
        N = i.point_data.number_of_arrays
        i.point_data.add_array(data.T.ravel())

        i.point_data.get_array(N).name = name
        if (i.dimensions != data.shape[::]).any():
            pdb.set_trace()
    return i


def add_cell_data(self):

    for cell in self.celllist:
        i = add_dataset(self, cell.data, cell.origin, cell.spacing,
                        name='cell' + str(cell.label), count=0)
        cell.vtkdata = i
"""
class lut_shifter(HasTraits):
    vmax=Range(0.,256,value=255,mode='slider')
    vmin=Range(0.,256,value=1,mode='slider')
    nodes=Bool(True)
    traits_view = View(
    Item(name='vmax'),Item(name='vmin'),Item(name='nodes'),
    resizable = True,width=300,height=100,title="lut")
    def __init__(self,ocn):
        HasTraits.__init__(self)
        self.lut = ocn.etube.module_manager.scalar_lut_manager.lut
        self.lut2 = ocn.npts.module_manager.scalar_lut_manager.lut
        self.mmax=float(self.lut.range[1])
        self.mmin=float(self.lut.range[0])
        #self.add_trait('vmin', vmin)
        #self.add_trait('vmax', vmax)
        self.table=self.lut.table.to_array()
        self.table2=self.lut2.table.to_array()
    def _vmax_changed(self,info):
        l=self.table
        l[:, -1] =255* np.ones(256)
        l[:,-1][:round(256*(self.vmin-self.mmin)/256)]=0
        l[:,-1][round(255*(self.vmax-self.mmin)/256):]=0
        self.lut.table =l
        if self.nodes:
            l2=self.table2
            l2[:, -1] =255* np.ones(256)
            l2[:,-1][:round(256*(self.vmin-self.mmin)/256)]=0
            l2[:,-1][round(255*(self.vmax-self.mmin)/256):]=0
            self.lut2.table =l2
    def _vmin_changed(self,info):
        l=self.table
        l[:, -1] =255* np.ones(256)
        l[:,-1][:round(256*(self.vmin-self.mmin)/256)]=0
        l[:,-1][round(255*(self.vmax-self.mmin)/256):]=0
        self.lut.table =l
"""
