var searchData=
[
  ['mainpage_2emd',['mainpage.md',['../mainpage_8md.html',1,'']]],
  ['make_5fdegree_5fhistogram',['make_degree_histogram',['../namespacetinaui.html#a11d817a12528a3cebf5ce0f1ae89a848',1,'tinaui']]],
  ['make_5flength_5fhistogram',['make_length_histogram',['../namespacetinaui.html#a477ab7ba0ad9dcc9a3937ed871037287',1,'tinaui']]],
  ['make_5fprojection_5fimages',['make_projection_images',['../namespacetinaui.html#a9c319ebc994f799f498fbb6922db414b',1,'tinaui']]],
  ['makejobskel',['makejobskel',['../namespacetinaskel.html#a1fbee70c66e79d6b7a07b605ece1ef85',1,'tinaskel']]],
  ['mainpage',['mainpage',['../md_mainpage.html',1,'']]],
  ['merge_5fshort_5fedges',['merge_short_edges',['../namespacetinanet.html#a42c15dcb73e7a487d567bc88b2cc27bb',1,'tinanet.merge_short_edges()'],['../namespacetinaui.html#a2c6e41b42ad61ccd94a371d6a3649945',1,'tinaui.merge_short_edges()']]],
  ['my_5fcmap',['my_cmap',['../namespaceanalyze__ocn.html#ab54fe63b96f53dfcf26e9504836ba764',1,'analyze_ocn']]]
];
