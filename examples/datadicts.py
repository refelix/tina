import sys
sys.path.append('../source')

from matplotlib import pyplot as plt  # for visualisation of imagedata
import networkx as nx
import tinaui
plt.ion()
# if only networks will be used load_imagedata can be set False
t2 = tinaui.load_status(folder='../test/sampledata/ocn', load_imagedata=True)
plt.imshow(t2.imgdict['raw'][:, :, 15])  # to show an individual imageslice
plt.figure()
# show a std projection along the last axis (try also .max() or .sum())
plt.imshow(t2.imgdict['bin'][:, :, :].std(axis=-1))

# that is the Network structure as created from the skeletonization
G = t2.networkdict['initial']

plt.figure()
# without .values() you get a dictonary with the name of the node as key
deg = G.degree().values()
# plot the degree distribution use bins for predefined bars
plt.hist(deg, log=True, bins=range(1, max(deg)))

plt.figure()
plt.hist(nx.get_edge_attributes(G, 'weight').values(), log=True)
