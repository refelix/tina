# -*- coding: utf-8 -*-
# author: Felix Repp felix.repp@mpikg.mpg.de
# !/usr/bin/env python
import networkx as nx
import scipy.optimize as op
import numpy as np
import tinanet
import matplotlib.pyplot as plt
import pdb
from scipy.ndimage import morphology as morph
"""@package tinana
%This module provides the tools to  analyze the data, make histograms, etc.

"""


def expd(x, A, b):
    return A * np.exp(-b * x)


def get_tot_length(n):
    return np.sum(nx.get_edge_attributes(n, 'weight').values())


def get_edge_hist(n, keyword='weight', cumulative='reverse', normalize=True, weights=None, bins=np.arange(0, 25, 0.5)):
    """
    calculates a histogram from the network `n` of the edge attribute with name `keyword`

    @param n NetworkX Graph
    @param keyword  using nx.get_edge_attributes('keyword'): if a list or an array is returned, values will be extended.
    @param cumulative if True: normal cumulative histogram, if 'reverse': negative slope
    @param normalize: arrea under curve =1
    @param weight: can be numbers or keyword
    @param bins bins of the histogram
    """
    wn = nx.get_edge_attributes(n, keyword).values()
    if isinstance(weights, type('')):
        weights = nx.get_edge_attributes(n, weights).values()
        if np.size(weights[0]) > 1:
            weightl = []
            for i in wight:
                weightl.extend(list(i))
            weights = weightl

    if np.size(wn[0]) > 1:
        wnl = []

        for i in wn:
            wnl.extend(list(i))
        wn = wnl

    wh = np.histogram(wn, weights=weights, bins=bins)

    whv = wh[0]  # values
    if cumulative == 'reverse':
        whv = whv[::-1].astype(float)

    if cumulative:
        whv = np.cumsum(whv)
    if cumulative == 'reverse':
        whv = whv[::-1]
    if normalize:
        whv /= np.sum(wh[0])

    return wh[1], whv


def get_node_hist(n, keyword='degree', cumulative='reverse', normalize=True, weighr=None, weights=None, bins=np.arange(0, 25, 1)):
    """
    calculates a histogram from the network `n` of the node attribute with name `keyword`, or the degree or weighed_degree of the node


    @param n NetworkX Graph
    @param keyword  using nx.get_node_attributes('keyword'), or n.degree
    @param cumulative if True: normal cumulative histogram, if 'reverse': negative slope
    @param normalize: arrea under curve =1
    @param node_weight: can be numbers or keyword
    @param edge_weights: sum of all edges weights
    @param bins bins of the histogram

    """
    if keyword == 'degree':
        wh = n.degree().values()
    elif keyword == 'weighted_degree':
        tinanet.get_weighted_degree.values()
    else:
        wh = nx.get_node_attributes(n, keyword).values()
    if weights:
        weights = nx.get_edge_attributes(n, weights).values()
    """
    if edge_weights:
        edwl=[]
        for node in nx.get_node_attributes(n,keyword).keys():
           s=0
           for n1,n2, ed in n.edges(node)
                s+=ed[edge_weights]
           edwl.append(s)
        weights= edwl
    """

    wh = np.histogram(wh, weights=weights, bins=bins)

    whv = wh[0]  # values
    if cumulative == 'reverse':
        whv = whv[::-1].astype(float)

    if cumulative:
        whv = np.cumsum(whv)
    if cumulative == 'reverse':
        whv = whv[::-1]
    if normalize:
        whv /= np.sum(wh[0])

    return wh[1], whv


def plot_histogram(edges, values, log=True, fmt=True, ylim=None, **kwargs):
    # pdb.set_trace()
    if log:
        plt.semilogy()
    if fmt is True:  # barplot
        bottom = min(values[values > 0]) / 2
        plt.bar(edges[:-1], values - bottom, bottom=bottom, **kwargs)
        plt.ylim([bottom, plt.ylim()[1]])
    else:
        plt.plot(.5 * (edges[:-1] + edges[1:]), values, fmt, **kwargs)


def expfit(x, y, sigma=None, p0=None, plot=False, **kwargs):

    try:
        popt, pcov = op.curve_fit(expd, x, y, sigma=sigma, p0=p0)  # ,
        if plot:
            yl = plt.ylim()
            plt.plot(x, expd(x, *popt))
            plt.ylim(yl)
        if np.any(pcov == np.inf):
            pcov = np.inf * np.ones((2, 2))
    except:
        print 'fit not successful'
        popt = np.array([np.nan, np.nan])

        pcov = np.inf * np.ones((2, 2))
    return popt, pcov


def cell_based_analysis(n, celldict, shape, spacing, mask, mindeg=0, minsize=0, maskerode=3):

    """
    calculates some properties of the segmented lacunae. If a mask is provided, only cells within the masked are evaluated. 
    To avoid artifacts from cells which are completely imaged, only those which do not extend to the border of the image, or outside the mask are evaluated this is desided by looking which cell extend outside of the mask after it has been eroded maskerode times. also small cells and cells with only few connections can be ignored.
    
    
    @param n network, used to determine degree of cells 
    @param celldict dictionary with label as key and tinacell.cell instace as values
    @param shape of the dataset
    @param spacing of the dataset
    @param mask ROI of the image data
   
    @parem mindeg minimal degree of lacunae to count as complete
    @param minsize minimal size of lacunae to count as complete (using spacing)
    @param maskerode  perform an erosion of the mask befor checking if cells are inside the mask.

    @returns result a dictionary with statistics of the lacuna
        'Lc.Nc' number of complete lacunae
        'Lc.sizem' mean size of lacune
        'Lc.sizestd' standard deviation of size of lacune
        'Lc.degm' mean degree of lacunae
        'Lc.degstd' standard deviation of degree of lacunae
        'Lc.vol' complete volume of the lacunae, including noncomplete 
        'Lc.rho_v' volumetric denisty
        'Lc.N' estimated number of lacune ('Lc.vol' / result['Lc.sizem')
        'Lc.dN'] = estimated uncertainty in number
        'Lc.rho_N' estimated number density 'Lc.N' / V
        'Lc.drho_N' estimated uncertainty in numberdensity
    @returns params sarameters of this function

    """

    celabel = np.zeros(shape, dtype=np.int)
    # X,Y,Z=np.mgrid[0:shape[1],0:shape[0],0:shape[2]]
    params = {'mindeg': mindeg, 'minsize': minsize}
    result = {}
    if len(celldict) == 0:
        print "no cells"
        return result, params
    for ce in celldict.values():
        celabel[ce.ioff[0]:ce.ioff[0] + ce.data.shape[0], ce.ioff[1]:ce.ioff[1] +
                ce.data.shape[1], ce.ioff[2]:ce.ioff[2] + ce.data.shape[2]] += (ce.node + 1) * ce.data

    m2 = morph.binary_erosion(mask ,iterations= maskerode)
    ace = np.unique(celabel)
    outce = np.unique(np.ravel(celabel[m2 == False]).tolist() + np.ravel(celabel[[0, -1], :, :]).tolist(
    ) + np.ravel(celabel[:, [0, -1], :]).tolist() + np.ravel(celabel[:, :, [0, -1]]).tolist())
    # wdi=np.histogramdd(np.array([tt.ravel(),rp.ravel(),dphi.ravel()]).T,(t2_edges,rp_edges,p_edges) , range=None, normed=False, weights=seglen)
    h = np.histogram(celabel, bins=range(max(ace) + 2))
    sizes = {}
    degs = {}
    vv = spacing[0] * spacing[1] * spacing[2]

    V=mask.sum()*vv
    # pdb.set_trace() #check ce-1
    for cel in ace:
        if cel == 0:
            continue
        de = n.degree()[cel - 1]
        si = h[0][cel] * vv

        if cel not in outce.tolist():
            si = h[0][cel] * vv
            celldict[cel - 1].inmask = True

            if de > mindeg and si > spacing[0] * spacing[1] * spacing[2]:
                degs[cel - 1] = de
                sizes[cel - 1] = h[0][cel]
                celldict[cel - 1].complete = True

            else:
                celldict[cel - 1].complete = False

        else:
            celldict[cel - 1].inmask = False
            if cel - 1 in celldict.keys():
                celldict[cel - 1].complete = False
        celldict[cel - 1].degree = de

        # celldict[cel-1].degree=n.degree()[int(celldict[cel-1].node[0])]
        # wcli=np.histogramdd(np.array([tt.ravel(),celabel.ravel()]).T,(t2_edges, range(celabel.max()+2)), range=None, normed=False)

    result['Lc.Nc'] = len(degs.keys())

    result['Lc.sizem'] = np.mean(sizes.values()) * vv
    result['Lc.sizestd'] = np.std(sizes.values()) * vv
    result['Lc.degm'] = np.mean(degs.values())
    result['Lc.degstd'] = np.std(degs.values())
    result['Lc.vol'] = (celabel[mask == 1] > 0).sum() * vv
    result['Lc.rho_v'] = result['Lc.vol'] / V

    result['Lc.N'] = result['Lc.vol'] / result['Lc.sizem']
    result['Lc.dN'] = result['Lc.N'] * \
        result['Lc.sizestd'] / result['Lc.sizem']
    result['Lc.rho_N'] = result['Lc.N'] / V
    result['Lc.drho_N'] = result['Lc.dN'] / V
    # pdb.set_trace()

    return result, params
