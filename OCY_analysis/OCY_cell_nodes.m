function [node,link,cell] = OCY_cell_nodes(node,link,cells)

w=size(cells,1);
l=size(cells,2);
h=size(cells,3);

cells=uint16(cells);
% get list of cell coordinates
cc=bwconncomp(cells);
n_cells = cc.NumObjects;

cell = [];
% generate cell structure
if(n_cells>0)
    for i=1:n_cells
        cells(cc.PixelIdxList{i})=i;
        cell(i).idx = cc.PixelIdxList{i};
        cell(i).nodes = [];
        [x y z]=ind2sub([w l h],cell(i).idx);
        cell(i).comx = mean(x);
        cell(i).comy = mean(y);
        cell(i).comz = mean(z);
    end;
end;
coun=1
cnn=zeros(n_cells,1);
nnc=zeros(n_cells,1);
for i=1:length(node)
    cn = max(cells(node(i).idx));
    if(cn)
        %stop
        node(i).cellnode = cn;
        cell(cn).nodes = i;
        cnn(coun)=cn;
	nnc(coun)=i;
        coun=coun+1;
        
    end;
end;
disp( coun)
%stop

