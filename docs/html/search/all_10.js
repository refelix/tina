var searchData=
[
  ['reduce_5falpha',['reduce_alpha',['../namespacetinanet.html#a4e0f679900be0a6855b3e1d6e859eac4',1,'tinanet']]],
  ['remotefolder',['remotefolder',['../classtina__main_1_1dataset.html#ac86715e927bdd13549b79bf24ff08678',1,'tina_main::dataset']]],
  ['remove_5fcells_5ffrom_5fnetwork',['remove_cells_from_network',['../namespacetinanet.html#a195a1b596d7aeea18ffc82eda69060f1',1,'tinanet']]],
  ['remove_5ffalse_5fnodes',['remove_false_nodes',['../namespacetinanet.html#a02820a42e03c02ae934586950f2d7279',1,'tinanet']]],
  ['remove_5fnetwork_5fblobs',['remove_network_blobs',['../namespacetinaimg.html#a52ed99e1454a9251123976ee388ff690',1,'tinaimg']]],
  ['remove_5fsmall',['remove_small',['../namespacetinaimg.html#aeff18978381c5b73e053ac19088d7cd8',1,'tinaimg']]],
  ['replaceedgesbysplines',['replaceEdgesBySplines',['../namespacetinanet.html#a4400f4171ea70834e8680891f546e0e7',1,'tinanet.replaceEdgesBySplines()'],['../namespacetinaui.html#a73e3faf9d5dc733c7e331e89bdc9a1f4',1,'tinaui.replaceEdgesBySplines()']]],
  ['resfolder',['resfolder',['../classtina__main_1_1dataset.html#aca46f7156dc00246a3f4393919e0b13a',1,'tina_main::dataset']]],
  ['result',['Result',['../classanalyze__ocn_1_1Result.html',1,'analyze_ocn']]],
  ['rnplot',['rnplot',['../classanalyze__ocn_1_1Result.html#a52b9782344554c7220f2d9ff6e02c7e8',1,'analyze_ocn::Result']]],
  ['rplot',['rplot',['../classanalyze__ocn_1_1Result.html#af8a66a116ea06ab9001d3eb287e53d55',1,'analyze_ocn::Result']]],
  ['rtnplot',['rtnplot',['../classanalyze__ocn_1_1Result.html#adec816d6fb705491e5009d1ebca92741',1,'analyze_ocn::Result']]],
  ['rtplot',['rtplot',['../classanalyze__ocn_1_1Result.html#a58f970aca1ebef3526479502090f2a62',1,'analyze_ocn::Result']]]
];
