# -*- coding: utf-8 -*-
# author: Felix Repp felix.repp@mpikg.mpg.de
# !/usr/bin/env python
import os
import numpy as np
import pdb
import networkx as nx
from scipy.interpolate import splprep, splev
import tina_main
import tinaskel
import lib
import sys
"""@package tinanet
%This module provides the tools work with the graph structure of the network


After converting the skeleton to a network structure, the following steps are advised:
-clean skeleton from nodes with degree 2
-Pruning of the network: remove short branches (artifacts ending at the surface)
-optionally merge short edges
-calculating orientations, etc.

"""


def check_get_attributes():
    """ old versions of networkx do not handle Multigraph as expected"""
    G = nx.MultiGraph()
    G.add_edge(0, 1, attr_dict={'t': 0})
    G.add_edge(0, 1, attr_dict={'t': 0})
    if len(nx.get_edge_attributes(G, 't').values()) != 2:
        pdb.set_trace()

check_get_attributes()


def filter_short_branches(n, shortedge=.5, iterate=True):
    """
    delete nodes with shorter than the threshold starting with the shortest edge. As removing an edge and the node of attatchment it might be needed to run this algorithmn more than once
    @param shortedge threshold below which
    """
    lenin = len(n)  # how many nodes to start with
    number_nodes = len(n) + 1
    m = 1
    # if iterate ==False this loop will be exited after first run
    while number_nodes != len(n):
        number_nodes = len(n)
        print 'filtering run #{}'.format(m)
        m += 1
        eds = []  # edge dicts
        ws = []  # edge weights
        for n1, n2, edgedict in n.edges(data=True):  # collect short branches
            if edgedict['weight'] < shortedge:
                if n.degree(n1) == 1 or n.degree(n2) == 1:
                    # if ws==0:
                    #    if n.degree(n1)==1:
                    #        n.remove_node(n1)
                    #    else:
                    #        n.remove_node(n2)
                    eds.append(edgedict)
                    ws.append(edgedict['weight'])
                    # pdb.set_trace()
        if not len(eds) == 0:

            # remove short edges first
            ws = np.array(ws)
            eds = np.array(eds)
            args = ws.argsort(axis=0)
            ws = ws[args]
            eds = eds[args]
            # ws,eds= (list(x) for x in zip(*sorted(zip(ws, eds))))
            # pdb.set_trace()
            for i in eds:

                try:  # in case node has been removed aready

                    # e=n[i]['n1']['n2']
                    n1 = n[i['n1']][i['n2']][0]['n1']
                    n2 = n[i['n1']][i['n2']][0]['n2']
                    t1 = n[n1]
                    t2 = n[n2]
                except:
                    # pdb.set_trace()
                    continue

                if n.degree(n1) == 1:
                    ni = n2
                    nj = n1
                else:
                    ni = n1
                    nj = n2
                # ni is the node that will remain, nj will be deleated

                """
                old... degree 2 nodes should be removed first
                if n.degree(ni)==2: if length was calculated wr
                    #n.remove_node(nj)
                    #print 'remove other'
                    continue
                """
                n.remove_node(nj)
                if n.degree(ni) != 2:  # ni remains a node
                    continue
                if n.degree(ni) == 0:  # if unconnected patch remove remaining node
                    n.remove_node(ni)
                    continue
                # else remove node with degree 2
                join_edges(n, ni)
    return n, {'shortedge':shortedge, 'iterate':iterate}


def merge_short_edges(n, thr, ignore_deadends=True):
    """
    merges clusters of nodes connected by edges shorter thr

    if ignore_deadends short edges which are dead ends will be spared
    """
    # make a copy and remove all links that remain untouched
    G = n.copy()
    remove_cells_from_network(G)
    ax = nx.get_node_attributes(n, 'x')
    ay = nx.get_node_attributes(n, 'y')
    az = nx.get_node_attributes(n, 'z')
    acn = nx.get_node_attributes(n, 'cellnode')
    for n1, n2, ed in G.edges(data=True):
        if ed['weight'] > thr:
            G.remove_edge(n1, n2)
    if ignore_deadends:
        deg = np.array(G.degree().values())
        key = np.array(G.degree().keys())
        G.remove_nodes_from(key[deg == 1].tolist())

    # find clusters of short connected networks and make one node out of it
    for nbunch in nx.connected_components(G):
        if len(nbunch) > 1:
            join_nodes(n, nbunch, ax, ay, az, acn)
    remove_false_nodes(n)
    return n, {'thr': thr, 'ignore_deadends': ignore_deadends}


def join_nodes(n, nbunch, ax, ay, az, acn):
    """  merges all nodes in nbunch to one new node
    """
    N = max(n.nodes())
    x = []
    y = []
    z = []
    edges = []
    cn = 0
    c = 0
    for node in nbunch:
        x.append(ax[node])
        y.append(ay[node])
        z.append(az[node])
        cn += (acn[node])
        try:
            edges.extend(n.edges([node], keys=True))
        except:
            pdb.set_trace()
    x = np.mean(x)
    y = np.mean(y)
    z = np.mean(z)
    n.add_node(N + 1, {'x': x, 'y': y, 'z': z, 'cell': False,
                       'cellnode': bool(cn), 'multinode': True})
    for n1, n2, key in edges:
        if n1 in nbunch and n2 in nbunch:
            continue
        else:
            ed2 = n[n1][n2][key]
            n1 = ed2['n1']
            n2 = ed2['n2']
            ed = {}
        if n1 in nbunch:
            ed['n1'] = N + 1
            ed['n2'] = n2
            ed['x'] = np.r_[x, ed2['x'][:-1]]
            ed['y'] = np.r_[y, ed2['y'][:-1]]
            ed['z'] = np.r_[z, ed2['z'][:-1]]
            ed['weight'] = tinaskel.get_length(ed['x'], ed['y'], ed['z'])
            n.add_edge(N + 1, n2, **ed)
        elif n2 in nbunch:
            ed['n2'] = N + 1
            ed['n1'] = n1
            ed['x'] = np.r_[ed2['x'][:-1], x]
            ed['y'] = np.r_[ed2['y'][:-1], y]
            ed['z'] = np.r_[ed2['z'][:-1], z]
            ed['weight'] = tinaskel.get_length(ed['x'], ed['y'], ed['z'])
            n.add_edge(n1, N + 1, **ed)
        else:
            pdb.set_trace()

        n.remove_edge(n1, n2, key)
    n.remove_nodes_from(nbunch)
    return N + 1


def join_edges(n, ni):
    """ join the two edges seperated by "false" knot with degree 2 joining the coordinates the right way. if edge is an isolated loop: do nothing
    @ param n network
    @ param ni node to be removed
    """
    if n.degree(ni) == 2:
        """
        for Multigraph m1,m2=n[ni].keys() does not work

        """
        if len(n[ni].keys()) != 1:  # loop through node with degree ==2

            m1, m2 = n[ni].keys()

            ed1 = n[m1][ni][0]
            ed2 = n[m2][ni][0]
        else:
            m1 = n[ni].keys()[0]
            if ni == m1:  # isolated loop
                return
            m2 = m1
            ed1 = n[m1][ni][0]
            ed2 = n[m1][ni][1]

        ed = {}
        ed['weight'] = ed1['weight'] + ed2['weight']
        # dependent on the orientation of the edges join coordinates

        if ed1['n2'] == ni:
            if ed2['n1'] == ni:
                ed['n1'] = m1
                ed['n2'] = m2
                ed['x'] = np.r_[ed1['x'], ed2['x'][1:]]
                ed['y'] = np.r_[ed1['y'], ed2['y'][1:]]
                ed['z'] = np.r_[ed1['z'], ed2['z'][1:]]
            else:
                # pdb.set_trace()
                ed['n1'] = m1
                ed['n2'] = m2
                ed['x'] = np.r_[ed1['x'], ed2['x'][-2::-1]]
                ed['y'] = np.r_[ed1['y'], ed2['y'][-2::-1]]
                ed['z'] = np.r_[ed1['z'], ed2['z'][-2::-1]]
        else:
            if ed2['n2'] == ni:
                ed['n1'] = m2
                ed['n2'] = m1
                ed['x'] = np.r_[ed2['x'], ed1['x'][1:]]
                ed['y'] = np.r_[ed2['y'], ed1['y'][1:]]
                ed['z'] = np.r_[ed2['z'], ed1['z'][1:]]
                # pdb.set_trace()
            else:
                # pdb.set_trace()
                ed['n1'] = m2
                ed['n2'] = m1
                ed['x'] = np.r_[ed2['x'][:1:-1], ed1['x']]
                ed['y'] = np.r_[ed2['y'][:1:-1], ed1['y']]
                ed['z'] = np.r_[ed2['z'][:1:-1], ed1['z']]

        n.add_edge(m1, m2, **ed)
        n.remove_node(ni)


def remove_false_nodes(n):
    """remove all nodes with degree==2 if not cellnodes
    joins attached edges by fixing coordinates

    """
    lab = np.array(n.degree().keys())  # maybe qicker n.nodes()
    deg = np.array(n.degree().values())
    cellnode = np.array(nx.get_node_attributes(
        n, 'cellnode').values(), dtype=np.bool)
    cell = np.array(nx.get_node_attributes(n, 'cell').values(), dtype=np.bool)
    inds = lab[(deg == 2) * ((cellnode == False) * (cell == False))]
    # pdb.set_trace()
    for ni in inds:
        join_edges(n, ni)  # 12982


# ocn.networkdict['masked2'][21379][21378][0]
#         tn.get_spline(ocn.networkdict['masked2'][21379][21378][0])

def get_spline(edgedict, sf=None, k=3, nest=-1, dist=1):
    """replaces points in an edge by a spline fit.  alsoprovides the orientations
    @param sf is the smoothnessfactor multilied by len (x) for parameter s 0.25 *spacing**2 for just pixel effects
    @param k spline order
    @param nest ontroles the amount on knot points
    @param dist distance of interpolated points
    see scipy.interplolate.splprep
    """
    x = edgedict['x']
    y = edgedict['y']
    z = edgedict['z']

    l = len(x)
    if l > 2:
        if edgedict['weight'] == 0:
            pdb.set_trace()
        if sf is None:
            s = (float(l)) * 0.25
        else:
            s = (float(l)) * sf
        try:
            ki = min(l - 1, k)
            # u=linspace(0,1,edgedict['weight']/.1+2)
            tckp, u = splprep([x, y, z], s=s, k=ki, nest=nest)
            unew = np.linspace(0, 1, round(edgedict['weight'] / dist) + 1)
            xnew, ynew, znew = splev(unew, tckp)
            dx, dy, dz = splev(unew, tckp, der=1)
        except:
            #pdb.set_trace()
            xnew, ynew, znew = [x, y, z]
            dx = np.array([np.nan] * len(x))
            dy = dx
            dz = dx
        edgedict['x'] = np.r_[x[0], xnew[1:-1], x[-1]]
        edgedict['y'] = np.r_[y[0], ynew[1:-1], y[-1]]
        edgedict['z'] = np.r_[z[0], znew[1:-1], z[-1]]

        direc = np.array([dx, dy, dz])
        s = np.sum(direc**2, axis=0)
        # edgedict['seglength']=np.linalg.linalg.norm(direc)
        if 0 not in s:
            direc /= np.sum(direc**2, axis=0)**.5
        else:
            pdb.set_trace

        edgedict['dx'] = direc[0]  # .tolist()[0]
        edgedict['dy'] = direc[1]  # .tolist()[0]
        edgedict['dz'] = direc[2]  # .tolist()[0]

        x = edgedict['x']
        y = edgedict['y']
        z = edgedict['z']

        ddx = np.r_[0, x[1:], 0] - np.r_[0, x[:-1], 0]
        ddy = np.r_[0, y[1:], 0] - np.r_[0, y[:-1], 0]
        ddz = np.r_[0, z[1:], 0] - np.r_[0, z[:-1], 0]
        ddx = (ddx[:-1] + ddx[1:]) / 2
        ddy = (ddy[:-1] + ddy[1:]) / 2
        ddz = (ddz[:-1] + ddz[1:]) / 2
        # if np.size(ddy)!=np.size(x):
        #    pdb.set_trace()
        # edgedict['ddx']=ddx
        # edgedict['ddy']=ddy
        # edgedict['ddz']=ddz
        edgedict['seglength'] = np.sqrt(
            np.array(ddx)**2 + np.array(ddy)**2 + np.array(ddz)**2)
        # edgedict['seglength']=np.sqrt(np.array(ddx)**2+np.array(ddy)**2+np.array(ddz)**2)
        edgedict['weight'] = np.sum(edgedict['seglength'])
    else:
        if edgedict['weight'] > 0:
            edgedict['dx'] = np.array(2 * [(x[1] - x[0]) / 2])
            edgedict['dy'] = np.array(2 * [(y[1] - y[0]) / 2])
            edgedict['dz'] = np.array(2 * [(z[1] - z[0]) / 2])
            edgedict['seglength'] = np.array(2 * [(edgedict['weight']) / 2])
        else:
            edgedict['dx'] = np.array(l * [0])
            edgedict['dy'] = np.array(l * [0])
            edgedict['dz'] = np.array(l * [0])
            edgedict['seglength'] = np.array(l * [0])

    # scipy.interpolate.spalde(np.linspace(0,1,l*N).tolist(), tckp)

    # except SystemError:
    #   print('propably multiple time similiar points in edge'+str(edgedict['n1'])+' '+str(edgedict['n2']))


def replaceEdgesBySplines(G, sf=None, k=3, nest=-1, dist=1):
    """replace the voxels of all edges by a smothing spline interpolation

    @param G networkx graph
    @param sf  sf is the smoothnessfactor multilied by len (x) for parameter s 0.25 *spacing**2 for just pixel effects
    @param k spline order
    @param nest ontroles the amount on knot points
    see scipy.interplolate.splprep
    @param dist distance of intepolatet points

    """
    print 'replace edges by splines'
    for n1, n2, edgedict in G.edges(data=True):
        get_spline(edgedict, sf=sf, k=k, nest=nest, dist=dist)
    G.graph['splines'] = True
    print 'done'


def get_orientation_and_straightness(G):
    """caclulates the orientation of the edges as straight lines"""
    print 'get orientation'
    i = 00
    N = G.number_of_edges()
    Ns = N / 100
    for n1, n2, edgedict in G.edges(data=True):
        i += 1
        if i % (Ns * 5) == 0:
            sys.stdout.write("\rpercentage: " + str(100 * i / N) + "%")
            sys.stdout.flush()
            
        x = edgedict['x']
        y = edgedict['y']
        z = edgedict['z']
        dx = x[-1] - x[0]
        dy = y[-1] - y[0]
        dz = z[-1] - z[0]

        v = [dx, dy, dz]

        d = np.sqrt(v[0]**2 + v[1]**2 + v[2]**2)

        edgedict['straightness'] = d / edgedict['weight']
        edgedict['direction'] = [dx / d, dy / d, dz / d]

        if nx.get_node_attributes(G, 'cell')[n1] or nx.get_node_attributes(G, 'cell')[n2]:
            edgedict['seglength'] = np.array(len(edgedict['x']) * [0])
        # edgedict['weight']=np.sum(edgedict['seglength'])
    print 'done'


def get_euler_angeles(v, R, L, normalized=False, project=True, tol=1e-10):
    """calculate orientation of vectors v with respect to R and L
    @param v edge direction(s)
    @param R orientation 1
    @param L orientation 2
    @param normalized set True if for sure all Vectors have length 1 ,
    @param project if True project orientation into hemisphere
    @param tol check if np.dot(R.T,L).sum()<=1e-10
    """

    if not normalized:
        vn = v / np.sqrt((v**2).sum(axis=0))
        Rn = R / np.sqrt((R**2).sum(axis=0))
        Ln = L / np.sqrt((L**2).sum(axis=0))
    else:
        vn = v
        Rn = R
        Ln = L

    if np.shape(Rn) == (3,)and np.shape(vn) == (3,):

        if project:
            vn *= np.dot(Rn.T, vn)
        theta = np.arccos((np.dot(Rn.T, vn)))
    elif np.shape(Rn) < np.shape(vn):
        # pdb.set_trace()
        Rn = Rn[:, np.newaxis]
        if project:
            vn *= np.sign(np.dot(Rn.T, vn))
        theta = np.arccos((np.dot(Rn.T, vn)))
    else:
        if project:
            vn *= np.diag(np.sign(np.dot(Rn.T, vn)))
        theta = np.arccos((np.diag(np.dot(Rn.T, vn))))

    if np.shape(Ln) < np.shape(vn)and np.shape(Ln) == (3,):
        Ln = Ln[:, np.newaxis]

    if np.any(L):
        # pdb.set_trace()
        if len(R.shape) > 1:
            di = np.diagonal(np.dot(R.T, L))
        
            if di[np.isnan(di) == False].max() >= tol:
                print 'warning angles' + str(np.dot(R.T, L).sum(axis=1).mean())
                # pdb.set_trace()
        elif np.dot(R.T, L)>= tol:
            print 'warning angles' + str(np.dot(R.T, L).sum(axis=1).mean())
            # pdb.set_trace()
        T = np.cross(Rn.T, Ln.T)
        tp = np.dot(T, vn)
        lp = np.dot(Ln.T, vn)
        #for p in [tp,lp]:
        if tp.size != v.size/3.:
                tp=np.diag(tp)
        if lp.size != v.size/3.:
                lp=np.diag(lp)
        #pdb.set_trace()
        alpha = np.arctan2(tp, lp)
        """if np.shape(Rn) < np.shape(vn) or np.shape(vn) == (3,):
            tp = np.dot(T, vn)
            # tproject=np.arccos(tp)
            lp = np.dot(Ln.T, vn)
            # lproject=np.arccos(lp)
            alpha = np.arctan2(tp, lp)
        else:
            tp = np.diag(np.dot(T, vn))
            # tproject=np.arccos(tp)
            lp = np.diag(np.dot(Ln.T, vn))
            # lproject=np.arccos(lp)
            alpha = np.arctan2(tp, lp)
         """   
    else:
        alpha = None
    # pdb.set_trace()
    #if isinstance(theta, type(np.array(0.))) and np.shape(theta)[0] == 1:
    #    theta = theta[0]
    #    alpha = alpha[0]
    #if np.isnan(alpha).sum():
    #    pdb.set_trace()
    return theta.ravel(), alpha.ravel()


def project_orientation(G, vec1, vec2=None, project=True):
    """
    calculate the orientation(euler angels) with respect to an vector of all edges
    @param vec1  the reference direction
    @param vect2 direction orthogonal to vect1
    @param project if True, orientation is projected into hemisphere
    """
    vec1 /= np.linalg.linalg.norm(vec1)
    vec2 /= np.linalg.linalg.norm(vec2)
    if G.graph['splines'] != True:
        print 'replace by splines first'
    else:
        # pdb.set_trace()
        if not nx.get_edge_attributes(G, 'direction'):
            get_orientation_and_straightness(G)
        for n1, n2, edgedict in G.edges(data=True):
            dx = edgedict['dx']
            dy = edgedict['dy']
            dz = edgedict['dz']
            DX, DY, DZ = edgedict['direction']
            v = np.array([dx, dy, dz])
            vG = np.array([DX, DY, DZ])
            # if edgedict['weight']==0:
            #    pdb.set_trace()
            theta, alpha = get_euler_angeles(v, vec1, vec2, project=project)
            edgedict['theta'] = theta
            edgedict['alpha'] = alpha
            theta, alpha = get_euler_angeles(vG, vec1, vec2, project=project)
            edgedict['thetaEdge'] = theta
            edgedict['alphaEdge'] = alpha


def get_cylindrical_projection(G, cm, project=True):
    """get projection with respect to center axis of a cylinder
    @param G networkx Graph
    @param cm coordinates of two points on cylinder center axis
    @param project as direction of the edges is not defined, by default all segments of the edges are projected into one hemishere
    """
    print 'cylindical projection'
    if G.graph['splines'] != True:
        print 'replace by splines first'
    else:
        # pdb.set_trace()
        # if not nx.get_edge_attributes(G,'direction'):
        #    get_orientation_and_straightness(G)
        N = G.number_of_edges()
        i = 0
        # xa=[]
        # ya=[]
        # za=[]
        N = len(G.edges())
        Ns = N / 100
        for n1, n2, edgedict in G.edges(data=True):
            if i % (Ns * 5) == 0:
                sys.stdout.write("\rpercentage: " + str(100 * i / N) + "%")
                sys.stdout.flush()
            i += 1
            x = edgedict['x']
            y = edgedict['y']
            z = edgedict['z']
            # xa.extend(x)
            # ya.extend(y)
            # za.extend(z)
            dx = edgedict['dx']
            dy = edgedict['dy']
            dz = edgedict['dz']
            if edgedict['weight'] == 0:
                edgedict['theta'] = np.array([np.nan] * len(x))
                edgedict['thetaEdge'] = np.nan
                # if not np.isfinite(edgedict['thetaEdge']):
                #    #pdb.set_trace()
                #    edgedict['thetaEdge']=-0.1
                edgedict['alpha'] = np.array([np.nan] * len(x))
                edgedict['alphaEdge'] = np.nan
                continue
            v = np.array([dx, dy, dz])
            v /= np.sum(v**2, axis=0)  # [:,np.newaxis]
            p = np.array([x, y, z])
            l = (np.array(cm[1], dtype=float) - np.array(cm[0]))[:, np.newaxis]
            l /= np.sqrt((l**2).sum(axis=0))

            d0 = p - np.array(cm[0])[:, np.newaxis]
            r = d0 - np.dot(d0.T, l).T * l  # [:,np.newaxis]

            r /= np.sqrt((r**2).sum(axis=0))  # [:,np.newaxis]
            # t=np.cross(r,l)
            # pdb.set_trace()

            theta, alpha = get_euler_angeles(
                v, r, l, normalized=True, project=project)

            edgedict['thetaEdge'] = np.nan
            edgedict['alphaEdge'] = np.nan
            edgedict['theta'] = theta
            edgedict['alpha'] = alpha
        # print np.mean(xa)
        # print np.mean(ya)
        # print np.mean(za)
        # pdb.set_trace()
    print 'done'


def reduce_alpha(G):
    """reduce alpha to one [0,pi/2]"""

    for n1, n2, edgedict in G.edges(data=True):
        alpha = edgedict['alpha']
        alpha[alpha > np.pi / 2] -= np.pi
        alpha[alpha < -np.pi / 2] += np.pi
        alpha = np.abs(alpha)
        edgedict['alpha'] = alpha


def set_distance_to_node(graph, Node):
    pre, node_distances = nx.dijkstra_predecessor_and_distance(graph, Node)
    unconnected = []
    # pdb.set_trace()
    for edge in graph.edges(data=True):
        edge[2]['distance_to_node'] = None
    N = graph.number_of_nodes()
    Ns = N / 100
    for i, node in enumerate(graph.nodes()):
                # if node in [2,11]:
                #    pdb.set_trace()
        if i % (Ns * 5) == 0:

            sys.stdout.write("\rpercentage: " + str(100 * i / N) + "%")
            sys.stdout.flush()

        try:
            far = node_distances[node]
        except:

            unconnected.append(node)
            continue
        graph.node[node]['distance_to_node'] = far
        # pdb.set_trace()
        """
                if node==Node:
                    continue
                prede=pre[node]
                if len (prede)>1:
                    pdb.set_trace()
                clo=node_distances[prede[0]]
                edge=graph[prede[0]][node]
                #if prede[0] in [2,11] and node in [2,11]:
                #    pdb.set_trace()
                for i in edge.keys():
                    edge[i]['dtnode']=Node
                    #edgedict=
                    x=edge[i]['x']
                    y=edge[i]['y']
                    z=edge[i]['z']
                    dx=np.array([0]+x[1:])-np.array([0]+x[:-1])
                    dy=np.array([0]+y[1:])-np.array([0]+y[:-1])
                    dz=np.array([0]+z[1:])-np.array([0]+z[:-1])
                    #pre+np.cumsum(sqrt(dx**2+dy**2+dz**2))

                    if edge[i]['n1']==pre:

                        edge[i]['distance_to_node']=clo+np.cumsum(np.sqrt(dx**2+dy**2+dz**2))
                    else:
                        edge[i]['distance_to_node']=far-np.cumsum(np.sqrt(np.array(dx)**2+np.array(dy)**2+np.array(dz)**2))
               """

    for node in unconnected:
        graph.node[node]['distance_to_node'] = -1
    for edge in graph.edges(data=True):
        interp_value(graph, edge[2], 'distance_to_node')


def interp_value(graph, edgedict, key):
    x = edgedict['x']
    y = edgedict['y']
    z = edgedict['z']
    dx = np.array([0] + x[1:]) - np.array([0] + x[:-1])
    dy = np.array([0] + y[1:]) - np.array([0] + y[:-1])
    dz = np.array([0] + z[1:]) - np.array([0] + z[:-1])
    cs = np.cumsum(np.sqrt(np.array(dx)**2 +
                           np.array(dy)**2 + np.array(dz)**2))
    v1 = graph.node[edgedict['n1']][key]
    v2 = graph.node[edgedict['n2']][key]

    edgedict[key] = v1 + cs / cs[-1] * (v2 - v1)
    """
            for n2 in graph[node].keys():
                edge=graph[node][n2]
                for i in edge.keys():
                    x=edge[i]['x']
                    y=edge[i]['y']
                    z=edge[i]['z']
                    dx=np.array([0]+x[1:])-np.array([0]+x[:-1])
                    dy=np.array([0]+y[1:])-np.array([0]+y[:-1])
                    dz=np.array([0]+z[1:])-np.array([0]+z[:-1])
                    edge[i]['distance_to_node']=0*np.array(dx)-1
        #pdb.set_trace()
        for edge in graph.edges(data=True):

            if edge[2]['distance_to_node']==None:

                        d2=graph.node[edge[2]['n2']]['distance_to_node']
                        d1=graph.node[edge[2]['n1']]['distance_to_node']
                        x=edge[2]['x']
                        y=edge[2]['y']
                        z=edge[2]['z']
                        dx=np.array([0]+x[1:])-np.array([0]+x[:-1])
                        dy=np.array([0]+y[1:])-np.array([0]+y[:-1])
                        dz=np.array([0]+z[1:])-np.array([0]+z[:-1])

                        if  d2>d1 :
                            edge[2]['distance_to_node']=d1+np.cumsum(np.sqrt(np.array(dx)**2+np.array(dy)**2+np.array(dz)**2))
                        else:
                            edge[2]['distance_to_node']=d2+np.cumsum(np.sqrt(np.array(dx)**2+np.array(dy)**2+np.array(dz)**2))

"""


def get_masked_network_edge(dataset, graph, mask, value=1, bigger_value=False):
    nodes_inside = []
    n = nx.MultiGraph()
    for node, nd in graph.nodes(data=True):
        x = nd['x']
        y = nd['y']
        z = nd['z']

        xi, yi, zi = lib.get_index_from_pos(dataset, x, y, z)

        if mask[xi, yi, zi] == value:
            n.add_node(node, attr_dict=nd)

    for n1, n2, ed in graph.edges(data=True):
        n1 = ed['n1']
        n2 = ed['n2']
        
        x = np.array(ed['x'])
        y = np.array(ed['y'])
        z = np.array(ed['z'])
        xi, yi, zi = lib.get_index_from_pos(dataset, x, y, z)

        oi = (mask[xi, yi, zi] == value).astype(int)
        if np.sum(oi) == 0:
            continue
        if np.sum(oi) == len(xi):
            n.add_edge(n1, n2, attr_dict=ed)
        if np.sum(oi) < xi.shape:
            if np.sum(oi) == 1:
                continue
            else:
                # pdb.set_trace()
                oip = np.where(oi[:-1] - oi[1:] == 1)[0].tolist()
                oin = np.where(oi[:-1] - oi[1:] == -1)[0].tolist()

                if len(oip) > 0:
                    oina = np.array(oin)
                    added = 0
                    for op in oip:
                        oins = oina[oina < op]
                        if len(oins):
                            i = oins[-1]
                            oin.remove(int(oins[-1]))

                        else:
                            i = -1

                        if not op - i >= 2:
                            continue
                        else:
                            if i > -1:
                                n.add_node(max(n) + 1, {'x': x[oins[-1] + 1], 'y': y[oins[-1] + 1], 'z': z[
                                           oins[-1] + 1], 'cell': False, 'cellnode': False, 'multinode': False})
                                m1 = max(n)

                            else:
                                m1 = n1
                            n.add_node(max(n) + 1, {'x': x[op], 'y': y[op], 'z': z[
                                       op], 'cell': False, 'cellnode': False, 'multinode': False})
                            m2 = max(n)
                            added += 1

                        xn = x[i + 1:op + 1]
                        yn = y[i + 1:op + 1]
                        zn = z[i + 1:op + 1]

                        w = length = tinaskel.get_length(xn, yn, zn)
                        if w == 0:
                            pdb.set_trace()
                        edgedict = {'n1': m1, 'n2': m2, 'x': xn,
                                    'y': yn, 'z': zn, 'weight': w}
                    if 'dx' in ed.keys():
                        get_spline(edgedict, dist=min(ed['seglength']))
                    # pdb.set_trace()
                    if added:
                        if m1 not in n or m2 not in n:
                            pdb.set_trace()
                        n.add_edge(m1, m2, attr_dict=edgedict)
                if len(oin):
                    if len(oin) > 1:
                        pdb.set_trace()
                        # should not happen
                    if np.size(x[oin[0]+1:])<2:
                        continue
                    n.add_node(max(n) + 1, {'x': x[oin[0]+1], 'y': y[oin[0]+1], 'z': z[
                               oin[0]+1], 'cell': False, 'cellnode': False, 'multinode': False})

                    xn = x[oin[0]+1:]
                    yn = y[oin[0]+1:]
                    zn = z[oin[0]+1:]

                    w = length = tinaskel.get_length(xn, yn, zn)
                    if w == 0:
                        pdb.set_trace()
                    edgedict = {'n1': max(n), 'n2': n2,
                                'x': xn, 'y': yn, 'z': zn, 'weight': w}
                    if 'dx' in ed.keys():
                        get_spline(edgedict, dist=min(ed['seglength']))
                    # pdb.set_trace()
                    n.add_edge(n2, max(n), attr_dict=edgedict)
            #if max(n) ==14603:
            #    pdb.set_trace()
    """
    stop
    i=0
    for n1,n2,ed in graph.edges(data=True):

        m1= n1 in nodes_inside
        #if n1==10295 or  n2 == 8615:
        #    pdb.set_trace()
        m2=n2 in nodes_inside
        if int(m1)+int(m2)==1:
            xn=[]
            yn=[]
            zn=[]
            rprojn=[]
            x=np.array(ed['x'])
            y=np.array(ed['y'])
            z=np.array(ed['z'])
            if 'theta' in ed.keys() :
                rp=np.array(ed['theta'])
            xi,yi,zi=lib.get_index_from_pos(self,x,y,z)
            for ni in range(xi.shape[0]):
                if mask[xi[ni],yi[ni],zi[ni]]==value:
                    xn.append(x[ni])
                    yn.append(y[ni])
                    zn.append(z[ni])
                    if 'theta' in ed.keys():
                        if np.isnan(rp).sum():
                           None
                        else:
                          rprojn.append(rp[ni])
            if len (xn)<2:
                continue

            if m1:

                if n1==ed['n1']:
                    na=n1
                    nb=N+i
                    nnx=xn[-1]
                    nny=yn[-1]
                    nnz=zn[-1]

                else:
                    na=N+i
                    nb=n1
                    nnx=xn[0]
                    nny=yn[0]
                    nnz=zn[0]

            else:
                    if n1==ed['n1']:
                    na=N+i
                    nb=n2
                    nnx=xn[0]
                    nny=yn[0]
                    nnz=zn[0]

                else:
                    na=n2
                    nb=N+i
                    nnx=xn[-1]
                    nny=yn[-1]
                    nnz=zn[-1]
            #if  N+i==10652:#na==10295 or na==10295 :
            #    pdb.set_trace()

            n.add_node(N+i,{'x':nnx,'y':nny,'z':nnz,'cell':False,'cellnode':False,'multinode':False})
            i+=1

            w=length=tinaskel.get_length(xn,yn,zn)
            if 'theta' in ed.keys() :
                   edgedict={'n1':na,'n2':nb,'x':xn,'y':yn,'z':zn,'weight':w,'theta':rprojn}
            else:
                   edgedict={'n1':na,'n2':nb,'x':xn,'y':yn,'z':zn,'weight':w}
            if 'dx' in ed.keys():
                get_spline(edgedict,dist=min(edgedict['seglength']))
            if na not in n.nodes() or nb not in n.nodes():
                pdb.set_trace()
            n.add_edge(na,nb,attr_dict=edgedict)
    """
    return n


def get_masked_network(self, graph, mask, value=1, bigger_value=False):
    nodes_inside = []
    
    x = nx.get_node_attributes(graph, 'x')
    y = nx.get_node_attributes(graph, 'y')
    z = nx.get_node_attributes(graph, 'z')
    xi, yi, zi = lib.get_index_from_pos(
        self, x.values(), y.values(), z.values())

    for ni in range(xi.shape[0]):
        if bigger_value:
            if mask[xi[ni], yi[ni], zi[ni]] >= value:
                nodes_inside.append(x.keys()[ni])
        else:
            if mask[xi[ni], yi[ni], zi[ni]] == value:
                nodes_inside.append(x.keys()[ni])
    n = nx.subgraph(graph, nodes_inside)
    return n


def assignEdges(self, graph):
    from operator import itemgetter

            # for node in graph.nodes():
            #    self.dendrites.ddict[node]={'distance_to_cell':{}}
    self.isolated_nodes = []
    #pdb.set_trace()
    for c in self.celllist:

        try:

            c.node_predecessor, c.node_distances = nx.dijkstra_predecessor_and_distance(
                graph, c.node)

        except nx.NetworkXNoPath as inst:
            print inst
            N = 1
        for node in graph.nodes():
            
            if node in c.node_distances.keys():
                        # pdb.set_trace()
                dd = graph.node[node]
                if 'distance_to_cell' not in dd.keys():
                    dd['distance_to_cell'] = {}
                dd['distance_to_cell'][c.node] = c.node_distances[node]
                #pdb.set_trace()
            else:
                graph.node[node]['closest_cell'] = None
                graph.node[node]['distance_to_cell']=None
                graph.node[node]['distance_closest_cell'] = None
    N = len(graph.nodes())
    Ns = N / 100
    #pdb.set_trace()
    for i, node in enumerate(graph.nodes()):
        if i % (Ns * 5) == 0:
            sys.stdout.write("\rpercentage: " + str(100 * i / N) + "%")
            sys.stdout.flush()
            #pdb.set_trace()
        
        if graph.node[node]['distance_to_cell'] is None:
            self.isolated_nodes.append(node)
            #graph.node[node]['closest_cell']
            continue
        #pdb.set_trace()    
        
        s1 = sorted(
            graph.node[node]['distance_to_cell'].iteritems(), key=itemgetter(1))
        # pdb.set_trace()self.celldict[l].connected_with[m]
        if node == [s1[0][0]]:
            continue
        if len(s1) >= 1:
            
            pre = self.celldict[s1[0][0]].node_predecessor[node][0]

            far = self.celldict[s1[0][0]].node_distances[node]
            clo = self.celldict[s1[0][0]].node_distances[pre]
            graph.node[node]['distance_closest_cell'] = far
            #pdb.set_trace()
            # edge=graph[pre][node]['line_graph']
            # l=self.network[pre][node]['weight']
            # pdb.set_trace()
            # if line.nodes()[0]==pre:
            #    s=0
            #    for m in line.edges():
            #        s+=self.pixelnet[m[0]][m[1]]['weight']
            #        self.pixelnet[m[0]][m[1]]['distance_to_cell']=clo+s
            # else:
            #    s=0
            #    for m in line.edges():
            #        s+=self.pixelnet[m[0]][m[1]]['weight']
            #        self.pixelnet[m[0]][m[1]]['distance_to_cell']=far-s
            
        # nbr=self.celldict[s1[0][0]].predecessor][node]
        # line=graph[node][nbr][['line_graph']
        # interp=interp1d[[s1[0][1],s1[0][1]],graph[s1[0][0]][s1[1][0]]['weight']
        # p=nx.dijkstra_predecessor_and_distance(line,s1[0][0])[1].values()
        # dnew=interp(p)
        

        self.celldict[s1[0][0]].nearest_nodes.append(node)
        graph.node[node]['closest_cell'] = s1[0][0]
        #pdb.set_trace()
        if len(s1) > 1:

            self.celldict[s1[0][0]].connected_with[s1[1][0]].append(node)
            self.celldict[s1[1][0]].connected_with[s1[0][0]].append(node)

     
        #pdb.set_trace()
           
    for edge in graph.edges(data=True):
        if graph.node[edge[0]] in self.isolated_nodes:
            edge[2]['distance_closest_cell'] = np.array([np.nan]*len(edge[2]['x']))#-np.ones(len(edge[2]['x']))
            
            continue
            
        if graph.node[edge[0]] ['cell'] is True  or  graph.node[edge[1]] ['cell']is True :
            edge[2]['distance_closest_cell'] = np.array([0]*len(edge[2]['x']))
        elif nx.get_node_attributes(graph, 'closest_cell')[edge[2]['n2']] == nx.get_node_attributes(graph, 'closest_cell')[edge[2]['n1']]and  nx.get_node_attributes(graph, 'closest_cell')[edge[2]['n2']] is not None:

            d2 = graph.node[edge[2]['n2']]['distance_closest_cell']
            d1 = graph.node[edge[2]['n1']]['distance_closest_cell']
            x = edge[2]['x'].tolist()
            y = edge[2]['y'].tolist()
            z = edge[2]['z'].tolist()
            dx = np.array([0] + x[1:]) - np.array([0] + x[:-1])
            dy = np.array([0] + y[1:]) - np.array([0] + y[:-1])
            dz = np.array([0] + z[1:]) - np.array([0] + z[:-1])
            
            if d2 > d1:
                edge[2]['distance_closest_cell'] = d1 + \
                    np.cumsum(np.sqrt(np.array(dx)**2 +
                                      np.array(dy)**2 + np.array(dz)**2))
            else:
                edge[2]['distance_closest_cell'] = d2 + \
                    np.cumsum(np.sqrt(np.array(dx)**2 + np.array(dy)**2 + np.array(dz)**2))
        else:
            
            edge[2]['distance_closest_cell'] = np.array([np.nan]*len(edge[2]['x']))#-np.ones(len(edge[2]['x']))len(edge[2]['x']
        if size(edge[2]['x'])!= size(edge[2]['distance_closest_cell']):
            pdb.set_trace()
    for l in self.celldict.keys():
        self.celldict[l].nearest_graph = graph.subgraph(
            self.celldict[l].nearest_nodes)
    import itertools
    for l, m in itertools.combinations(self.celldict.keys(), 2):

        self.celldict[l].connected_with_graph[m] = graph.subgraph(
            self.celldict[l].connected_with[m])

        self.celldict[m].connected_with_graph[l] = graph.subgraph(
            self.celldict[m].connected_with[l])

    return


def get_weighted_degree(n):

    wdeg = {}
    for node in nx.get_node_attributes(n, keyword).keys():
        s = 0
        for n1, n2, ed in n.edges(node):
            s += ed[edge_weights]
        wdeg[node] = s
    return wdeg


def get_cellconnected_network(self, cell1, cell2, graph=None):
    if (graph is None):
        graph = self.Network
    nodes = list(self.celldict[cell1].connected_with[cell2])
    inttype = np.array([1, 2])[0].__class__
    pdb.set_trace()
    for node in self.celldict[cell1].connected_with[cell2]:
            # pdb.set_trace()
        if nx.get_node_attributes(graph, 'cell')[node] == False:
            p = self.celldict[cell1].node_predecessor[node][0]
            print p
            while (p.__class__ == inttype and p not in nodes):

                nodes.append(p)
                p = self.celldict[cell1].node_predecessor[p][0]
                print p
                print 1

            p = self.celldict[cell2].node_predecessor[node][0]
            while (p.__class__ == inttype and p not in nodes):
                if p.__class__ == inttype:
                    nodes.append(p)
                    p = self.celldict[cell2].node_predecessor[p][0]
                    print p
                    print 2

    self.celldict[cell1].connected_with_graph[cell2] = graph.subgraph(nodes)
    self.celldict[cell2].connected_with_graph[cell1] = graph.subgraph(nodes)

    return


def remove_cells_from_network(graph):
    celd = nx.get_node_attributes(graph, 'cell')
    for k in celd.keys():
        if celd[k]:
            graph.remove_node(k)
"""
def get_edge_props(n):
    #v=[]

    #w=[]
    print 'get_edge_probs'
    result={}
    for n1,n2,edgedict in n.edges(data=True):
        if edgedict['weight']>0:
            for key in edgedict.keys():
                if key not in result.keys():
                   result[key]=[]
                value=edgedict[key]
                if type(value)==list:
                    result [key].extend(value)
                elif type(value)==np.ndarray:
                   result [key].extend(value.tolist())
                else:
                   result [key].append(value)
    print 'done'

    return result
   """


def get_edge_properties(G, keys=None):
    seg_keys = []  # for subdevisions properties
    whole_keys = []  # for properties describing complete edge
    for i, e in enumerate(G.edges(data=True)):
        ed = e[2]
        if i == 0:

            if keys is None:
                keys = ed.keys()
            for k in keys:
                if np.size(ed[k]) == 1:
                    whole_keys.append(k)
                elif np.size(ed[k]) > 1:
                    if k == 'direction':
                        print 'fix direction'
                    else:
                        seg_keys.append(k)
                else:
                    pdb.set_trace()
            whole_dict = {}
            seg_dict = {}
            for k in whole_keys:
                whole_dict[k] = []
            for k in seg_keys:
                seg_dict[k] = []
        for k in whole_keys:
            whole_dict[k].append(ed[k])
            # if np.size(ed[k])!=1:
            #    pdb.set_trace()
        l = 0
        for k in seg_keys:
            v = ed[k]
            if type(v) == np.ndarray:
                v = v.tolist()
            if (not len(v) == l) and l > 0:
                pdb.set_trace()
            l = len(v)
            seg_dict[k].extend(v)
    return whole_dict, seg_dict


def add_functions_to_node_dict(G):
    deg = G.degree()
    G2 = nx.Graph(G)
    cc = nx.clustering(G2)
    nx.set_node_attributes(G, 'degree', deg)

    nx.set_node_attributes(G, 'clustering', cc)


def get_node_properties(G, keys=None):
    ndict = {}
    add_functions_to_node_dict(G)
    # pdb.set_trace()
    ndict['node'] = []
    for i, n in enumerate(G.nodes(data=True)):
        if keys is None:
            keys = n[1].keys()
            if 'idx' in keys:
                keys.remove('idx')

        ndict['node'].append(n[0])
        for k in keys:
            if i == 0:
                ndict[k] = []
            try:
                ndict[k].append(n[1][k])
            except:
                print str(k) + " not in dict for node " + str(n[0])
    return ndict


def get_nw_hist(G, keys, edges, weighted=True):
    """
    calculate histogram of edges corresponding to attributes defind by keys, and edges
    
    """
    if weighted:
        keys.append('seglength')
    whole_dict, seg_dict = get_edge_properties(G, keys)

    attributes = []
    for k in keys[:-1]:
        attributes.append(seg_dict[k])
    # if np.isnan(attributes).sum() or np.isinf(attributes).sum():
    #   pdb.set_trace()
    if weighted:
        w = np.histogramdd(np.array(attributes).T, bins=edges,
                       range=None, normed=False, weights=seg_dict['seglength'])
    else:
        w = np.histogramdd(np.array(attributes).T, bins=edges,
                       range=None, normed=False)

    return w


def get_inhomogenity(G, shape, mask=None, cells=None, V=400, origin=(0, 0, 0), spacing=(1, 1, 1), prefix='ihomo_'):
    """calculated the density of qubic subvolumes with size V of the dataset based on a the volume with imgdict['mask']==1 and imgdict['cells']==0.
    
    @param Network, needs to have edge length, e.g. be smoothed
    @param shape of the imagedata
    @param mask if None is provided np.ones is used
    @param cells gets excluded from mask
    @param V size of the subvolume
    @param origin, spacing to convert from indeces to coordinates
    @param minweight
    @param dataset

    """
    

    xmin = origin[0]
    ymin = origin[1]
    zmin = origin[2]
    xmax = xmin + spacing[0] * shape[0]
    ymax = ymin + spacing[1] * shape[1]
    zmax = zmin + spacing[2] * shape[2]

    dr = float(V)**(1. / 3)

    dx = np.arange(xmin, xmax+dr, dr)
    dy = np.arange(ymin, ymax+dr, dr)
    dz = np.arange(zmin, zmax+dr, dr)

    L, edges = get_nw_hist(G, ['x', 'y', 'z'], [dx, dy, dz])
    if mask is None:
        mask = np.ones(shape, dtype=np.uint8)
    else:
        shape = mask.shape
    if cells is not None:
        mask = np.logical_and(mask == 1, cells == False)
    X, Y, Z = np.mgrid[:shape[0], :shape[1], :shape[2]]
    Y = Y.astype(np.float) * spacing[1]
    X = X.astype(np.float) * spacing[0]
    Z = Z.astype(np.float) * spacing[2]
    Y += origin[1]
    X += origin[0]
    Z += origin[2]
    u, edges = lib.histogramdd(np.array([X.ravel(), Y.ravel(), Z.ravel()]).T, [
                               dx, dy, dz], weights=mask.ravel(), rrange=None, normed=False, parts=10)
    # pdb.set_trace()
    u *= spacing[0] * spacing[1] * spacing[2]
    rho= L / u
    for i in range(L.shape[-1]):
        #pdb.set_trace()
        np.savetxt(prefix+'rho{}.csv'.format(i), rho[:,:,i], delimiter=",")
        np.savetxt(prefix+'vol{}.csv'.format(i), u[:,:,i], delimiter=",")
    
    np.savetxt(prefix+'edges_x-voxel.csv', ((edges[1]- origin[1]) / spacing[1]).astype(np.uint16) , delimiter=",")
    np.savetxt(prefix+'edges_y-voxel.csv', ((edges[0]- origin[0]) / spacing[0]).astype(np.uint16) , delimiter=",")
    np.savetxt(prefix+'edges_z-voxel.csv', ((edges[2]- origin[2]) / spacing[2]).astype(np.uint16) , delimiter=",")
    return rho , u, edges


def del_node_attribute(G, key):
    for node in G.nodes():
        if key in G.node[node].keys():
            del G.node[node][key]

# class lut_shifter(HasTraits):

#    a=Int(1)
#    def __init__(self,tub):
#        HasTraits.__init__(self)
#
#        vmax=Range(0,10,value=5,mode='slider')
#        vmin=Range(0,10,value=5,mode='slider')
#        self.add_trait('vmin', vmin)
#        self.add_trait('vmax', vmax)
