# -*- coding: utf-8 -*-
# author: Felix Repp felix.repp@mpikg.mpg.de
# !/usr/bin/env python
"""@package tinaskel

%This module provides the tools to obtain a network structure from the skeletonized images.
A nice description of the skeletonization process can be  found [here](http://www.inf.u-szeged.hu/~palagyi/skel/skel.html).


When the skeletonization is   performed by  matlab, %this module is used to run matlab either on the local machine or on the cluster using the SUN Grid Engine via an ssh connection.
When the matlab script Skel2Graph has been used, the result was already a network structure, the resulting network can be loaded using this module and converted into a [NetworkX](http://networkx.github.io/) structure.


The network consist of nodes, connected via edges. Additional to the information that can be obtained using algorithms of networkx, e.g. the degree of the nodes, calculated values of both, nodes and edges, are stored in th attribute dictionary.


\code{py}
G=t1.networkdict['initial']
G.nodes(data=True)[0]
In: G.nodes(data=True)[0]
Out:
(0,
 {'cell': False,
  'cellnode': False,
  'idx': [20746],
  'multinode': False,
  'x': 0.0,
  'y': 0.0,
  'z': 921.9896})

\endcode

besides the coordinates of the node, ` x,y,z ` the flags mean the following:
+ ` 'cell'` %This is true if the node is a cell
+ ` 'cellnode'`  cellnodes are introduced on the surface of the cell, to assign the network within the cell a length of 0
+ ` 'multinode'` %This is true if in the skeleton the node was composed of more than one voxel

both the ` 'multinode'` flag, as well as the ` 'idx'` list of the indices of the node voxels of the skeletonized image are not important for the user and could at most be used for debugging.


The edges also store the coordinates of the  voxels

\code{.py}
In: G.edges(data=True)[0]
Out:
 (0,
 83,
 {'n1': 0,
  'n2': 83,
  'weight': 16.774313191001166,
  'x': array([ 0.    ,  0.    , ...]),
  'y': array([  0.    ,   0.3033,  ...]),
  'z': array([ 921.9896,  922.286 , ...])})
\endcode

` 'n1'` and ` 'n2'`  entries are needed to identify the direction of the coordinates of the edge with respect to the nodes (starting from node ` 'n1'`) ` 'weight'`

"""
import os
import numpy as np
from scipy.ndimage import label
from scipy import io
from scipy import sparse
import pdb
import networkx as nx
import sys
import itertools
sys.path.append("../lib")

import tina_main
from tinacell import Cell

import time
import tinanet


def skel_data_thinvox(bini, skelfolder, thinvoxfolder='../thinvox', binfile='bin.binvox', thinfile='thinned.binvox'):
    """@param      bini binary image of the network
    @param skelfolder where to save binary image and skeletonized data
    @param thinvoxfolder where to find the thinvox binaries

    @param  binfile name how to save binary image
    @param  thinfile name how to save skelton image
    """
    import binvox_rw as rw
    print'save binvox file'
    with open(os.path.join(skelfolder, binfile), 'wb') as f:
        rw.Voxels(bini, bini.shape, [0., 0., 0.], 1., 'xzy').write(f)
    print 'done'
    thinvoxfolder = os.path.abspath(thinvoxfolder)
    oldpath = os.getcwd()
    os.chdir(skelfolder)
    if os.path.exists(os.path.join(skelfolder, thinfile)):
        os.remove(os.path.join(skelfolder, thinfile))

    ti1 = time.time()
    os.system('"'+os.path.join(thinvoxfolder ,'thinvox')+'" -f palagyi -t binvox ' + os.path.join(skelfolder, binfile) + '')
    print 'skeletonization took {:.2f} s'.format(time.time() - ti1)
    os.rename('thinned.binvox', thinfile)
    os.chdir(oldpath)

    skel = load_skel_thinvox(skelfolder, thinfile)

    return skel


def skel_data(bin, cell, skelfolder, matlabfolder='../OCY_analysis/', rtype='local', filter_nodes=False, filter_branches=4, remotefolder=None, do_compression=True, pad=False):
    """
    %This function saves the data such that it can be loaded by matlab, and start the skeletonization

    @param      bin binary image of the network
    @param      cell binary image of thresholded cells
    @param      skelfolder where the thresholded data will be stored as well as the skeletonization result. if skeletonization will be performed remote, must be be copied to remote

    @param      matlabfolder where to finde the skeletonization algorithmn (must be the folder where the skeletonization is performed local or remote)
    @param      rtype 'local'to run it on the machine,
     'string' to create the files but do not execute it. A string is returned which can be used to create a job fir skeletonizations of  %%this or several datasets
    @param      filter_nodes merge nodes connected by short edges shorter than this threshold, measured in voxel
    @param      filter_branches prun network using this thresholds
    @param      remotefolder this folder states where the computer performing the skeletonization will find the thresholded data
    @param     do_compression compress matlab file
    @param     pad pad array with zeros, as otherwise structures on the rim can get lost
    """
    olddir = os.getcwd()
    # np.save(o.folder+'/rawacn.npy',I)
    # np.save(o.folder+'/cellacn.npy',D)
    # pdb.set_trace()

    # pad before providing it to matlab

    # if not os.path.exists( skelfolder):
    #      os.mkdir(skelfolder)

    # fname=os.path.join(skelfolder,'thresholded.bin')
    # fname2=os.path.join(skelfolder,'cells.bin')
    filename = os.path.join(skelfolder, 'prep.mat')
    #
    # images are stored as binary files as matlab export only supports double floats
    #
    # cell.T.astype(bool).tofile(fname2)
    # bin.T.astype(bool).tofile(fname)

    if bin.dtype == np.bool:
        bin.dtype = np.uint8
    if type(cell) == np.ndarray and cell.dtype == np.bool:
        cell.dtype = np.uint8
    if pad:
        bin2 = np.zeros(np.array(bin.shape) + 2)
        bin2[1:-1, 1:-1, 1:-1] = bin
        if type(cell) == np.ndarray:
            cell2 = np.zeros(np.array(bin.shape) + 2)
            cell2[1:-1, 1:-1, 1:-1] = cell
    else:
        bin2 = bin
        cell2 = cell
    shape = bin2.shape

    extent = [0, bin.shape[0], 0, bin.shape[1], 0, bin.shape[2]]

    io.savemat(filename, {'img': bin2, 'cel': cell2, 'shape': shape, 'extent': extent, 'THR_METHOD': 6, 'skelfolder': skelfolder,
                          'filer_branches': filter_branches, 'filter_nodes': filter_nodes}, do_compression=do_compression)

    if rtype == "local":
        os.chdir(matlabfolder)
        os.system('matlab -nodesktop -r " OCY_main_python(' +
                  "'" + filename + "','" + skelfolder + "')" + '"')
        os.chdir(olddir)

    elif rtype == "string":
        if not remotefolder:
            sfolder = skelfolder
        else:
            sfolder = remotefolder
        st = 'cd ' + matlabfolder + \
            '\n matlab -nodesktop -r " OCY_main_python(' + \
            "'" + filename + "','" + sfolder + "')" + '"'
        return st


def makejobskel(dataset, string, jobname='jobskel', matlabfolder='../OCY_analysis/', sshcon=False, submit=True, copy=False, remotefolder=None, remotematlabfolder=None):
    """
    create a job for skeletonization on the cluster if a ssh to the cluster is provided, the job will also be submittet
    @param  dataset to track the process
    @param  strings call string or list of strings as provided by skel_data()
    @param  jobname If None a unique name based on the time will be used
    @param  matlabfolder folder where the skelteonization skripts are located (must be local)
    @param  sshcon if the job is to be submitted a ssh connection as provided by the pysftp library is needed
    @param submit submit the job to the cluster (SUN Grid Engine)
    @param copy copy thresholded images to remote folder

    """
    if not remotefolder:
        remotefolder = dataset.remotefolder
    if jobname is None:
        import time
        t = time.time()
        jn = 'job' + str(t)
    else:
        jn = jobname

    with open(os.path.join(matlabfolder, 'ML', jn), 'w') as f2:

        with open(os.path.join(matlabfolder, 'ML', 'jobx')) as f:

            for i in f.readlines()[:]:
                f2.write(i)
            if type(string) == str:
                string = [string]
            for task in string:
                f2.write(task)
    if sshcon:

        if copy:
            pdb.set_trace()
            if remotefolder:
                skip = False
                lsstr = sshcon.execute('ls ' + remotefolder)
                sshcon.put(os.path.join(matlabfolder, 'ML', jn),
                           os.path.join(remotematlabfolder, 'ML', jn))
                if len(lsstr) == 1 and 'No such file' in lsstr[0]:
                    mkstr = sshcon.execute('mkdir ' + remotefolder)
                    if len(mkstr) == 1 and 'No such file' in mkstr[0]:
                        print 'Folder structure in remote location not suitable'
                        skip = True
                if not skip:
                    for fi in ['prep.mat']:
                        if fi in sshcon.execute('ls ' + os.path.join(remotefolder, fi)):
                            if raw_input("overite file [No]/yes") in ["y", "yes", "Y"]:

                                sshcon.put(os.path.join(
                                    dataset.skelfolder, fi), os.path.join(remotefolder, fi))
                        else:
                            sshcon.put(os.path.join(
                                dataset.skelfolder, fi), os.path.join(remotefolder, fi))

                dataset.remotefolder = remotefolder

            else:
                print 'No remote folder provided'
        else:
            matlabfolder = os.path.abspath(matlabfolder)

        if submit:
            print sshcon.execute('dos2unix ' + os.path.join(matlabfolder, 'ML', jn))
            print sshcon.execute('qsub ' + os.path.join(matlabfolder, 'ML', jn))


def load_skel_thinvox(skelfolder, thinname):
    import binvox_rw as rw
    with open(os.path.join(skelfolder, thinname), 'rb') as f:
        skel = rw.read_as_3d_array(f, fix_coords=False).data.astype(np.bool)
    return skel


def get_graph_matlab(skelfolder, origin, spacing, padded=True):
    """
    make networkx structure from philips matlab structure
    cellnodes of degree two are added at the surface of the cells, such that cells can be removed without removing adjacent canaliculi
    @param skelfolder (where to find data)
    @param padded the matlab code changed and padded the image data. set True to account for this
    """
    celllist = []
    celldict = {}
    import scipy.io as io
    nw = nx.MultiGraph()

    nodes = io.loadmat(skelfolder + '/node.mat')['node']
    cell = io.loadmat(skelfolder + '/cell.mat')['cell']
    link = io.loadmat(skelfolder + '/link.mat')['link']
    sh = io.loadmat(skelfolder + '/shape.mat')
    w = sh['w'][0][0]
    h = sh['h'][0][0]
    l = sh['l'][0][0]
    r0 = origin

    dr = spacing
    """
    if padded:
        #pdb.set_trace()

        #w+=2
        #h+=2
        #l+=2

        r0[0]-=spacing[0]
        r0[1]-=spacing[1]
        r0[2]-=spacing[2]
    """

    L = link[0].shape[0]
    N = nodes[0].shape[0]

    multinodes = []
    m = 0

    celldict = {}
    # pdb.set_trace()
    for i in range(nodes[0].shape[0]):

        if len(nodes[0][i]['idx']) > 1:
            mn = True
            multinodes.append(i)

        else:
            mn = False

        [n1x, n1y, n1z] = get_cms(
            (nodes[0][i]['idx'] - 1), (w, l, h), r0, dr, order='F')

        # meaning of cellnode changed: used to be node on the surface now is
        # cell.
        if 'cell' in nodes[0][i].dtype.names:
            cf = 'cell'
        else:
            cf = 'cellnode'
        cn = bool(nodes[0][i][cf])

        nw.add_node(i, {'x': n1x, 'y': n1y, 'z': n1z,
                        'cell': cn, 'cellnode': False, 'multinode': mn})

        if cn:
            # pdb.set_trace()
            if i in celldict.keys():
                celldict[i].append()
            else:
                celldict[i] = [nodes[0][i]['idx'] - 1]
        # pdb.set_trace()
    xn = nx.get_node_attributes(nw, 'x')
    yn = nx.get_node_attributes(nw, 'y')
    zn = nx.get_node_attributes(nw, 'z')
    # pdb.set_trace()

    for i in range(L):
        li = link[0][i]
        n1i = li['n1'][0][0] - 1
        n2i = li['n2'][0][0] - 1
        # if n1i==144 or n2i==144:
        # pdb.set_trace()

        inds = (li[2][0] - 1).tolist()
        if len(inds) != len(np.unique(inds).tolist()):
            if inds[0] == inds[1]:
                inds = inds[1:]
                print 'fixed double index in link' + str(i)
            elif inds[0] == inds[-1]:
                inds = inds[1:]
                print 'loop in link' + str(i)
            else:
                print 'unfixed double index in Link' + str(i)
                pdb.set_trace()
        # if n1i==-1 or n2i==-1:
        #    pdb.set_trace()
        if n1i in [-1, -2]:

            x_, y_, z_ = get_r(np.array(inds[0]), (w, l, h), r0, dr, order='F')

            n1i = N
            N += 1
            nw.add_node(n1i, {'x': x_, 'y': y_, 'z': z_,
                              'cell': False, 'cellnode': False, 'multinode': False})
            xn[n1i] = x_
            yn[n1i] = y_
            zn[n1i] = z_

        if n2i in [-1, -2]:
            # if inds[0] in nodes[0][n1i][0].ravel().tolist():
            n2i = N
            N += 1
            x_, y_, z_ = get_r(
                np.array(inds[-1]), (w, l, h), r0, dr, order='F')

            nw.add_node(n2i, {'x': x_, 'y': y_, 'z': z_,
                              'cellnode': False, 'cell': False, 'multinode': True})
            xn[n2i] = x_
            yn[n2i] = y_
            zn[n2i] = z_

        # pdb.set_trace()
        x_, y_, z_ = get_r(np.array(inds), (w, l, h), r0, dr, order='F')
        x_ = x_.tolist()
        y_ = y_.tolist()
        z_ = z_.tolist()

        if len(z_) != len(x_):
            # pdb.set_trace()
            # don't ask me why
            z_ = z_[0]

        # handle multinodes

        if n1i in multinodes or n2i in multinodes:
            d1 = (xn[n1i] - x_[0]) ** 2 + (yn[n1i] - y_[0])**2 + (zn[n1i] - z_[0])**2 + \
                (xn[n2i] - x_[-1]) ** 2 + \
                (yn[n2i] - y_[-1])**2 + (zn[n2i] - z_[-1])**2
            d2 = (xn[n2i] - x_[0]) ** 2 + (yn[n2i] - y_[0])**2 + (zn[n2i] - z_[0])**2 + \
                (xn[n1i] - x_[-1]) ** 2 + \
                (yn[n1i] - y_[-1])**2 + (zn[n1i] - z_[-1])**2
            if d2 < d1:
                nh = n1i
                n1i = n2i
                n2i = nh

        if n1i in multinodes and not (xn[n1i] == x_[0] and yn[n1i] == y_[0] and zn[n1i] == z_[0]):
                # pdb.set_trace()

            x_.insert(0, xn[n1i])
            y_.insert(0, yn[n1i])
            z_.insert(0, zn[n1i])
        if n2i in multinodes and not(xn[n2i] == x_[-1] and yn[n2i] == y_[-1] and zn[n2i] == z_[-1]):
            # pdb.set_trace()
            x_.append(xn[n2i])
            y_.append(yn[n2i])
            z_.append(zn[n2i])

        # make cellnodes

        if n2i in celldict.keys():
            # pdb.set_trace()
            if len(x_) > 2:
                if n1i in celldict.keys():
                    # pdb.set_trace()
                    if len(x_) == 3:
                        continue  # would result in one voxel edge
                if nx.get_node_attributes(nw, 'cell')[n2i] == True:

                    # pdb.set_trace()
                    nw.add_node(N, attr_dict={
                                'x': x_[-2], 'y': y_[-2], 'z': z_[-2], 'cellnode': True, 'cell': False, 'multinode': False})

                    nw.add_edge(N, n2i, attr_dict={
                                'x': x_[-2:], 'y': y_[-2:], 'z': z_[-2:], 'weight': 0, 'n1': N, 'n2': n2i})

                    n2i = N
                    x_ = x_[:-1]
                    y_ = y_[:-1]
                    z_ = z_[:-1]
                    # length=get_length(x_[:-1],y_[:-1],z_[:-1])
                    # nw.add_edge(n1i,N,attr_dict={'x':x_[:-1],'y':y_[:-1],'z':z_[:-1],'weight':length,'n1':n1i,'n2':N})

                    N += 1
                # else:
                #    length=get_length(x_,y_,z_)
                #    nw.add_edge(n1i,n2i,attr_dict={'x':x_,'y':y_,'z':z_,'weight':length,'n1':n1i,'n2':n2i})

        if n1i in celldict.keys():
            # pdb.set_trace()
            if len(x_) > 2:
                if nx.get_node_attributes(nw, 'cell')[n1i] == True:
                    nw.add_node(N, attr_dict={'x': x_[1], 'y': y_[1], 'z': z_[
                                1], 'cellnode': True, 'cell': False, 'multinode': False})

                    nw.add_edge(n1i, N, attr_dict={'x': x_[:2], 'y': y_[
                                :2], 'z': z_[:2], 'weight': 0, 'n1': n1i, 'n2': N})

                    # length=get_length(x_[1:],y_[1:],z_[1:])
                    # nw.add_edge(N,n2i,attr_dict={'x':x_[1:],'y':y_[1:],'z':z_[1:],'weight':length,'n1':N,'n2':n2i})
                    n1i = N
                    x_ = x_[1:]
                    y_ = y_[1:]
                    z_ = z_[1:]
                    N += 1
                # else:
        length = get_length(x_, y_, z_)
        nw.add_edge(n1i, n2i, attr_dict={
                    'x': x_, 'y': y_, 'z': z_, 'weight': length, 'n1': n1i, 'n2': n2i})
        """else:
            length=get_length(x_,y_,z_)
            nw.add_edge(n1i,n2i,attr_dict={'x':x_,'y':y_,'z':z_,'weight':length,'n1':n1i,'n2':n2i})
         """
    nw.graph['splines'] = False
    tinanet.remove_false_nodes(nw)

    # do some statistics with cells
    celllist = []
    for k in celldict.keys():
        # pdb.set_trace()#cell[0][celldict[k]]['idx'][0]-1)
        # [n1x,n1y,n1z]=get_cms(),(w,l,h),r0,dr)
        # cellnode
        n1i = celldict[k]  # cell[0][k]['nodes']-1
        # N+=1
        cellc = Cell(k, (nodes[0][k]['idx'] - 1).ravel().tolist(),
                     w, l, h, r0, dr, order='F')
        celllist.append(cellc)
        celldict[k] = cellc

    for l, m in itertools.combinations(celldict.keys(), 2):
        celldict[l].connected_with_graph = {}
        celldict[m].connected_with_graph = {}
        celldict[l].connected_with[m] = []
        celldict[m].connected_with[l] = []
        # if nodes[0,n1][3][0,0]==1:
        #
    # pdb.set_trace()
    nw.graph['splines'] = False

    
    return nw, celldict, celllist


def get_nhood(img):
    """
    calculates the neighborhood of all voxel, needed to create graph out of skel image
    inspired by [Skel2Graph]() from Philipp Kollmannsberger.

    @param img binary skeleton image. needs to be padded with zeros

    @retval nhood neighborhood of all voxels that are True
    @retval nhi the indices of all 27 neighbors, incase they are true
    @retval inds of all True voxels (raveled)
 retruning also the indeces of
    """
    print 'calculating neighborhood'
    inds=np.arange(img.size)
    inds=inds[img.ravel()]
    #pdb.set_trace()
    #inds = np.where(img.ravel())[0]
    x, y, z = np.unravel_index(inds, img.shape)  # unraveled indices
    # space for all neighbors
    nhi = np.zeros((inds.shape[0], 27), dtype=np.uint32)
    # nhi=sparse.lil_matrix((inds.shape[0],27),dtype=np.uint32)#space for all
    # neighbors
    s0, s1, s2 = np.array(img.shape) -2
    if s0 * s1 * s2 > np.iinfo(nhi.dtype).max:
        print 'the indices of the array are higher than the supporeted dtype'
        pdb.set_trace()
    nhood = np.zeros(np.array(img.shape), dtype=np.uint8)
    for xx in range(0, 3):
        for yy in range(0, 3):
            for zz in range(0, 3):
                n = img[xx:(s0 + xx), yy:(s1 + yy),                        zz:(s2 + zz)]  # shifted image
                # nhood+= n
                w = np.lib.ravel_multi_index(np.array([xx, yy, zz]), (3, 3, 3))
                # nhi[:,w]=np.lib.ravel_multi_index(np.array([xx+x-1,yy+y-1,zz+z-1]),np.array(img.shape))*n[img[1:-1,1:-1,1:-1]]
                nhi[:, w] = (np.lib.ravel_multi_index(np.array([xx + x - 1, yy + y - 1, zz + z - 1]), np.array(img.shape)) * n[img[1:-1, 1:-1, 1:-1]])

                # check if neighbor is True
                # pdb.set_trace()
    #pdb.set_trace()
    # calculate number of neighbors (+1)
    nhood = np.sum(nhi > 0, axis=-1, dtype=np.uint8)
    # pdb.set_trace()
    print 'done'
    return nhood, nhi, inds


def get_network_from_skel(skel, cells=None, r0=[0, 0, 0], dr=[1, 1, 1], pad=True, add_cellnode=True):
    """
    converts skeletonized image into network structure
    @param img skeltonized image
    @param cells image of the cells to tag cellnodes or None
    @param r0 origin of the image
    @param dr spacing of the image
    @param pad if True the image is padded first with zeros, if this had been done before it can be set False
    @param add_cellnode to ignore the network within the cells, nodes are added on the surface of the cells, and the weight insight the cells are set to 0
    """
    print 'get network from skel'
    # pad image?
    import collections
    import itertools as IT
    if pad:
        img = np.zeros(np.array(skel.shape) + 2, dtype=bool)
        img[1:-1, 1:-1, 1:-1] = skel
        if np.any(cells):
            cellsp = np.zeros(np.array(skel.shape) + 2, dtype=bool)
            cellsp[1:-1, 1:-1, 1:-1] = cells
            img[cellsp] = True
        r0 -= np.array(dr)
    else:
        img = skel

        if np.any(cells):
            img[cells] = True

    xs, ys, zs = np.shape(img)

    # get neighborhood
    nhood, nhi, inds = get_nhood(img)

    """
    skelinds=np.zeros_like(img,dtype=np.int64).ravel()
    skelinds[inds]=inds
    skelinds.shape=img.shape
    """

    # edges are those with 2 neighbors
    # pdb.set_trace()
    edgevox = nhood == 3
    cans = inds[edgevox]
    nodes = (nhood != 3) * (nhood != 1)  # all others but noise
    ninds = inds[nodes]
    # pdb.set_trace()
    # get the two neighbors of edges for iterating through edges
    can_nh_idx = nhi[edgevox]
    del nhi
    can_nh_idx[:, 13] = 0
    can_nb = np.sort(can_nh_idx)[:, -2:]
    del can_nh_idx
    # get clusters of nodes
    no = np.zeros_like(img, dtype=bool)
    no[np.lib.unravel_index(inds[nodes], [xs, ys, zs])] = 1
    indnodes, num = label(no, structure=np.ones((3, 3, 3)))

    # create network structure
    import networkx as nx
    G = nx.MultiGraph()
    celldict = {}
    print 'add nodes'
    # feed nodes into network
    # uni,nodeinds_all=np.unique(indnodes,return_index=True)
    nodeinds_all = collections.defaultdict(list)
    try:
        indravel = indnodes.ravel()

        # was indnodes.size
        for idx, val in IT.izip(np.arange(no.size, dtype=np.uint64), indravel):
            if not val == 0:
                nodeinds_all[val].append(idx)

        del indnodes
    except:  # memory allocation the following uses less memory
        nodeinds_all = collections.defaultdict(list)
        if num < 2**16:
            indnodes = indnodes.astype(np.uint16)
        indravel = indnodes.ravel()
        del indnodes
        steps = np.linspace(
            0, indravel.size, indravel.size / 10000, dtype=np.uint64)
        for i in range(len(steps) - 1):
            for idx, val in IT.izip(np.arange(steps[i], steps[i + 1], dtype=np.uint64), indravel[steps[i]:steps[i + 1]]):
                if not val == 0:
                    nodeinds_all[val].append(idx)
    N=len(nodeinds_all.keys())
    for i in nodeinds_all.keys():
        sys.stdout.write("\rpercentage: " + str(100 * i / N) + "%")
        sys.stdout.flush()
        nodeinds = nodeinds_all[i]
        i -= 1  # to have nodes starting at 0
        x, y, z = np.lib.unravel_index(nodeinds, no.shape)
        """
        #slow
        x,y,z=np.where(indnodes==i)
        nodeinds=np.lib.ravel_multi_index(np.array([x,y,z]),indnodes.shape)
        """
        xmean = x.mean()
        ymean = y.mean()
        zmean = z.mean()
        ce = False
        # pdb.set_trace()
        if cells is not None and np.any(cells):
            # pdb.set_trace()
            if cellsp[int(xmean), int(ymean), int(zmean)]:
                ce = True
                celldict[i] = [nodeinds]
                # pdb.set_trace()
        if len(x) > 1:
            # node concids out of more than one voxel (only important for
            # debugging?)
            mn = True
        else:
            mn = False
        G.add_node(i, {'x': xmean * dr[0] + r0[0], 'y': ymean * dr[1] + r0[1], 'z': zmean * dr[
                   2] + r0[2], 'multinode': mn, 'cell': ce, 'cellnode': False, 'idx': nodeinds})
        # find all canal vox in nb of all node idx
    print 'nodes done, track edges'
    # del nodeinds_all
    nodex = nx.get_node_attributes(G, 'x')
    nodey = nx.get_node_attributes(G, 'y')
    nodez = nx.get_node_attributes(G, 'z')
    edge_inds = []

    # now, find all edges

    can_ravel = can_nb.ravel()  # might make it faster
    # pdb.set_trace()
    N=len(G.nodes())
    for i, nd in G.nodes(data=True):
        sys.stdout.write("\rpercentage: " + str(100 * i / N) + "%")
        sys.stdout.flush()
        # if i ==46:
        # pdb.set_trace()
        nodeinds = nd['idx']
        # find all edge voxels which are neighbors of node i
        nbs = np.in1d(can_ravel, nodeinds).reshape(can_nb.shape)
        can_n, pos = np.where(nbs)
        # iterate through edge untill node is reached
        for m, idx in enumerate(can_n):
            edge_inds = [cans[idx]]
            test = can_nb[idx, pos[m] == 0]
            # pdb.set_trace()
            while test in cans:
                edge_inds.append(test)
                newcan = can_nb[cans == test]
                otest = test

                test = newcan[newcan != edge_inds[-2]][0]
            # pdb.set_trace()
            if test in ninds:
                # n2=indnodes[np.lib.unravel_index(test,[xs,ys,zs])]-1
                n2 = indravel[test] - 1
                # if edge_inds[-1]==128257:
                # pdb.se_trace()
                # to avoid starting the edge from the oposite side
                can_nb[cans == edge_inds[-1]] *= 0
                if n2 not in G.nodes():
                    pdb.set_trace()
                    # this should never happen
                else:
                    # cellnode
                    x, y, z = np.lib.unravel_index(
                        np.array(edge_inds).astype(np.int64), img.shape)
                    x_ = np.r_[nodex[i], x * dr[0] + r0[0], nodex[n2]]
                    y_ = np.r_[nodey[i], y * dr[1] + r0[1], nodey[n2]]
                    z_ = np.r_[nodez[i], z * dr[2] + r0[2], nodez[n2]]

                    G.add_edge(i, n2, attr_dict={'x': x_, 'y': y_, 'z': z_, 'weight': get_length(
                        x_, y_, z_), 'n1': i, 'n2': n2})
                    # G.add_edge(i,n2,
            # else:
            #   pdb.set_trace()
    print 'edges done'
    # pdb.set_trace()
    # create cellnodes
    for n1 in celldict.keys():
        # n1 is index of cell
        for n2 in G[n1].keys():  # there can be more than one edge between two nodes
            edgedicts = G[n1][n2]
            for h in edgedicts.keys():
                edge = edgedicts[h]

                x_ = edge['x']
                y_ = edge['y']
                z_ = edge['z']
                # loop from cell to cell though one voxel
                if edge['n1'] == n1 and edge['n2'] == n1 and len(x_) < 4:
                    # pdb.set_trace()
                    G.remove_edge(n1, n2, h)
                    continue
                # if len(x_)<3:
                #        pdb.set_trace()
                # pdb.set_trace()
                if edge['n1'] == n1:  # the edge coordinates start at cell
                    if len(x_) > 2:
                        ni1 = max(G.nodes()) + 1
                        G.add_node(ni1, attr_dict={'x': x_[1], 'y': y_[1], 'z': z_[
                                   1], 'cellnode': True, 'cell': False, 'multinode': False})

                        G.add_edge(n1, ni1, attr_dict={'x': x_[:2], 'y': y_[:2], 'z': z_[
                                   :2], 'weight': 0, 'n1': n1, 'n2': ni1})  # edge from cell to cell node

                        if not edge['n2'] == n1:  # loop
                            length = get_length(x_[1:], y_[1:], z_[1:])
                            G.add_edge(ni1, n2, attr_dict={'x': x_[1:], 'y': y_[1:], 'z': z_[
                                       1:], 'weight': length, 'n1': ni1, 'n2': n2})
            # what happens with connections between two cells or loops
                    else:
                        if not edge['n2'] == n1:
                            G.node[n2]['cellnode'] = True
                    # if 994 ==ni1 :
                    #    pdb.set_trace()
                if edge['n2'] == n1:  # the edge coordinates ends at cell
                    if len(x_) > 2:
                        ni2 = max(G.nodes()) + 1
                        G.add_node(ni2, attr_dict={
                                   'x': x_[-2], 'y': y_[-2], 'z': z_[-2], 'cellnode': True, 'cell': False, 'multinode': False})

                        G.add_edge(ni2, n1, attr_dict={
                                   'x': x_[-2:], 'y': y_[-2:], 'z': z_[-2:], 'weight': 0, 'n1': ni2, 'n2': n1})
                        if not edge['n1'] == n1:  # loop
                            length = get_length(x_[:-1], y_[:-1], z_[:-1])
                            G.add_edge(n2, ni2, attr_dict={'x': x_[
                                       :-1], 'y': y_[:-1], 'z': z_[:-1], 'weight': length, 'n1': n2, 'n2': ni2})

                        # if 994 ==ni2 :
                        #      pdb.set_trace()
                    else:
                        if not edge['n2'] == n2:
                            G.node[n2]['cellnode'] = True
                if edge['n1'] == n1 and edge['n2'] == n1:  # loop from cell to cell
                    # if len(x_)>3:
                    length = get_length(x_[1:-1], y_[1:-1], z_[1:-1])
                    G.add_edge(ni1, ni2, attr_dict={'x': x_[
                               1:-1], 'y': y_[1:-1], 'z': z_[1:-1], 'weight': length, 'n1': ni1, 'n2': ni2})

                G.remove_edge(n1, n2, h)
                # if   max(G.nodes())==412:
                #    pdb.set_trace()
                if length == 0:
                    pdb.set_trace()

    # do some statistics with cells
    w, h, l = img.shape
    celllist = []
    tinanet.remove_false_nodes(G)
    for k in celldict.keys():
        #pdb.set_trace()#cell[0][celldict[k]]['idx'][0]-1)
        # [n1x,n1y,n1z]=get_cms((nodeinds_all[k]),(w,l,h),r0,dr)
        # cellnode
        n1i = celldict[k]  # cell[0][k]['nodes']-1
        # N+=1
        cellc = Cell(k,  nx.get_node_attributes(G, 'idx')
                     [k], w, h, l, r0, dr, offset=[1, 1, 1])
        celllist.append(cellc)
        celldict[k] = cellc
        # if k==12982:
        #    pdb.set_trace()
    for l, m in itertools.combinations(celldict.keys(), 2):
        celldict[l].connected_with_graph = {}
        celldict[m].connected_with_graph = {}
        celldict[l].connected_with[m] = []
        celldict[m].connected_with[l] = []
        # if nodes[0,n1][3][0,0]==1:
        #
    # pdb.set_trace()
    G.graph['splines'] = False

    d=np.array(G.degree().values())==0
    
    c=np.array(nx.get_node_attributes(G,'cell').values())==False
    k=np.array(G.degree().keys())[c*d]
    G.remove_nodes_from(k)
    #if np.array((G.degree().values()==0)*(nx.get_node_attibutes(G,'cell').values()==False)).sum():
    #    pdb.set_trace()
    return G, celldict, celllist


def get_r(inds, shape, r0=[0, 0, 0], dr=[1, 1, 1], order='C'):
    """
    get positiion of voxels
    @param inds 1D index of the voxel
    @param shape of array


        """
    return map(np.add, r0, map(np.multiply, dr, np.lib.unravel_index(inds, shape, order=order)))


def get_cms(inds, shape, r0=[0, 0, 0], dr=[1, 1, 1], order='C'):
    """
    get mean positiion of voxels
    @param inds 1D index of the voxel
    @param shape of array


        """
    x_, y_, z_ = get_r(inds, shape, r0, dr, order=order)
    return x_.mean(), y_.mean(), z_.mean()


def get_length(x_, y_, z_):
    """
    calculate length of an edge
    """
    if len(x_) > 1:
        return np.sum(np.sqrt((np.array(x_[1:]) - np.array(x_[:-1]))**2 + (np.array(y_[1:]) - np.array(y_[:-1]))**2 + (np.array(z_[1:]) - np.array(z_[:-1]))**2))
    else:
        return 0

"""def show_axis(self):
        if self.stats['skew']<0:
            c=(1,0,0)
            E=self.stats['eigen']
            uv=(self.stats['cm']-np.sqrt(E[0][0])*E[1][0]).tolist()+(2*np.sqrt(E[0][0])*E[1][0]).tolist()
        else:
            c=(0,0,1)
            E=self.stats['eigen']
            uv=(self.stats['cm']-np.sqrt(E[0][-1])*E[1][-1]).tolist()+(2*np.sqrt(E[0][-1])*E[1][-1]).tolist()
        pdb.set_trace()
        mlab.quiver3d(*uv,color=c)
    """


"""
def makejob(folder,sshcon,andreas=False,adapt=False,std=False):
    import os
    import time
    import shutil
    t=time.time()
    olddir=os.getcwd()
    os.chdir('/usr/data/nfs5/repp/visualize_ocn/code/cluster')
    if andreas:
        ff=open('eval_cluster_andreas.py','r')
    elif adapt :
        ff=open('eval_clusteradapt.py','r')


    else:
        ff=open('eval_cluster.py','r')
    if std==True:
        ff=open('eval_clusterstd.py','r')
    fn=open("eval_cluster"+str(int(t))+".py",'w')
    fn.write('folder="'+folder+'"')
    for i in ff.readlines():
        fn.write(i)
    import time
    time.sleep(1)
    fn.close()
    ff.close()
    jn='jobq'+folder.split('/')[-2]+folder.split('/')[-1]

    f=open('job3')
    f2=open(jn,'w')
    for i in f.readlines()[:]:
        f2.write(i)

      f2.write("python eval_cluster"+str(int(t))+".py")
    f.close()
    f2.close()

    #dos2unix job2
    #pdb.set_trace()0

    import ssh
    bb=sshcon.execute('cd '+'/usr/data/nfs5/repp/visualize_ocn/code/cluster'+'; dos2unix '+jn)
    aa=sshcon.execute('cd '+'/usr/data/nfs5/repp/visualize_ocn/code/cluster'+'; qsub '+jn)
    os.chdir(olddir)
    """
