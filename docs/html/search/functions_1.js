var searchData=
[
  ['adaptive_5fthresholding',['adaptive_thresholding',['../namespacetinaimg.html#a233119a5bb41fb63dccbc6aae40bccd6',1,'tinaimg.adaptive_thresholding()'],['../namespacetinaui.html#accf347c1756704cdd0b90bbfb7685b85',1,'tinaui.adaptive_thresholding()']]],
  ['adaptive_5fthresholding_5fold',['adaptive_thresholding_old',['../namespacetinaimg.html#a3f597c9e87dbdfb58b192cd37cba7520',1,'tinaimg']]],
  ['add_5fcell_5fdata',['add_cell_data',['../namespacetinaviz.html#a62f12e417db659f8a015f4b5f0b98ff2',1,'tinaviz']]],
  ['add_5fdataset',['add_dataset',['../namespacetinaviz.html#a2ec240187bf52e00b6c2d5a0266ea01d',1,'tinaviz']]],
  ['add_5ffunctions_5fto_5fnode_5fdict',['add_functions_to_node_dict',['../namespacetinanet.html#a96c4a7dc67888d69c71908ee77ebd06c',1,'tinanet']]],
  ['add_5ftag',['add_tag',['../namespacelib.html#a13f1b022436f99075b232f701284502a',1,'lib']]],
  ['add_5ftag_5fto_5ffilenames',['add_tag_to_filenames',['../namespacetinaui.html#ad3b0ae74a7d43e7a21572fe5191205ad',1,'tinaui']]],
  ['analysenw',['analysenw',['../namespaceanalyze__ocn.html#a773dc4fef2d2c8e95074a635b5c2d5b2',1,'analyze_ocn']]],
  ['analysenworientation',['analysenworientation',['../namespaceanalyze__ocn.html#a65dfc2f755595298d90b14bbacebee9d',1,'analyze_ocn']]],
  ['analyze_5fcells',['analyze_cells',['../namespacetinaui.html#a290e27505d28fa70a40201c49ee4553e',1,'tinaui']]],
  ['analyze_5finhomogenity',['analyze_inhomogenity',['../namespacetinaui.html#ad1f0c111630cf6b241b9802f3b83d68a',1,'tinaui']]],
  ['assignedges',['assignEdges',['../namespacetinanet.html#a4bf3868b163dedf06c8aa6a211c35393',1,'tinanet']]]
];
