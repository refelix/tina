import sys
sys.path.append('../source')
import tinaui
folder = '../test/sampledata/ocn'

# load already skeletonized data
t1 = tinaui.load_status(folder, load_imagedata=True)

# relace the discrete coordinates in the network by smooth connections
tinaui.replaceEdgesBySplines(t1,name='initial',copy='smooth')

# perform some analysis
tinaui.simple_network_analysis(t1,'smooth')

tinaui.analyze_inhomogenity(t1,'smooth',V=400,maskname=None,minweight=350)

tinaui.analyze_cells(t1,'smooth',maskname=None)
tinaui.save_quantified(t1)
tinaui.export_metadata(t1)
tinaui.export_network_data(t1,'smooth')
tinaui.make_projection_images(t1)


