var searchData=
[
  ['cdict',['cdict',['../namespaceanalyze__ocn.html#a77c0714a32677bf88a73b7b2e4e41089',1,'analyze_ocn']]],
  ['cell',['Cell',['../classtinacell_1_1Cell.html',1,'tinacell']]],
  ['cell_5factorsl',['cell_actorsl',['../classtina__main_1_1dataset.html#a9bdc3349af013b9585445d6bfef116ed',1,'tina_main::dataset']]],
  ['cell_5fbased_5fanalysis',['cell_based_analysis',['../namespaceanalyze__ocn.html#a5e040aab8ca7f9d6b429628a9414c203',1,'analyze_ocn.cell_based_analysis()'],['../namespacetinana.html#af308f1f9597782c51f87e1bfcc59f54b',1,'tinana.cell_based_analysis()']]],
  ['celldict',['celldict',['../classtina__main_1_1dataset.html#a6f6676e17bea196f9647fca40af6f6dd',1,'tina_main::dataset']]],
  ['celllist',['celllist',['../classtina__main_1_1dataset.html#a944be41cd5ce8046d80029f4b4152e8c',1,'tina_main::dataset']]],
  ['check_5fget_5fattributes',['check_get_attributes',['../namespacetinanet.html#af991ee75fde33ed2fa8456897a9baa7f',1,'tinanet']]],
  ['connected_5fwith',['connected_with',['../classtinacell_1_1Cell.html#a491bff0d8670b0fb362c33cbbd3f269e',1,'tinacell::Cell']]],
  ['copy_5fres_5ffolder',['copy_res_folder',['../namespacelib.html#a3e2759545f714d968e5ef2be395b8646',1,'lib']]],
  ['copy_5fres_5ffolder2',['copy_res_folder2',['../namespacelib.html#a8b3d7cf19433e20320e2a6cb1172582e',1,'lib']]],
  ['cyl_5fto_5fcat',['cyl_to_cat',['../namespacelib.html#a27f4803032c02d48493552afa78e7b03',1,'lib']]]
];
