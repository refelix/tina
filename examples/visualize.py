import sys
sys.path.append('../source')

import tinaui
folder = '../test/sampledata/ocn'
t1 = tinaui.load_status(folder, load_imagedata=True)
# use the name of image data, the extent is given in voxels check the GUI
# of mayavi pipline and for possible ways to change apearence, or
# mlab.pipeline.surface?
dataset, surface = tinaui.plot_iso(
    t1, name='bin', extent=[0, 50, 0, 100, 0, 100], opacity=.5, color=(1, 0, 0))
tinaui.plot_iso(t1, 'cells')
tinaui.plot_graph(t1, 'initial')
