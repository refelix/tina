# -*- coding: utf-8 -*-
# author: Felix Repp felix.repp@mpikg.mpg.de
# !/usr/bin/env python
"""@namespace tinaui
\brief %This module provides the user interface for an easy usage of TINA using the  tina_main.dataset container.



Tinaui allow an easy usage of the tools as well as documentation of the process. They are often named like the functions found in the other packages which they map. The difference between the use of the original function and a function in tinaui is that in the latter case, tinaui handles the produces data and keeps an protocol (`dataset.status`)of what you did and which parameters you used.

Brief introduction to usage
=====
These examples run with the sample data stored in '../test/sampledata/ocn'. For more a detailed description of the functions  click on the highlighted functions, use the help provided by `function?` within ipython, or read the detailed help below.

All the data of one dataset is stored in memory as a tina_main.dataset class, which is initialized by
\code{.py}
from tina_main import dataset
t1=dataset(folder)
\endcode
You have to provide the folder, which is where typically the image data is located. If `None` is provided, a file dialog is used to select the folder. During initialization new subdirectories are created to save the results and other created data. (see tina_main.dataset.__init__() for additional options)



Loading a Dataset and skeletonizing it locally
------

\include start_skeletonization_matlab.py
see examples/start_skeletonization_matlab.py  ( \%`run start_skeletonization_matlab.py` out of examples folder)


The same can be done using thinvox
\include start_skeletonization_thinvox.py
see examples/start_skeletonization_thinvox.py

tinaui assumes a lot of parameters which provided useful in analyzing confocal data of osteocyte networks. Different samples, resolution, or even jest different settings at the microscope might signify that some of the parameters need can be adjusted: You can click on the function names which are highlighted in blue to see all parameters.

As thresholding of image data is a challenging task this step needs to be checked manually. If the results of the predefined parameters of the adaptive thresholding  are not satisfactory, you can set the test flag of this function to look at images which represent the sub-steps of this routine.


For the following examples it is assumed that the skeletonization has been performed.

Accessing image and Network data
---
To run these examples, it is essential that the data first has been skeltonized


3D images in TINA are 3D numpy arrays
tinaui stores %this image data as well as network structures in standard python dictionaries (imgdict und networkdict)


\include datadicts.py
see examples/datadicts.py

NetworkX is used to analyze the network, information about edges, nodes are stored as attributes, see [NetworkX Tutorial](http://networkx.github.io/documentation/networkx-1.9.1/tutorial/tutorial.html#adding-attributes-to-graphs-nodes-and-edges) as well as tinaskel.



3D Visualization
----


%This package  includes a interface for the visualization. To run TINA on the cluster the visualization flag in  tinaui.py needs to be set to `False` if you run TINA on the cluster, to use the visualization it must be set `True`
there are two function: tinaui.plot_iso for volumetric data, and tinaui.plot_graph to visualize the Network structure

\include visualize.py
see examples/visualize.py

tinaui takes care of management of mayavi datasets, as well as coordinates. Otherwise
\code{.py}
from mayavi import mlab
mlab.contour3d(t1.imgdict['raw'],contours=[25])
\endcode
can be used for a quick inspection of imagedata

Orientation of Network
----
\include networkdemo.py
see examples/networkdemo.py


Introduction to more functions
=====

The following explanations are also treated in the detailed.ipynb
to explore %this start
\code
ipython notebook --pylab=inline
\endcode

Most of the functions of tinaui have some predefined values which are defined in the source code by

\code{.py}
def function(x,a=15,b=None):# here a and b are predefined
    pass
\endcode
The significance of the parameters are explained in the description you find in the help of the functions. %This chapter aims to show how to use these parameters, and describes some of the typical applications.

Opposed to the brief sample above not all data is included to directly run these examples.

Loading image data
----
Amusing you have included the necessary libraries and initialized a dataset
\code{.py}
import sys
sys.path.append('../source')

from tina_main import dataset
import  tinaui

t1= dataset(folder='../test/sampledata/ocn',skelfolder='+skel_manual')
\endcode
in this example you can see that you can specify a folder for the skeletonized data, if you use `+` it will be created relative to the datafolder (see help of `dataset.__init__()`).  %This folder as well as a folder for the results will directly be created.

you can load image stacks using `tinaui.load_stack()`.

\code{.py}
tinaui.load_stack(dataset=t1,name='raw', folder=t1.folder, ftype='tif',dtype='uint8')
\endcode

This loads all tif files within the folder. You find the numpy array as ``t1.imgdict['raw']``. You can set the dtype of the array: typical images are uint8, dependent on the needed precision when applying filter, you maybe want to use 'float' or 'double'. If you load a binary image you can load it as 'bool'.

\code{.py}
tinaui.load_stack(dataset=t1,name='mask', folder=os.path.join(t1.folder,'mask'), ftype='png',dtype='bool')
t1.imgdict['raw'][t1.imgdict['mask']==False]=0
\endcode

Loading a second stack, tinaui also checks if the size of the added stack is the same as the initial one.

if you have several stacks loaded and forgot how you named them, use
\code{.py}
t1.imgdict.keys()
\endcode
to get a list of all the names of your image stacks.


Save image stack
---
Similiar to loading a stack of images, you can also save a processed 3D image as images.

\code{.py}
tinaui.save_stack(t1,'bin',ftype='png',folder=None)
\endcode
saves the image ``t1.imgdict['bin']`` in the default folder(this is a subdirectory of the datafolder named like the name of the image.

Setting shape and origin, loading initial data
----

To use the physical dimensions of the image (e.g. to calculate the length of the network) you can use the dataset.spacing. In addition, dataset.origin to specify the coordinate of the voxel(0,0,0). By default `spacing=(1,1,1)`, `origin=(0,0,0)`.
Using tinaui.parse_xml() origin and spacing are retrieved from xml files created by Laica SP5 confocal microscopes.


`tinaui.load_initial_data()` combines loading of an imagestack and setting of the spacing. If `xml==False`, and no spacing and origin is provided, this routine asks the user to enter values manually.


Thresholding an image
---
As the skeletonization is performed on an binary image, you first have to threshold it.
If your image has good contrast, you can try using a global threshold:

\code{.py}
t1.imgdict[ 'bin']=t1.imgdict[ 'raw']>30 #30 being the global threshold
\endcode

To find a useful threshold you can start with a histogram and if you have two nicely separated peaks of background and foreground voxels.

\code{.py}
import matplotlib.pyplot as plt
plt.hist(t1.imgdict['raw'].ravel(),bins=range(256))#use ravel as hist expects 1D data
\endcode

you can check your result by visualizing the thresholded image, either single slides, or projections
\code{.py}
import matplotlib.pyplot as plt

plt.subplot(2,2,1) # upper left
plt.imshow((t1.imgdict['raw']).std(axis=-1))# raw data
plt.title('raw')

plt.subplot(2,2,2) # upper right
plt.imshow((t1.imgdict['raw']>39).std(axis=-1))#thresholded data
plt.title('thresholded std projection')

plt.subplot(2,2,3) # lower left
plt.imshow((t1.imgdict['raw']>39).sum(axis=-1))
plt.title('thresholded sum projection')

plt.subplot(2,2,4) # lower right
plt.imshow((t1.imgdict['raw']>39)[:,:,5])
plt.title('thresholded single slice')
\endcode

\image html thr_subplot.png'

The projection using `.std(axis=-1)` has a nice contrast for inspecting both, lacunae as well as canaliculi.
You can see that while some canaliculi seem nicely thresholded, others disintegrate.


###Adaptive thresholding
Reasons why a global threshold does not work well are that the intensity of the signal decreases with the thickness of the sample, and the staining is not constant.

As all canaliculi have a comparable diameter, [difference of Gaussians](https://en.wikipedia.org/wiki/Difference_of_Gaussians)  (DOG) can be well used as an adaptive threshold method. Similarly to the difference it is possible to use the ratio of two different smoothened images. The idea of both these methods is to compare  the image after smoothing  with Gaussian functions of different kernel size (i.e.\ different standard deviation). The default parameters work well if the network structures have a diameter of 2-3 voxel.
 `tinaui.adaptive_thresholding` allows to use both those methods.
\code{.py}
tinaui.adaptive_thresholding(dataset,s1=1, s2=1.6,
    sc1=5, sc2=9, thr=1.05, thrc=1.1, ratio=True,extreme=False,cells=True,)
\endcode

Using the ratio when `ratio==True` and the difference when `ratio==False`.


While there are a lot of parameters compared to a global threshold, they tend to be relatively robust for similiar measurements. `s1` and `s2` are the width of the kernels of the two Gaussian filter function measured in voxel.  `thr` is the threshold which will be slightly above 1 for a ratio, and around 20 when the difference is used.

As lacunae are on a different length scale, few additional steps following the same line of considerations using different Gaussian kernel sizes were  required to  also properly threshold the lacunae. (For  images of different networks without such bigger  structures set `cells=False`) `sc1=5`, `sc2=9` and `thrc=1.1` have the same meaning as in the first step.


For a more detailed description have a look at the adaptive_thresholding notebook within the notebook folder.

Segmenting the cells
----

Have a look in the adaptive thresholding notebook. At the end the segmentation is also explained and visualized.
In an image of OLCN the lacunae are structures which have bigger dimensions than the thin cananliculi. After eroding the image until all the canaliculi are gone will produce an image with center parts of the lacunae (done by `dist_tr` and  distance transform). Dilating that image regrows the lacunae. However due to the oblate shape of the lacunae,this might change their shape. The algorithm developed during my thesis tries to grow the cell further until locally the addition of voxels would reach into the canaliculi (defined by `CANALICULI_SIZE`). For a better result this not performed on the binary image but on a eroded version (`dist_tr2`). Clusters of voxels which are to small to be cells can be filtered out (`vc`).

\code{.py}
tinaui.find_cells(t1,dist_tr=4,dist_tr2=2,CANALICULI_SIZE=15,vc=500)
\endcode


Skeletonize the data
---
Both implemented versions use a thinning algorithm to skeletonize the data. You can either use [Skeleton3D](http://se.mathworks.com/matlabcentral/fileexchange/43400-skeleton3d), a matlab code developed by Philip Kollmannsberger or [thinvox](http://www.cs.princeton.edu/~min/thinvox/) by Patrick Min.

\code{.py}
tinaui.skeletonize_thinvox(t1,skelfolder=None, thinvoxfolder='../thinvoxfolder/', binfile='bin.binvox',thinfile='thinned.binvox')
tinaui.skeletonize_matlab(t1,skelfolder=None, matlabfolder='../thinvoxfolder/', filter_nodes=False,filter_branches=4, do_compression=True):
\endcode

both algorithms can be used the same way, you only need to provide a dataset. All the other parameters can remain untouched.
The matlab code, already concerts the skeleton to a network (graph) structure and performs some cleaning of the network. By default short edges (shorter than 4 voxel (can be changed in OCY_main_python  line 97  `Skel2Graph3D(skel,THR)`) are removed. This is comparable to
\code{.py}
tinanet.filter_short_branches(n,shortedge=1.2, iterate =True)#n is the network structure
\endcode
while in this case the threshold shortedge takes the spacing of the voxels into account. In the process of the matlab script small segments of network, edges that are not connected to any other edge get removed.

In addition the matlab code can merge nodes whose connections are shorter than a certain threshold. By default this is not done, but can be done by setting  `filter_nodes=THR`.

While `tinaui.skeletonize_thinvox` uses just the binary image (it gets stored as  skelfolder/binfile) and return an image of the skeleton  (skelfolder/thinfile), `tinaui.skeletonize_thinvox` also needs the binary cell image. These two images as well as the filter threshold and some additional information get stored as skelfolder/prep.mat', the script will then produce several `.mat` files, containing the skeleton as well some that describing the network.




Convert skeletonized image to network structure
---

As described above if you used matlab to skeletonize your data, you don't get the skeletonized image, but already a network structure. This still needs to be converted into a networkx structure. Also the coordinates of the matlab file are voxels, tina uses real coordinates, accounting for the spacing (voxel size).

The function
\code{.py}
tinaui.get_graph(t1,skelfolder=None, filter_short=0,cells=True)
\endcode
can be used independent which method you used for the skeletonization. As each tinaui routine that you perform writes some information into the status of the dataset, tinaui can decide whether you just convert the network structure or if it creates it based on th skeleton image.

If you used thinvox for the skeletonization, the creation of the network goes along the lines of the matlab script. As the skeletonization creates some sub structure within the cells, all nodes within the cells are treated as one if you set `cells==True`.
For a better comparability of the two skeletonization methods short ends can be directly removed (see next section).

See the quick guide for some examples how to access network data, or wait for the analysis package to be documented.


Pruning of the network and smoothing
---

Pruning of the network refers to a removal of the short branches of the network that arise through roughness of the surface of the thresholded image. Smoothing of the image before the skeltonization can also reduce those artifacts.

Pruning can be done using
\code{.py}
tinanet.filter_short_branches(n,shortedge=.5, iterate =True):
\endcode
(this should be wrapped into tinaui)
where shortedge is a length threshold for short branches which will be removed. When an edge is removed and the node it was connected to has now degree 2, the node gets removed and the two edges connecting to it become one edge. If iterate == True, the algorithm tries if any of the merged edges is also shorter than the short edge, until no change of the network is observed.

Originally the coordinates of the edges were the center of the voxels, i.e. describing a wiggling line. Smoothing splines can be fitted using

\code{.py}
tinaui.replaceEdgesBySplines(t1,name='initial',copy='smooth',sf=None,k=2,nest=-1)
\endcode

This function changes the coordinates, therefore a backup of the network is performed: the network with the name `name` is used, and unless `copy==False` a copy with the corresponding name is used. `sf` is a parameter describing the amount of smoothing.
The higher the value the straighter the line becomes.
I recommend playing around with this value and verify the outcome using the 3d visualization
As a first guess to compensate the variation in the range of individual pixel a value `sf=0.25*d^2` with `d` being the mean spacing, should be used. If you set `sf==None` this value multiplied by 2 (to be on the save side)  is used. A displacement of the nodes when short edges were removed can also be removed using this function.

Calculating the splines allows to determine the orientation of the canaliculi, and is performed at the same time.




Status / metadata
----

Using functions from the tinaui namespace does not only make the usage more simple, it also tries tokeeps track of applied functions and used parameters. This information is also stored in the `dataset.status` dictionary.
The data can be exported as a csv file using 
\code{.py}
tinaui.export_metadata(dataset, folder=None, fname='metadata.csv')
\endcode


You can also use `tinaui.save_status()` and `tinaui.load_status()` to save and restore the progress of your analysis:
\code{.py}
import tinaui
tinaui.save_status(t1)
t2=tinaui.load_status(folder='../test/sampledata/ocn')
\endcode


Note that these functions do not save all the data itself (saving imagedata again would produce a lot of redundant data). `save_status` instead saves where loaded images  are located, etc.
You can change this default behaviour using the `save_images` keyword.

Often when reloading a dataset you don't need the image data, as most of the analysis is performed on the network. If you need the imagedata, for example for visualization use
\code{.py}
tinaui.load_status(folder='../test/sampledata/ocn', load_imagedata=True).# or load_imagedata=['bin','cells']
\endcode
`tinaui.load_status` try to retrieve the image data from several sources: if it was saved using `tinaui.save_stack`, or previously loaded with `load_stack` (e.g. the raw data). Thresholded data is stored for the skeletonization in different formats, like the skeletonized image: these images can also be reloaded using loadstatus. The same is true for the cell image if you use matlab, however not for thinvox. In the latter case, if you have not saved the cells as stack, `load_status` uses tinaui.find_cells with the parameters saved in status.



`tinaui.load_status` by default loads saved networks. Networks get saved if you use `tinaui.save_status()`
if you want to start you analysis with a virgin network, you can use `tinaui.load_status(t1,reimport_network=True)`


 For additional options read manual of `tinaui.save_status` and `tinaui.load_status`.

You can also export the metadata as a .csv file, which will be found in the resultsfolder
 \code{.py}
 export_metadata(dataset)
#endcode

Visualization
----

tinaui brings two functions for visualization of the network:

\code{.py}
def plot_iso(dataset,name='raw',extent=None,contours=None,blurr=0,figure=None,**kwargs)
#and
def plot_graph(dataset,name,node_size=1, node_color=(1,1,1),scale_node_keyword=None,node_color_keyword=None,
             edge_color_keyword=None, edge_radius_keyword=None,
               edge_color=(0,0,0),edge_colormap='jet',
               tube_radius=.4,figure=None,**kwargs) :
\endcode

The first is used to visualize volumetric data, the second the network structure.

Mayavi, which is used for the visualization is based on the c++ library [vtk](http://www.vtk.org/). It uses its own data sources, filters and visualization modules, which are managed by the mayavi pipeline.

when you first call `plot_iso` with a new name, this function creates the data source, so if you want to visualize different regions using an `extent` you do not always copy the data.
For binary data, you should use  `contours=[.5]` or for the raw data use the threshold you want (make sure you use `[thr]` as if you do not pass a list the value defines how many contours you obtain).  You can interactively change the threshold by opening the mayavi pipeline (upper left corner) and use the slicer in the contour item.
`kwargs' are keywords that are passed to the surface module, this is handy if you want to set `opacity=.5, color=(1,0,0)'


`plot_graph` is used to visualize the network including some properties. For both, nodes as well as edges you can visualize data scaling the diameter as well as colorizing them. You can set keywords that are retrieved by
\code{.py}
nx.get_node_attributes(dataset.networkdict[name],keyword)
#or
nx.get_edge_attributes(dataset.networkdict[name],keyword)
\endcode
respectively. In addition you can use `scale_node_keyword='degree'.
You can inspect the network interactivly and find out the label of certain node. To do so you have to call
\code{.py}
dataset.acticate_picker()
\endcode




Analysis
----

There are many quantities that can be calculated from a Network.

\include simple_analysis.py
see examples/simple_analysis.py

provide a collection of quantification methods. Have a look at the documentation of the functions.

\code{.py}
tinaui.save_quantified(t1)
\endcode
is used to export the results of these tools.



`analyze_inhomogenity` computes several quantities:

Most intuitively the complete length `L` or more characteristic the density `rho=L/V` where `V` is the volume of the sample.




There are further many stochastic properties of substructures. You can calculate the density for small subvolumes to study inhomogeneity of your sample, or look at properties like the length of the edges or the degree of the nodes.
Besides the function that are implemented in NetworkX like

\code{.py}
nx.degree(G)
#and
nx.clustering(G) #to calculate the clustering coefficient
\endcode

Note that to calculate the cluster-coefficient the graph needs to be converted to a normal nx.Graph, instead of the nx.Multigraph() used by default.

tinanet allows to calculate several quantities and save them as edge/node attributes. Besides quantities for each edge, like `weight`, `straightness` and `direction` you can calculate orientation for small segments of each length. Using orientation of the network you can calculate angles between the network and characteristic directions.

For an analysis with your favourite program outside of python, you can use 
\code{.py}
tinaui.export_network_data(t1,name='initial')
\endcode



Plotting a histogram of such quantities help to understand differences between datasets.

\code{.py}
tinana.get_edge_hist(n,keyword='weight',cumulative='reverse', normalize=True, weights=None,bins=np.arange(0,25,0.5))
#and
tinana.get_node_hist(n,keyword='degree',cumulative='reverse', normalize=True, weighr=None,weights=None,bins=np.arange(0,25,1))
\endcode
allow you to calculate such histograms of a graph `n` for any attribute of the network.

For the typical distribution of node degree and edge weight, tinaui provides more convenient functions


 \code{.py}
def make_degree_histogram(dataset,name,bins=np.arange(3,20,1),cumulative='reverse', log=True,weighted=False,fmt=True,**kwargs)
#and
make_length_histogram(dataset,name,bins=np.arange(0,25,1),cumulative='reverse', log=True,weighted=False,fmt=True,**kwargs)
\endcode
which also plot the histograms and perform exponential fits on the histogram.






"""
import os
import numpy as np
import pdb
import networkx as nx
import sys
import matplotlib.pyplot as plt

from xml.dom.minidom import parse
import numpy.ma as ma

import pandas
import tinanet
import tinaimg
import tinana
import tinaskel
import tinacell
import cPickle
import scipy.io
import io
import time
import lib
import copy as co

visualization = True


def load_stack(dataset, name, folder=None, ftype='tif', nfilter=None, extent=None, treat_color=None, dtype='uint8'):
    """
    function to load imagedata from folder using tinaimg.loadstack() and taking care of status and imgdict
    @param    dataset: of class TINA where the data should be managed
    @param    name name for imgdict
    @param    folder where to look for data, if None: use dataset.folder
    @param    ftype, nfilter, extent, treat_color,dtype, see tinaimg.loadstack() for definition

    """
    if folder is None:
        folder = dataset.folder
    print 'loading stack ' + name
    dat = tinaimg.load_stack(folder=folder, ftype=ftype, nfilter=nfilter,
                             extent=extent, treat_color=treat_color, dtype=dtype)
    # dataset.status[name+'_image']={'type':'stack','folder':folder, 'ftype':ftype,'nfilter':nfilter,'extent':extent,'treat_color':treat_color,'dtype':dtype}
    dataset.imgdict[name] = dat
    suc = dataset.set_shape(dat.shape)
    dataset.status['loaded_stacks'][name] = {'name': name, 'folder': folder, 'ftype': ftype,
                                             'nfilter': nfilter, 'extent': extent, 'treat_color': treat_color, 'dtype': dtype}
    return suc


def save_stack(dataset, name, ftype='png', folder=None):
    """
    saves an 3d image from the imgdict as 2d images
    @param dataset from tina_main
    @param name keyword in imgdict
    @param ftype how to save ('png' or 'tif') 'jpg' is not recommendet
    @param folder where to save it, if None, a folder in the skelfolder with the name `name` is created
    """
    print 'export image stack ' + name
    if not folder:
        folder = os.path.join(dataset.skelfolder, name, '')
    tinaimg.save_stack(dataset.imgdict[name], folder, name, ftype)
    dataset.status['saved_stacks'][name] = {
        'name': name, 'folder': folder, 'ftype': ftype, 'dtype': dataset.imgdict[name].dtype}
    print 'done'


def load_initial_data(dataset, name='raw', ftype='tif', nfilter=None, extent=None, treat_color=None, dtype='uint8', xml=False, origin=None, spacing=None, datafolder=None, defspacing=False):
    """
    function to initialise dataset by loading imagedata from folder using tinaimg.loadstack()
    in addition the xml file of SP5 microscops can be used to determine origing and spacing of imagedata
    @param    dataset of class TINA where the data should be managed
    @param    name of image in imgdict
    @param    ftype, nfilter, extent, treat_color,dtype, see tinaimg.loadstack() for definition
    @param    xml if True, the datadolder is scanned for a suitible properties file, if not found or if xml==None, an dialog is opend, alternatively a path/filename can be provided
    @param    datafolder load images from datafolder instead of datasets default folder

    @param    defspacing use spacing of dataset
    @param    samplename for use in exported data, if None is provided, folder name is used
    @param    dataname name of the datase for use in exported data, if None is provided, folder name is used
    """
    if datafolder is None:
        datafolder = dataset.folder

    suc = load_stack(dataset, name, folder=datafolder, ftype=ftype,
                     nfilter=nfilter, extent=extent, treat_color=treat_color, dtype=dtype)

    if not suc:
        pdb.set_trace()

    if xml:
        origin, spacing = parse_xml(xml, datafolder)

    elif not(origin and spacing):

        inp = raw_input('set origin in \mu m(default = ' + str(dataset.origin))
        if inp == '':
            origin = dataset.origin
        else:
            origin = eval(inp)
        inp = raw_input(
            'set spacing in \mu m(default = ' + str(dataset.spacing))
        if inp == '':
            spacing = dataset.spacing
        else:
            spacing = eval(inp)
    dataset.origin = origin
    dataset.spacing = spacing
    dataset.extent = extent

    # return


def set_dataname(dataset, name=None):
    """
    assign a name to a dataset, which will show up in exported data in addition to folder
    """
    if name is None:
        f, name = os.path.split(dataset.folder)
        if name == '':  # if folder has final slash
            f, name = os.path.split(dataset.folder)
        if '-' in name and 'FM' in name.split('-')[0]:  # andreas nomencature

            name = name.split('-')[1]

    dataset.quantified['data_name'] = name
    dataset.status['data_name'] = name


def set_samplename(dataset, name=None):
    """
    assign a name to a dataset, which will show up in exported data in addition to folder
    """
    if name is None:
        f, name = os.path.split(dataset.folder)
        if name == '':  # if folder has final slash
            f, name = os.path.split(dataset.folder)
        if '-' in name and 'FM' in name.split('-')[0]:  # andreas nomencature
            name = name.split('-')[0]

    dataset.status['samle_name'] = name
    dataset.quantified['sample_name'] = name


def parse_xml(xml, datafolder=None):
    """get origin and spacing from xml files produced by laicas sp5 confocal microscope

    @param xml can be the name of the xml file, or True or None if None, a dialog window is used to ask for the file, if True, a file containing '_Properties.xml' within the datafolder is searched
    @param datafolder if xml=True, this is where to look for the xml file

    """
    xml_name = None
    if xml is True and datafolder:
        for fn in os.listdir(datafolder):
            if '_Properties.xml' in fn:
                xml_name = os.path.join(datafolder, fn)
                break
    if isinstance(xml, type('')):
        xml_name = xml
    if not xml_name or xml is None:
        import tkFileDialog
        xml_name = tkFileDialog.askopenfilename(
            initialdir=datafolder, defaultextension='xml', title='choose xml file')

    dom = parse(xml_name)
    l = dom.getElementsByTagName("ScannerSettingRecord")
    zs = None
    for le in l:
        if le.getAttribute('Identifier') == 'dblVoxelX':
            xs = float(le.getAttribute('Variant').replace(',', '.')) * 1e-3
        elif le.getAttribute('Identifier') == 'dblVoxelY':
            ys = float(le.getAttribute('Variant').replace(',', '.')) * 1e-3
        elif le.getAttribute('Identifier') == 'dblVoxelZ':
            zs = float(le.getAttribute('Variant').replace(',', '.')) * 1e-3
        elif le.getAttribute('Identifier') == 'dblStepSize':
            ss = float(le.getAttribute('Variant').replace(',', '.'))
    if zs is None:
        zs = ss
        print 'Andreas confocal?'
    l = dom.getElementsByTagName("DimensionDescription")

    for le in l:

        if le.getAttribute('DimID') == 'X':
            x0 = float(le.getAttribute('Origin').replace(',', '.'))
            # xs=float(le.getAttribute('Length'))/int(le.getAttribute('NumberOfElements'))
        elif le.getAttribute('DimID') == 'Y':
            y0 = float(le.getAttribute('Origin').replace(',', '.'))
            # ys=float(le.getAttribute('Length'))/int(le.getAttribute('NumberOfElements'))
        elif le.getAttribute('DimID') == 'Z':
            z0 = float(le.getAttribute('Origin').replace(',', '.'))
            # zs=float(le.getAttribute('Length'))/int(le.getAttribute('NumberOfElements'))
    origin = [x0, y0, z0]
    spacing = [xs, ys, zs]
    return origin, spacing
    # pdb.set_trace()
    # self.meshgrid_from_xml(name+'_Properties.xml',extend)


def global_thresholding(dataset, thr, rawname='raw', binname='bin'):
    """
    thresholds dataset.imgdict[rawname] and stores the result as  dataset.imgdict[binname]
    @param datasat TINA dataset
    @param rawname key of dataset.imgdict for raw data
    @param binname key of dataset.imgdict for storing binary data
    @param thr global threshold
    """
    dataset.imgdict[binname] = dataset.imgdict[rawname] > thr
    dataset.status['thresholded'] = {'type': 'global', 'params': {
        'thr': thr, rawname: rawname, binname: binname}}


def adaptive_thresholding(dataset, rawname='raw', binname='bin', s1=1.2, s2=1.45,
                        thr=1.035, cells=True, sc1=4, sc2=9, thrc=1.12, 
                        ratio=True, remove=True, remove_thr=.05, erosion=3,
                        dilation=3, minsize=450, fillcells=True,
                        closing_iterations=1, thin_section=True, thin_axis=2,
                        remove_hollow_cells=False, extreme=False, lowf=.5,
                        highf=.98, addl=1, subh=1):
    """This algorithm performs a thresholding of network images with varying intensity.
     The idea of this method is to compare  the image after smoothing  with Gaussian functions of different kernel size (i.e.\ different standard deviation). The default parameters work well if the network structures have a diameter of 2-3 voxel.
    after applying difference of Gaussians (DOG) to threshold the network structure, with smoothing diameters s1, s2, it is applied again with wider diameters sc1, sc2.
    To avoid artifacts in regions with bright an thick canaliculi, the `remove_network_blobs` algorithm can be used id `remove` is set to `True`. In addition cells can be filled with the `fill_cells` algortithm.

    have a look at the thresholding notebook for an explanation of the single steps, as well as the phd thesis of Felix Repp 4.2.3.1 for some description

    @param img imagedata to be thresholded
    @param    s1 width of the kernal of the first Gaussian Filter, applied to reduce some noise
    @param    s2 width of the kernal of the second Gaussian Filter, to compare intensity to environment
    @param   thr threshold for ratio a value of 1.05 produces good results, in case of differences a value of 1 seems also reasonable
    @param cells if True, dog is repeated with sc1 and sc2  
    @param    sc1 width of the kernal of the first Gaussian Filter, for cells
    @param    sc2 width of the kernal of the second Gaussian Filter for cells, to compare intensity to environment

    @param   thr2 threshold to retrieve cells

    @param  ratio if True, the ration instead of the difference is calculated
    @param remove if True,(and cells == True) remove_network blobs is on the second DOG to only add cells, using the following parameters 
    @param remove_thr set threshold of what will be removed, a smaller threshold will remove more
    @param erosion erode after removing
    @param dilation dilation after erosion
    @param minsize noise smaller than this size will be removed 

    @param fillcells if True (and cells == True), closes empty spaces, after morphologically dilating the image,
    @param closing_iterations during filling, perform n times dilation before filling and erosion afterwards
    @thin_section if cells are cut, filling won't succed. If one dimesion is much thinner than the
    other two the image can be "sealed" to fill most of the cut cells, if set True.
    @param  thin_axis which axis is the smallest for sealing
    @param extreme global threshold of extreme values (see thr_extreme documentation)
    @param lowf relative position in the brightness histogram of the dark voxels on which `addl` will be added in thr_extrean
    @param highf relative position in the brightness histogram  from which `subh` will be substracted in thr_extrean
    @param addl offset in thr_extrean
    @param subh offset in thr_extrean
    """
    bin, params = tinaimg.adaptive_thresholding(dataset.imgdict[rawname], s1=s1,
                s2=s2, thr=thr, cells=cells, sc1=sc1, sc2=sc2, thrc=thrc,
                ratio=ratio, remove=remove, remove_thr=remove_thr, erosion=erosion,
                dilation=dilation, minsize=minsize, fillcells=fillcells,
                closing_iterations=closing_iterations, thin_section=thin_section,
                thin_axis=thin_axis, extreme=extreme, lowf=lowf, highf=highf, 
                addl=addl, subh=subh)
    params['rawname'] = rawname
    params['binname'] = binname
    dataset.status['thresholded'] = {'type': 'adaptive', 'params': params}
    dataset.imgdict[binname] = bin


def find_cells(dataset, binname='bin', cellname='cells', dist_tr=4.5, dist_tr2=2.4, CANALICULI_SIZE=55, vc=1000, dilate_cells=0, dilate_bin=0):
    """Maps tinaimg.findcells().Segment Cells from binary image, by using the distance transform and two distance tresholds. seeds, defined by `dist_tr` are grow in the limits defined by  `dist_tr2`. The later will have a strong effect on the canaliculi but will not get rid of all of them. To avoid cells growing into canaliculi and at the  same time trying to fill the cells into the tips, An anisotropic growth is implemeted that should stop when it grows into remainders of canaliculi. The growth stops locally when the locally connected voxels which are added become smaller than `CANALICULI_SIZE`.
since the cells grow to a maximum  defined by `dist_tr2` they might still be smaller than desired. a later isotropic dilation by `dilate_cells`.
Adaptive thresholding can lead to thresholding artifacts close to cells, resulting in a loss of conectivity. As a counter measure cells can be grown beyond the desired size and added to the binary image. As a last step, the cells are dilated by `dilate_bin` iterattions and the binary image is adapted. this does not affect the cells returned by this function

All values measured in voxels.
    @param dataset
    @param dist_tr threshold for seeds
    @param dist_tr2 threshold to grow into
    @param CANALICULI_SIZE (in Voxel)
    @param VC minimal Volume for cell to remain in segmentation (in Voxel)
    @retval cells segmented cells
    @retval params Parameters used
    """
    cells, params = tinaimg.find_cells(dataset.imgdict[
                                       binname], dist_tr=dist_tr, dist_tr2=dist_tr2, CANALICULI_SIZE=CANALICULI_SIZE, vc=vc, dilate_cells=dilate_cells, dilate_bin=dilate_bin)
    dataset.imgdict[cellname] = cells
   
    dataset.status['cells'] = {'type': 'grown', 'cellname':cellname,'binname':binname, 'params': params}

def skeletonize_matlab_local(dataset, **kwargs):
    skeletonize_matlab(dataset, **kwargs)

def skeletonize_matlab(dataset, skelfolder=None, matlabfolder='../OCY_analysis/', filter_nodes=False, filter_branches=4, binname='bin', cellname='cells', skelname='skel', do_compression=True, pad=False):
    """
    call matlab on local machine to skeletonize the data (needs dataset.imgdict['bin'],dataset.imgdict['cells'])
    @param dataset
    @param skelfolder where to save binary image and skeletonized data
    @param matlabfolder where to find the matlab files
    @param      filter_nodes merge nodes connected by short edges shorter than this threshold, measured in voxel
    @param      filter_branches prun network using this thresholds, threshold in voxel
    @param     binname name of the image to skeletonize
    @param     skelname name of the image where skeleton is stored
    @param     do_compression compress matlab file
    @param     pad pad array with zeros, as otherwise structures on the rim can get lost
    """
    if skelfolder is None:
        skelfolder = dataset.skelfolder
    else:
        dataset.set_skelfolder(skelfolder)

    tinaskel.skel_data(dataset.imgdict[binname], dataset.imgdict[cellname], skelfolder=dataset.skelfolder, matlabfolder=matlabfolder,
                       rtype='local', filter_nodes=filter_nodes, filter_branches=filter_branches, remotefolder=None, do_compression=do_compression, pad=pad)
    if 'win' in sys.platform:
        raw_input(
            "Press enter when skeletonization finished (matlab window closed)")

    dataset.status['skeletonized'] = {'type': 'matlab_local',
     'binname':binname, 'skelname':skelname,
         'cellname':cellname, 'params': {
        'filter_nodes': filter_nodes, 'filter_branches': filter_branches, 'pad': pad}}

def skeletonize_thinvox_local(dataset, **kwargs):
    skeletonize_thinvox(dataset, **kwargs)

def skeletonize_thinvox(dataset, skelfolder=None, thinvoxfolder='../thinvox/', binfile='bin.binvox', thinfile='thinned.binvox',binname='bin', skelname='skel',):
    """
    call thinvox on local machine to skeletonize the data (needs dataset.imgdict['bin']). Skeletonized data will be saved as 'skelfolder/thinname. overrides existing file

    @param dataset
    @param skelfolder where to save binary image and skeletonized data
    @param thinvoxfolder where to find the thinvox binaries
        @param  binname name how to save binary image
    @param  thinname name how to save skelton image
    """
    print 'skeletonize'
    
    if skelfolder is None:
        skelfolder = dataset.skelfolder
    else:
        dataset.set_skelfolder(skelfolder)

    if dataset.imgdict[binname].dtype == np.bool:

        bini = dataset.imgdict[binname]
    else:
        bini = dataset.imgdict[binname].astype(np.bool)

    skel = tinaskel.skel_data_thinvox(
        bini, skelfolder, thinvoxfolder, binfile, thinfile)
    dataset.imgdict[skelname] = skel
    dataset.status['skeletonized'] = {
        'type': 'thinvox_local', 'binfile': binfile, 'thinfile': thinfile, 'binname':binname, 'skelname':skelname}

    print 'done'


def get_masked_network_edge(dataset, name, maskname, copy='masked', value=1, bigger_value=False):

    print 'masking network'

    if copy:
        G = dataset.networkdict[name].copy()
        dataset.networkdict[copy] = G
        dataset.status['networks'][copy] = co.deepcopy(
dataset.status['networks'][name])
        name = copy
        if 'saved' in dataset.status['networks'][copy].keys():
            dataset.status['networks'][copy].pop('saved')
        # dataset.status['networks'][name]
    else:
        G = dataset.networkdict[name]

    mask = dataset.imgdict[maskname]
    dataset.networkdict[name] = tinanet.get_masked_network_edge(
        dataset, G, mask, value, bigger_value)

    dataset.status['networks'][name]['masked'] = {
        'maskname': maskname, 'value': value, 'bigger_value': bigger_value}
    print 'done'


def get_connection():
    """
    to comunicate with the cluster
    """
    from pysftp import Connection
    import getpass
    sshhost = raw_input("enter host server adress: ")
    sshuser = raw_input("enter user name: ")

    print 'enter_pw for' + sshuser
    pa = getpass.getpass()
    return Connection(sshhost, sshuser, password=pa)


def save_network(dataset, label, folder, finame):
    """
    save network at folder/finame
    @param  dataset
    @param  label of the network in network dict
    @param  folder
    @param  finame

    """
    print 'pickle network ' + label
    with open(os.path.join(folder, finame), 'wb')as fi:
        cPickle.dump(dataset.networkdict[label], fi)
        dataset.status['networks'][label][
            'saved'] = os.path.join(folder, finame)
    print 'done'


def get_graph_local(dataset,**kwargs):
    get_graph(dataset,**kwargs)


def get_graph(dataset, networkname='initial', skelname="skel",cellname="cells", skelfolder=None, filter_short=0, cells=True, remove_idx=True):
    """ load skeletonized network and convet it to networkx structure, using tinaskel.get_graph()
    @param dataset
    @param betworkname key of networkdict
    @param skelname key of skeleton in imagedict 
    @param cellname key of skeleton in imagedict
    
    @param skelfolder Where to find the network, if None is provided dataset.skelfolder is used
    @param filtershort prun short branches shorter than this threshold
    @param cells during if you create the network from the skeleton rather from the matlab files, the structure of the skeleton in the segmented cells is ignored: if set to True, imgdict['cells'] is used
    @param remove_idx if True: the id of nodes voxels is removed. recommended as cell nodes otherwith blow up memory usage in saving network

    """
    print 'converting skel to network'
    if skelfolder is None:
        skelfolder = dataset.skelfolder

    if 'matlab' in dataset.status['skeletonized']['type']:
        nw, celldict, celllist = tinaskel.get_graph_matlab(
            skelfolder, dataset.origin, dataset.spacing)
    if 'thinvox' in dataset.status['skeletonized']['type']:

        if cells is True:
            cells = dataset.imgdict[cellname]
        else:
            cells = None
        nw, celldict, celllist = tinaskel.get_network_from_skel(
            dataset.imgdict[skelname], cells, dataset.origin, dataset.spacing)

    tinanet.remove_false_nodes(nw)
    if remove_idx:
        tinanet.del_node_attribute(nw, 'idx')
    if filter_short:
        tinanet.filter_short_branches(nw, filter_short)
    dataset.networkdict[networkname] = nw
    dataset.status['networks'][networkname] = {
        'loaded': skelfolder, 'skelname':skelname, 'cellname':cellname,'filtered': filter_short}
    dataset.celldict = celldict
    dataset.celllist = celllist
    print 'network created'


"""def get_graph_remote(dataset, sshcon=None, name='initial', skelfolder=None, remotefolder=None, copyskel=False):
     load skeletonized network and convet it to networkx structure, using tinaskel.get_graph()
    @param dataset
    @param sshcon ssh connection based on pysftp
    @param skelfolder Where to copy the network, if None is provided dataset.skelfolder is used
    @param remotefolder remote location of the network
    @param copyskel also get the skeletonized imgae

    
    print 'converting skeleton to network'
    if sshcon is None:

        sshcon = get_connection()  # sshhost,sshuser,password=pa)

    if skelfolder is None:
        skelfolder = dataset.skelfolder
    if remotefolder is None:
        remotefolder = dataset.remotefolder
    skip = False
    for fi in ['cell.mat', 'link.mat', 'node.mat', 'extent.mat']:
        if not skip and os.path. exists(os.path.join(dataset.skelfolder, fi)):
            if raw_input("overite files [No]/yes") in ["y", "yes", "Y"]:
                skip = True
                txt = sshcon.get(os.path.join(remotefolder, fi),
                                 os.path.join(skelfolder, fi))
                # if "does not exist" in txt:
                #  pdb.set_trace()
    fi = "skel.mat"
    txt = sshcon.get(os.path.join(remotefolder, fi),
                     os.path.join(skelfolder, fi))

    nw, celldict, celllist = tinaskel.get_graph(
        skelfolder, dataset.origin, dataset.spacing)
    dataset.networkdict[name] = nw
    dataset.status['networks'][name]['loaded'] = skelfolder
    dataset.celldict = celldict
    dataset.celllist = celllist
    print 'done'
"""

def merge_short_edges(dataset,  thr,  name='initial', copy=False,  ignore_deadends=True):
    """
    merges clusters of nodes connected by edges shorter thr
    @param thr nodes closer than this threshold  will be merged
    @param name name of the network
    @param copy if False, network will be changed, otherwise a copy of
        the network will be made and stored as networkdict[copy]
    if ignore_deadends short edges which are dead ends will be spared
    """

    if copy:
        G = dataset.networkdict[name].copy()
        dataset.networkdict[copy] = G
        dataset.status['networks'][copy] = co.deepcopy(dataset.status['networks'][name])
        name = copy
        if 'saved' in dataset.status['networks'][copy].keys():
            dataset.status['networks'][copy].pop('saved')


    else:
        G = dataset.networkdict[name]
    n,parms = tinanet.merge_short_edges(G,  thr,  
ignore_deadends=ignore_deadends)
    dataset.status['networks'][name]['merged'] = params

def filter_short_branches(dataset, name='initial', copy=False, shortedge=.5, iterate=True):
    """
    delete nodes with shorter than the threshold starting with the shortest edge. As removing an edge and the node of attatchment it might be needed to run this algorithmn more than once
    @param shortedge threshold below which

    @param name name of the network
    @param copy if False, network will be changed, otherwise a copy of
        the network will be made and stored as networkdict[copy]
    @param iterate untill no more short edges are found
    """

    if copy:
        G = dataset.networkdict[name].copy()
        dataset.networkdict[copy] = G
        dataset.status['networks'][copy] = co.deepcopy(dataset.status['networks'][name])
        if 'saved' in dataset.status['networks'][copy].keys():
            dataset.status['networks'][copy].pop('saved')
        name = copy

    else:
        G = dataset.networkdict[name]
    n,parms = tinanet.filter_short_branches(G,name=name, shortedge=shortedge, iterate=iterate)
    dataset.status['networks'][name]['filtered'] = params
    



def save_status(dataset, folder=None, filename='status.tin', save_networks=True, save_celldict=True, save_images=False, export=True):
    """
   save dataset status such that the process can be reloaded.
   actual data is not saved, so that a renaming of the datastructure will lead to problems reloading the process using Pickle
   @param dataset
   @param folder where to save the status, if None is provided the datafolder is used.
   @param filename the standard extension is tin, but any extension can be used.
   @param save_networks if True all Networks are saved in skelfolder
   @param save_celldict if True the celldict is saved in skelfolder
   @param save_images Image data does not need to be saved additionally as it  typically quite big and is saved anyway. If True or a list of names e.g. ['bin','skel', name] images will be saved as png in folder/name
   @param export if True: metadata, celldict and quantified will be saved as csv

"""
    if folder is None:
        folder = dataset.folder
    if not filename[-4:] == '.tin':
        filename = filename + '.tin'
    savedict = {'status': dataset.status, 'folder': dataset.folder, 'skelfolder': dataset.skelfolder, 'resfolder': dataset.resfolder,
                'remotefolder': dataset.remotefolder, 'shape': dataset.shape, 'spacing': dataset.spacing, 'origin': dataset.origin, 'extent': dataset.extent}
    dataset.status['spacing']=dataset.spacing
    dataset.status['origin']=dataset.origin
    if export:
        save_cell_results(dataset)
        save_quantified(dataset)
        export_metadata(dataset)

    if save_networks is True:
        for label in dataset.networkdict.keys():
            print('save network ' + label)
            save_network(dataset, label, dataset.skelfolder,
                         'network' + label + '.pc')
    if save_celldict is True:

        with open(os.path.join(dataset.skelfolder, 'celldict.pc'), 'wb')as fi:
            cPickle.dump(dataset.celldict, fi)
            dataset.status['celldict_saved'] = os.path.join(
                dataset.skelfolder, 'celldict.pc')
    print 'done'
    if save_images:
        if save_images is True:
            save_images = dataset.imgdict.keys()  # all images
        for name in save_images:
            save_stack(dataset, name)

    savedict['status_location'] = os.path.join(folder, filename)
    dataset.status['status_location'] = os.path.join(folder, filename)
    with open(os.path.join(folder, filename), 'wb') as fi:
        cPickle.dump(savedict, fi)
    print 'status saved'


def load_status(folder, filename='status.tin', load_networks=True, reimport_network=False, load_imagedata=False, skelfolder=None, resfolder=None, load_celldict=True, relative=True):
    """
    load dataset in process for analysis or visualization.

    @param folder where is the status file, typically in datafolder
    @param filename
    @param load_networks load the network as saved
    @param load_celldict imports the celldice
    @param reimport_network if True, the matlab results are reloaded all the others ignored. reloaded network will not be saved
    @params load_images possible True, False, ['raw','bin','cells','skel'] if saved as stack, these images will be used
    @param skelfolder in case data was copied
    @param resfolder in case data was copied
    @param relative if True, folders will be made relative to the status.
    """
    from tina_main import dataset

    with open(os.path.join(folder, filename), 'rb') as f:
        savedict = cPickle.load(f)
    if not skelfolder:
        skelfolder = savedict['skelfolder']
    # else:
    #    copied=True#to be able to load networks
    if not resfolder:
        resfolder = savedict['resfolder']
    status = savedict['status']
    if relative:

        olddir = savedict['folder']
        skelfolder = skelfolder.replace(olddir, folder).replace('\\', '/')
        resfolder = resfolder.replace(olddir, folder).replace('\\', '/')
        # pdb.set_trace()
        for name in status['saved_stacks'].keys():
            status['saved_stacks'][name]['folder'] = status['saved_stacks'][
                name]['folder'].replace(olddir, folder).replace('\\', '/')
        for name in status['loaded_stacks'].keys():
            status['loaded_stacks'][name]['folder'] = status['loaded_stacks'][
                name]['folder'].replace(olddir, folder).replace('\\', '/')
        for name in status['networks'].keys():
            status['networks'][name]['saved'] = status['networks'][
                name]['saved'].replace(olddir, folder).replace('\\', '/')
        status['celldict_saved'] = status['celldict_saved'].replace(
            olddir, folder).replace('\\', '/')
    dat = dataset(folder, skelfolder=skelfolder,
                  resfolder=resfolder, remotefolder=savedict['remotefolder'], del_skelfolder=False)
    dat.origin = savedict['origin']
    dat.spacing = savedict['spacing']
    dat.extent = savedict['extent']
    dat.set_shape(savedict['shape'])

    if load_imagedata:
        # pdb.set_trace()
        # loading saved stacks
        for name in status['saved_stacks'].keys():
            if load_imagedata is True or name in load_imagedata:
                suc = load_stack(dat, **status['saved_stacks'][name])
                if not suc:
                    pdb.set_trace()
        # loading loaded stacks, if not saved additionally

        for name in status['loaded_stacks'].keys():
            if (load_imagedata is True or name in load_imagedata) and name not in dat.imgdict.keys():
                suc = load_stack(dat, **status['loaded_stacks'][name])
                if not suc:
                    pdb.set_trace()

        # binary, cell and skeleton image can also be retrieved from the files
        # being saved for matlab or thinvox
        if 'skeletonized' in status.keys():
            if 'matlab' in status['skeletonized']['type']:
                if 'binname' in status['skeletonized'].keys():
                   binname = status['skeletonized']['binname']
                else:
                    binname = 'bin'
                if 'cellename' in status['skeletonized'].keys():
                   cellname = status['skeletonized']['cellname']
                else:
                    cellname = 'cells'
                if (load_imagedata is True or binname in load_imagedata) and binname not in dat.imgdict.keys():

                    prep = scipy.io.loadmat(
                        os.path.join(dat.skelfolder, 'prep.mat'))
                    dat.imgdict[binname] = prep['img']
                    suc = dat.set_shape(dat.imgdict[binname].shape)
                if (load_imagedata is True or cellname in load_imagedata) and cellname not in dat.imgdict.keys():
                    if not prep:
                        prep = scipy.io.loadmat(
                            os.path.join(dat.skelfolder, 'prep.mat'))
                    dat.imgdict[cellname] = prep['cel']
                    suc = dat.set_shape(dat.imgdict[cellname].shape)

            if 'thinvox' in status['skeletonized']['type']:
                if 'binname' in status['skeletonized'].keys():
                   binname = status['skeletonized']['binname']
                else:
                    binname = 'bin'
                if 'cellename' in status['skeletonized'].keys():
                   cellname = status['skeletonized']['cellname']
                else:
                    cellsname = 'cells'
                if (load_imagedata is True or binname in load_imagedata) and binname not in dat.imgdict.keys():
        
                    import binvox_rw as rw
                    with open(os.path.join(dat.skelfolder, status['skeletonized']['binfile']), 'rb') as f:
                        bin = rw.read_as_3d_array(f, fix_coords=False).data
                    dat.imgdict[binname] = bin.astype(np.bool)
                    suc = dat.set_shape(dat.imgdict[binname].shape)
    if load_imagedata is not False:
        if (load_imagedata is True or 'cells' in load_imagedata):
            
            if 'cells' in status.keys() and 'cells' not in dat.imgdict.keys():
                print( "cells image has not been saved, will be found using old parameters. If you don't need them, adapt load_imagedata value" )
                find_cells(dat, **status['cells']['params'])
            
    if reimport_network:
        dat.status['skeletonized'] = status['skeletonized']
        
        if status['networks']['initial']:
            filter_short = status['networks']['initial']

            get_graph(dat, skelfolder=status['networks']['initial'][
                            'loaded'], filter_short=status['networks']['initial']['filtered'])
            dat.status['networks']['initial'] = {'loaded': status['networks']['initial'][
                'loaded'], 'filtered': status['networks']['initial']['filtered']}

    elif load_networks is True:
        for k in status['networks'].keys():
            if 'saved' in status['networks'][k]:
                print ('load network ' + k) 
                # if copied:
                #    fn=os.path.join(dat.skelfolder,status['networks'][k]['saved'].split(savedict['skelfolder'])[-1])
                # else:
                fn = status['networks'][k]['saved']
                with open(fn, 'rb') as fi:
                    dat.networkdict[k] = cPickle.load(fi)
                    dat.status['networks'][k] = {}
                    dat.status['networks'][k]['loaded'] = status[
                        'networks'][k]['saved']
    if load_celldict:
        if 'celldict_saved' in status.keys():
            fn = status['celldict_saved']
            with open(fn, 'rb') as fi:
                dat.celldict = cPickle.load(fi)
            dat.celllist = (dat.celldict.values())
    dat.status = status
    return dat

    # check if files exist...
    # 'thresholded':None, 'skeletonized':None, 'network':None, 'analyzed':False, 'cells':None,'raw_loaded'=None


# dataset.status['skeletonized']='submittet'

def replaceEdgesBySplines(dataset, name='initial', copy='smooth', sf=None, k=2, nest=-1):
    """replace the voxels of all edges by a smothing spline interpolation
    @param dataset
    @param name of the network of which the edges will be replaced

    @param copy if False, the edges of the original network will be replaced else a copy with the name "copy" will be created.
    @param sf is the smoothnessfactor multilied by len (x) for parameter s 0.25 *spacing**2 for just pixel effects
    @param k spline order
    @param nest controles the amount on knot points

    see scipy.interplolate.splprep


    """
    print 'calculating splines'
    if sf is None:
        sf = 0.5 * np.mean(dataset.spacing)**2
    if copy:
        G = dataset.networkdict[name].copy()
        dataset.networkdict[copy] = G
        dataset.status['networks'][copy] = co.deepcopy(dataset.status['networks'][name])
        if 'saved' in dataset.status['networks'][copy].keys():
            dataset.status['networks'][copy].pop('saved')
        name = copy

    else:
        G = dataset.networkdict[name]
    tinanet.replaceEdgesBySplines(
        G, sf=sf, k=k, nest=nest, dist=np.mean(dataset.spacing))

    dataset.status['networks'][name]['smoothened'] = {
        'sf': sf, 'k': k, 'nest': nest}
    print 'done'


def make_length_histogram(dataset, name, bins=np.arange(0, 25, 1), cumulative='reverse', log=True, weighted=False, fmt=True, **kwargs):
    """
    calculates and draws a histogram   of the lengt of the edges using the network specified by `name`.
    An exponential decay is fitted to the histogram, the fit is weighted by the height of the bin, multiplied with the total length of all edges within the bin.
     Makes a plot and saves it to the results Folder
    @param dataset
    @param name key of dataset.networkdict
    @param bins of the histogram
    @param cumulative if True: normal cumulative histogram, if 'reverse': negative slope
    @param log semilogarithmic y axis?
    @param weighted if True: weights each edge with its length does not do any fits
    @param fmt how to draw values of the histogram (see plt.plot?), if True: bar chart
    @param kwargs arguments to pass to plotting functions, e.g. color='b'
    """
    if weighted:
        edges, values = tinana.get_edge_hist(dataset.networkdict[
                                             name], keyword='weight', cumulative=cumulative, normalize=True, weights='weight', bins=bins)
        tinana.plot_histogram(edges, values, log, fmt=True, **kwargs)
        popt, pcov = None, None
    else:
        edges, values = tinana.get_edge_hist(dataset.networkdict[
                                             name], keyword='weight', cumulative=cumulative, normalize=True, weights=None, bins=bins)
        tinana.plot_histogram(edges, values, log, fmt=fmt, **kwargs)

        edges, v2 = tinana.get_edge_hist(dataset.networkdict[
                                         name], keyword='weight', cumulative=False, normalize=True, weights='weight', bins=bins)

        w = v2 * values
        sigma = 1 / (w**.5)
        x = .5 * (edges[:-1] + edges[1:])
        popt, pcov = tinana.expfit(x, values, sigma=sigma, plot=True)
        plt.title(r'$\beta={:.3f} \pm {:.3f}$'.format(popt[1], pcov[1, 1]**.5))

    if cumulative:
        plt.ylabel('cumulative frequency')
    else:
        plt.ylabel('frequency')
    plt.xlabel('edge weight')
    plt.savefig(os.path.join(dataset.resfolder, name + 'edgeweight.png'))
    plt.savefig(os.path.join(dataset.resfolder, name + 'edgeweight.pdf'))
    return popt, pcov


def make_degree_histogram(dataset, name, bins=np.arange(3, 20, 1), cumulative='reverse', log=True, weighted=False, fmt=True, **kwargs):
    """
    calculates and draws a histogram   of the degree of nodes using the network specified by `name`.
    An exponential decay is fitted to the histogram, the fit is weighted by the length of the network in each bin
     Makes a plot and saves it to the results Folder
    @param dataset
    @param name key of dataset.networkdict
    @param bins of the histogram
    @param cumulative if True: normal cumulative histogram, if 'reverse': negative slope
    @param log semilogarithmic y axis?
    @param weighted if True: lenght of the edges connected to each node
    @param fmt how to draw values of the histogram (see plt.plot?), if True: bar chart
    param kwargs arguments to pass to plotting functions, e.g. color='b'
    """
    if weighted:
        edges, values = tinana.get_node_hist(dataset.networkdict[
                                             name], keyword='weighted_degree', cumulative=cumulative, normalize=True, bins=bins)
        tinana.plot_histogram(edges, values, log, fmt=True, **kwargs)

    else:
        edges, values = tinana.get_node_hist(dataset.networkdict[
                                             name], keyword='degree', cumulative=cumulative, normalize=True, weights=None, bins=bins)
        tinana.plot_histogram(edges, values, log, fmt=fmt, **kwargs)

        edges, v2 = tinana.get_edge_hist(dataset.networkdict[
                                         name], keyword='weight', cumulative=False, normalize=True, weights='weight', bins=bins)

    x = .5 * (edges[:-1] + edges[1:])
    popt, pcov = tinana.expfit(x, values, sigma=None, plot=True)
    if popt[0] is None:
        plt.title('fit not successful')
    else:
        plt.title(r'$\beta={:.3f} \pm {:.3f}$'.format(popt[1], pcov[1, 1]**.5))

    if cumulative:
        plt.ylabel('cumulative frequency')
    else:
        plt.ylabel('frequency')
    plt.xlabel('degree')
    plt.savefig(os.path.join(dataset.resfolder, name + 'nodeweight.png'))
    plt.savefig(os.path.join(dataset.resfolder, name + 'nodeweight.pdf'))

    return popt, pcov


def simple_network_analysis(dataset, name, maskname='mask',cellname='cells', maskvalue=1, figures=None, fmt=True, **kwargs):
    """
    first implementation of some network analysis. calculating histograms for the degree of the nodes, the weights of the edges and fitting exponential decays to it. also calculates the total length and the average cluster coeff.
if imagedict[maskname] exists, only the volume where this image has the maskvalue will be evaluated, if imagedict[cellname] exist, the cells will be substracted from this volume
    @param dataset
    @param name of the network
    
    @param figures if you want to plot the histograms into specigfic figures you can provide a list with two figure numbers e.g. [1,2]
    @param fmt if True do a bar plot, otherwise you can provide formats for the plot function (e.g. 'kx' for black crosses)
    @param kwargs further arguments for the plots e.g. ``color='b'``
    @retval epopt fit parameters of weight decay
    @retval epcov covariance matrix from fit
    @retval npopt fit parameters of degree decay
    @retval npcov covariance matrix from fit
    @retval L total length
    @retval ccm average cluster coefficient
       dataset.quantified['V'] = V evaluated (mask)volume
    dataset.quantified['cluster_mean_' + name] = ccm
    dataset.quantified['Ca.Le_' + name] = total length in evaluated volume
    dataset.quantified['Ca.Dn_' + name] = canalicular density within the volume
    dataset.quantified['weight_k_' + name] = epopt[1]
    dataset.quantified['weight_dk_' + name] = np.sqrt(epcov[1][1])
    dataset.quantified['degree_k_' + name] = npopt[1]
    dataset.quantified['degree_dk_' + name] = np.sqrt(npcov[1][1])


    """
    print 'simple networkanalysis'
    n = dataset.networkdict[name]
    if not figures:
        plt.figure()
    else:
        plt.figure(figures[0])
    epopt, epcov = make_length_histogram(
        dataset, name, weighted=False, fmt=fmt, **kwargs)
    if not figures:
        plt.figure()
    else:
        plt.figure(figures[1])
    npopt, npcov = make_degree_histogram(
        dataset, name, weighted=False, fmt=fmt, bins=range(3, 10), **kwargs)

    G = nx.Graph(n)  # cluster coeffitient not defined for multigraph

    ccm = nx.cluster.average_clustering(G)
    deg = np.array(G.degree().values())
    # cc=nx.clustering(G)
    # ncn==float(np.sum((cc>.5)))/len(G)
    # tree_node_coeff=float(np.sum((cc==0)*(deg==3)))/len(G)

    L = tinana.get_tot_length(n)
    if 'mask' in dataset.imgdict.keys():
        m = dataset.imgdict['mask'] == maskvalue
        if cellname in dataset.imgdict.keys():
            m -= dataset.imgdict[cellname]
        V = m.sum() * dataset.spacing[0] * \
            dataset.spacing[1] * dataset.spacing[2]
    else:

        V = dataset.shape[0] * dataset.shape[1] * dataset.shape[2]
        if cellname in dataset.imgdict.keys():
            V -= dataset.imgdict[cellname].sum()
        V *= dataset.spacing[0] * dataset.spacing[1] * dataset.spacing[2]
    rho = L / V
    dataset.quantified['V'] = V
    dataset.quantified['cluster_mean_' + name] = ccm
    dataset.quantified['Ca.Le_' + name] = L
    dataset.quantified['Ca.Dn_' + name] = rho
    dataset.quantified['weight_k_' + name] = epopt[1]
    dataset.quantified['weight_dk_' + name] = np.sqrt(epcov[1][1])
    dataset.quantified['degree_k_' + name] = npopt[1]
    dataset.quantified['degree_dk_' + name] = np.sqrt(npcov[1][1])
    # pdb.set_trace()
    
    
    print 'done'
    dataset.status['simple_analysis'][name]={'params':{'maskname':maskname,'cellname':'cells', 'maskvalue':maskvalue}}

    return epopt, epcov, npopt, npcov, L, ccm


def analyze_cells(dataset, name='initial', maskname='mask',mindeg=10, minsize=50,maskerode=None):
    """
    calculates some properties of the segmented lacunae. If a mask is provided, only cells within the masked are evaluated. 
    To avoid artifacts from cells which are completely imaged, only those which do not extend to the border of the image, or outside the mask are evaluated this is desided by looking which cell extend outside of the mask after it has been eroded maskerode times. also small cells and cells with only few connections can be ignored.
   statistics of the lacuna will be stored within the quantified dictionary
        'Lc.Nc' number of complete lacunae
        'Lc.sizem' mean size of lacune
        'Lc.sizestd' standard deviation of size of lacune
        'Lc.degm' mean degree of lacunae
        'Lc.degstd' standard deviation of degree of lacunae
        'Lc.vol' complete volume of the lacunae, including noncomplete 
        'Lc.rho_v' volumetric denisty
        'Lc.N' estimated number of lacune ('Lc.vol' / result['Lc.sizem')
        'Lc.dN'] = estimated uncertainty in number
        'Lc.rho_N' estimated number density 'Lc.N' / V
        'Lc.drho_N' estimated uncertainty in numberdensity
    
    
    @param name of the networkused to determine degree of cells 
   
    @param maskname name of  ROI of the imagedict
   
    @parem mindeg minimal degree of lacunae to count as complete
    @param minsize minimal size of lacunae to count as complete (considering spacing)
    @param maskerode  perform an erosion of the mask befor checking if cells are inside the mask. if None is provided a value based on the cellfinding parameres is used.

  

    """
    if maskerode is None:
        tr2 = dataset.status['cells']['params']['dist_tr2']
        dil = dataset.status['cells']['params']['dilate_cells']
        maskerode=int(tr2-dil)+1        
    if maskname is not None:
        mask=dataset.imgdict[maskname]
    else: 
        mask=np.zeros(dataset.shape,dtype=np.bool)
    result, params = tinana.cell_based_analysis(dataset.networkdict[
                        name], dataset.celldict, dataset.shape,
                        dataset.spacing, mask, mindeg=mindeg, minsize=minsize, maskerode=maskerode)
    dataset.quantified.update(result)
    
    dataset.status['cellresults_filter'] = params


def analyze_inhomogenity(dataset, networkname, maskname='mask', cellname='cells', V=400, minweight=0):
    """
    The inhomogeneity of the network as discribed by thdistribution of the density within small subvolumes depend on the size of the  subvolume V.
    To otain density distribution of the network networkname, the length of all subsections of the network is devided by the volume of the mask within the subvolumes. The latter is calculated based on the  mask voxels within the subvolume, using a nearest neighbour approximation, the subvolumes are not all the same size. a better solution should be found.
    as small volumes tend to have a higher distribution, the minimal size of the subvolumes which are included in the evaluation is defined by minweight.
    
    For a quick revaluation, all subvolumes are saved in as .csv files.
        
    
    """
        
    G = dataset.networkdict[networkname]
    if maskname is None:
        mask = None
    else:
        mask = dataset.imgdict[maskname]
    if cellname is None:
        cells = None
    else:
        cells = dataset.imgdict[cellname]
    rho_all, u, edges = tinanet.get_inhomogenity(
        G=G, shape=dataset.shape, mask=mask, cells=cells, V=V, origin=dataset.origin, spacing=dataset.spacing,prefix=os.path.join(dataset.resfolder,'inhomo_'+networkname+'_'))
    
    u = ma.array(u, mask=u <= minweight)
    rho_all = ma.array(rho_all, mask=u.mask)
    rho_mean = ma.average(rho_all, weights=u)
    std = ma.sum(((rho_all - rho_mean)**2 * u) / (u.sum() - u.mean()))**.5
    dataset.quantified['rho_sub' + networkname] = rho_mean
    dataset.quantified['std_sub' + networkname] = std
    plt.figure()
    plt.hist(rho_all[u.mask == False].ravel(), weights=u[
             u.mask == False].ravel(), bins=30)
    plt.savefig(os.path.join(dataset.resfolder,
                             'histall_' + networkname + '.png'))
    dataset.status['inhomogeneity_analysis'][networkname]={'params':{'networkname':networkname, 'maskname':maskname, 'cellname':cellname, 'V':V, 'minweight':minweight}}


def save_cell_results(dataset, folder=None, fname='cells.csv'):
    if folder is None:
        folder = dataset.skelfolder
    tinacell.export_celldict(dataset.celldict, folder, fname)
    dataset.status['celldict_exported'] = os.path.join(folder, fname)


def save_quantified(dataset, folder=None, fname='quantified.csv'):
    if 'dataname' not in dataset.quantified.keys():
        set_dataname(dataset)
    if 'samplename' not in dataset.quantified.keys():
        set_samplename(dataset)
    if folder is None:
        folder = dataset.resfolder

    s1 = pandas.Series(dataset.quantified)
    df = pandas.DataFrame(s1)
    df.to_csv(os.path.join(folder, fname))
    dataset.status['quantified_saved'] = os.path.join(folder, fname)


def export_metadata(dataset, folder=None, fname='metadata.csv'):
    if folder is None:
        folder = dataset.resfolder
    statusc = dataset.status.copy()
    repeat = True
    while repeat:
        repeat = False
        for key in statusc.keys():
            if isinstance(statusc[key], type({})):
                dic = statusc.pop(key)
                for k2 in dic.keys():
                    d = dic[k2]
                    if isinstance(d, type({})):
                        repeat = True
                    statusc[key + '_' + k2] = d
    dataset.status['metadata_exported'] = os.path.join(folder, fname)

    s1 = pandas.Series(statusc)
    df = pandas.DataFrame(s1)
    df.to_csv(os.path.join(folder, fname))


def simple_compare_networks(results, x_positions=None, xlabel=None, xticks=None, folder=None, dataset=None, figures=None):
    if x_positions is None:
        x_positions = range(len(results))
    if xlabel is None:
        xlabel = ''
    if folder is None and dataset:
        folder = dataset.resfolder

    LL = []
    beta_w = []
    dbeta_w = []
    beta_d = []
    dbeta_d = []
    ccl = []

    for res in results:
        epopt, epcov, npopt, npcov, L, ccm = res
        LL.append(L)
        beta_w.append(epopt[0])
        dbeta_w.append(np.sqrt(epcov[0, 0]))
        beta_d.append(npopt[0])
        dbeta_d.append(np.sqrt(epcov[0, 0]))
        ccl.append(ccm)
    # weight decay histograms
    plt.figure(figures[0])
    plt.title('weight decay')
    plt.savefig(os.path.join(folder, 'filtered' + 'edgeweight.png'))
    plt.savefig(os.path.join(folder, 'filtered' + 'edgeweight.pdf'))

    # degree decay histograms
    plt.figure(figures[1])
    plt.title('degree decay')
    plt.savefig(os.path.join(folder, 'filtered' + 'nodeweight.png'))
    plt.savefig(os.path.join(folder, 'filtered' + 'nodeweight.pdf'))

    plt.figure()
    plt.plot(x_positions, LL)
    plt.ylabel('total length')
    plt.xlabel(xlabel)
    plt.ylim([0, plt.ylim()[1] * 1.1])
    if xticks:
        plt.xticks(x_positions, xticks)
    plt.savefig((os.path.join(folder, 'length.png')))
    plt.savefig((os.path.join(folder, 'length.pdf')))

    plt.figure()
    plt.plot(x_positions, ccl)
    plt.ylabel('average cluster coefficient')
    plt.xlabel(xlabel)
    if xticks:
        plt.xticks(x_positions, xticks)
    plt.savefig((os.path.join(folder, 'cluster.png')))
    plt.savefig((os.path.join(folder, 'cluster.pdf')))

    plt.figure()
    plt.errorbar(x_positions, beta_w, yerr=dbeta_w)
    plt.ylabel('weight decay')
    plt.xlabel(xlabel)
    if xticks:
        plt.xticks(x_positions, xticks)
    plt.savefig((os.path.join(folder, 'weight.png')))
    plt.savefig((os.path.join(folder, 'weight.pdf')))

    plt.figure()
    plt.errorbar(x_positions, beta_d, yerr=dbeta_d)
    plt.ylabel('degree decay')
    plt.xlabel(xlabel)
    if xticks:
        plt.xticks(x_positions, xticks)
    plt.savefig((os.path.join(folder, 'degree.png')))
    plt.savefig((os.path.join(folder, 'degree.pdf')))


def export_network_data(dataset, name, nodekeys=None, edgekeys=None, folder=None, fmt='%.18e', delimiter=',', newline='\n', node_name='node', whole_name='whole', seg_name='seg'):
    if folder is None:
        folder = dataset.skelfolder

    node_name = node_name + '_' + name + '.csv'
    whole_name = whole_name + '_' + name + '.csv'
    seg_name = seg_name + '_' + name + '.csv'
    whole_dict, seg_dict = tinanet.get_edge_properties(
        dataset.networkdict[name], edgekeys)
    node_dict = tinanet.get_node_properties(
        dataset.networkdict[name], nodekeys)
    hn = ''
    for i, k in enumerate(node_dict.keys()):
        hn += k
        if i < (len(node_dict.keys()) - 1):
            hn += delimiter
        else:
            hn += newline
    np.savetxt(os.path.join(folder, node_name), np.array(
        node_dict.values()).T, header=hn, fmt=fmt, delimiter=delimiter, newline=newline)
    hw = ''
    for i, k in enumerate(whole_dict.keys()):
        hw += k
        if i < (len(whole_dict.keys()) - 1):
            hw += delimiter
        else:
            hw += newline

    np.savetxt(os.path.join(folder, whole_name), np.array(
        whole_dict.values()).T, header=hw, fmt=fmt, delimiter=delimiter, newline=newline)
    hs = ''
    for i, k in enumerate(seg_dict.keys()):
        hs += k
        if i < (len(seg_dict.keys()) - 1):
            hs += delimiter
        else:
            hs += newline

    np.savetxt(os.path.join(folder, seg_name), np.array(
        seg_dict.values()).T, header=hs, fmt=fmt, delimiter=delimiter, newline=newline)
    # pdb.set_trace()


def project_raw(dataset, rawname='raw', zmin=0, zmax=-1, folder=None, fname='raw_project', cmap='cubehelix'):
    if folder is None:
        folder = dataset.resfolder

    raw = dataset.imgdict[rawname][:, :, zmin:zmax]
    if not (zmin == 0 and zmax == -1):
        fname = fname + str(zmin) + '_' + str(zmax)
    fname = fname + '.jpg'
    tinaimg.project_raw(raw, os.path.join(folder, fname), cmap=cmap)


def project_raw_bin(dataset, rawname='raw', binname='bin', zmin=0, zmax=-1, folder=None, fname='raw_bin_project'):
    if folder is None:
        folder = dataset.resfolder

    raw = dataset.imgdict[rawname][:, :, zmin:zmax]
    bin = dataset.imgdict[binname][:, :, zmin:zmax]
    if not (zmin == 0 and zmax == -1):
        fname = fname + str(zmin) + '_' + str(zmax)
    fname = fname + '.jpg'
    tinaimg.project_raw_bin(raw, bin.astype(np.bool), os.path.join(folder, fname))


def project_bin_cells(dataset, binname='bin', cellname='cells', zmin=0, zmax=-1, folder=None, fname='bin_cell_project'):
    if folder is None:
        folder = dataset.resfolder

    cells = dataset.imgdict[cellname][:, :, zmin:zmax]
    bin = dataset.imgdict[binname][:, :, zmin:zmax]
    if not (zmin == 0 and zmax == -1):
        fname = fname + str(zmin) + '_' + str(zmax)
    fname = fname + '.jpg'
    tinaimg.project_bin_cells(bin, cells, os.path.join(folder, fname))


def make_projection_images(dataset, zmin=0, zmax=-1, folder=None):
    project_bin_cells(dataset, zmin=zmin, zmax=zmax, folder=folder)
    project_raw_bin(dataset, zmin=zmin, zmax=zmax, folder=folder)
    project_raw(dataset, zmin=zmin, zmax=zmax, folder=folder)


def add_tag_to_filenames(dataset, nametag, subfolders=False, update_saved_status=True):
    """
    add nametag to filenames for all files in skelfolder and resfolder, folders do not get renamed. if the nametag is aready included in the filename, the file will not be renamed
    this also renames filnames in status , 
    @param dataset: which dataset
    @param nametag: tag to add
    @param subfolder: if true, all files in all subfolders of the skelfolder will be renamed
    @param update_saved_status: replaces the previously saved status with a current one where names were updated
    """
    savefict=None
    if 'status_location' in dataset.status.keys():
        with open(dataset.status['status_location'],'r') as fi:
            savedict=cPickle.load(fi)
    lib.add_tag(dataset.skelfolder, nametag)
    lib.add_tag(dataset.resfolder, nametag)
    if subfolders:
        for folder in os.listdir(dataset.skelfolder):
            if os.path.isdir(os.path.join(dataset.skelfolder, folder)):
                if not os.path.join(dataset.skelfolder, folder) == dataset.resfolder:
                    add_tag(os.path.join(dataset.skelfolder, folder), nametag)
    dataset.status['tagged'] = nametag
    
    for network in dataset.status['networks']:
        if 'saved' in dataset.status['networks'][network].keys():
            s=dataset.status['networks'][network]['saved']
            if not nametag in os.path.split(s)[-1]:
                dataset.status['networks'][network]['saved'] = s.replace('.pc',nametag+'.pc')
                
        if 'celldict_saved' in dataset.status.keys():
            s=dataset.status['celldict_saved']
            if not nametag in os.path.split(s)[-1]:
                dataset.status['celldict_saved'] = s.replace('.pc',nametag+'.pc')
                
        if 'quantified_saved' in dataset.status.keys():
            s=dataset.status['quantified_saved']
            if not nametag in os.path.split(s)[-1]:
                dataset.status['quantified_saved']=s.replace('.',nametag+'.')
                
        if 'metadata_exported' in dataset.status.keys():
            s=dataset.status['metadata_exported']
            if not nametag in os.path.split(s)[-1]:
                s2=s.replace('.',nametag+'.')
                dataset.status['metadata_exported']=s2
    
    if savedict and update_saved_status:
         os.remove(dataset.status['status_location'])
         #s=dataset.status['status_location']
         #if not nametag in os.path.split(s)[-1]:
         #   dataset.status['status_location'] = s.replace('.',nametag+'.')
         #savedict['status_location'] = dataset.status['status_location'] 
         savedict['status'] = dataset.status
    with open(dataset.status['status_location'], 'wb') as fi:
        cPickle.dump(savedict, fi)
            
if visualization:

    import tinaviz as viz

    def plot_iso(dataset, name='raw', extent=None, contours=None, blurr=0, figure=None, **kwargs):
        """
        Function to visualize imagedata assigned to dataset by using an isosurface.
        if data is to be plotted for the first time the mayavi pipeline is initialized
        @param dataset
        @param name of data to visualize
        @param extent extent of the volume to be visualized (the complete image is added to the datasource
        @param contours a list of contour values e.g. [150] if None is provided a automatically calculated value is used
        @param blurr a gaussion blur can be applied to smoothen the data a little and make the visualizaton mor smooth
         @param figure when this function is called the first time dataset.__init_visual__(figure)  is called, figure should be provided if two datasets needs to be plotted in one figure
         @param **kwargs arguments passed to mlab.pipeline.surface
        """

        if not dataset.fig:
            dataset.__init_visual__(figure)
        if not dataset.vtkdata:

            i = viz.add_dataset(dataset, name, count=0)
        else:
            names = []
            for m in range(dataset.vtkdata.data.point_data.number_of_arrays):
                names += dataset.vtkdata.data.point_data.get_array_name(m)

            if name not in names:
                i = viz.add_dataset(dataset, name, count=1)
        da, surf = viz.plot_iso(
            dataset.vtkdata, name, extent=extent, contours=contours, blurr=blurr, **kwargs)
        da.parent.name = name
        return da, surf

    def plot_graph(dataset, name, node_size=1, node_color=(1, 1, 1), scale_node_keyword=None,
                   node_color_keyword=None,
                   edge_color_keyword=None, edge_radius_keyword=None,
                   edge_color=(0, 0, 0), edge_colormap='jet',
                   tube_radius=.4, figure=None, **kwargs):
        """
     Function to visualize imagedata assigned to dataset by using an isosurface.
        if data is to be plotted for the first time the mayavi pipeline is initialized
        @param dataset
        @param name of network to visualize
        @param node_size
        @param node_color
        @param scale_node_keyword='distance_to_node','degree' scales nodes with many neighbours bigger

        @param edge_color_keyword={None,'distance_to_nearest_cell','growthproj'}
            None, use edgecolor,
            #'distance_to_nearest_cell' each edge same color,
            'growthproj' color coresponds do projection of dendrite and growthfront
            'straightness'
            'distance_to_node'
        @param edge_radius_keyword
        @param node_color_keyword{'distance_to_node',distance_closest_cell}
         """
        if not dataset.fig:
            dataset.__init_visual__(figure)
        G = dataset.networkdict[name]
        etube, npts = viz.plot_graph(dataset, G, node_size=node_size, node_color=node_color, scale_node_keyword=scale_node_keyword,
                                     node_color_keyword=node_color_keyword,
                                     edge_color_keyword=edge_color_keyword, edge_radius_keyword=edge_radius_keyword,
                                     edge_color=edge_color, edge_colormap=edge_colormap,
                                     tube_radius=tube_radius, figure=figure, **kwargs)
        dataset.etubes.append(etube)
        etube.parent.parent.parent.parent.parent.name = name + ' edges source'
        dataset.nptsd[name] = npts
        npts.parent.parent.name = name + ' node source'
        # vtkdata
        # modified
