# -*- coding: utf-8 -*-
# author: Felix Repp felix.repp@mpikg.mpg.de
# !/usr/bin/env python
import os
import numpy as np
import pdb
import shutil

import tinaui

__version__ = '0.5.2'
# import tina
"""\mainpage
\section Introduction
Tool for Image and Network Analysis (TINA) is a software which was  written to analyze the structure of the osteocyte lacuno canalicular networks (OLCN) in osteons. To obtain and analyze the network topology, the 3D image of the network first needs to be skeletonized, e.g. reduced to a subset of voxels which represent roughly the mid-line of the connections and preserves the topology. %This is performed by an external software. Besides the preparation of the images, TINA convert these skeletonized images into a [NetworkX](https://networkx.github.io/) structure, to allow a flexible analysis using Python. %This allows to visualize the 3D organization of the Network using [mayavi](http://mayavi.sourceforge.net/). Several quantities of the network can be calculated, visualized and exported for further analysis.

\section Origin
Of course TINA can be used to analyze different images of networks, however making %this tool more universal is still work in process. OLCN images consist of structures of different sizes. The cell network in bone consists of a dense network of thin canaliculi (in our datasets: a few voxel in diameter) with a few cell lacunae (much bigger), and further blood vessels which are even bigger and can be segmented manually. The focus of %this tool is the analysis of the structure of the canaliculi and so far only little analysis of the cell lacunae is implemented. While typically network analysis describes the connections of nodes via edges, for our questions we needed to treat cells separately. %This is reflected in the tool, but can be mostly ignored if images of more classical network (e.g. the network of Haversian canals) are analyzed.
\image html signall.jpg

\section idworkflow Work flow

\li Starting with the 3D image of the network, %this image needs to be thresholded, and cells segmented. Tools for %this as well as loading images are provided by the tinaimg module. Additional to the network images, masks are supported. All images and further data are stored in the dataset class provided by the TINA-core module.
\li The skeletonization can be performed using either [Skeleton3D](http://se.mathworks.com/matlabcentral/fileexchange/43400-skeleton3d), a matlab code developed by Philip Kollmannsberger or [thinvox](http://www.cs.princeton.edu/~min/thinvox/) by Patrick Min. In principle [any other](http://www.cs.rug.nl/svcg/Shapes/SkelBenchmark) skeletonization routine can be used, but so far only thinning algorithms were testet.
The tinaskel module is used to run the skeletonization. %This module also provides the interface the read the resulting network and convert it to a NetworkX structure
\li The network can be analyzed using the tools provided by the tinanet module
\li The cells can be analyzed using the tools provided by the tinacell module
\li A 3D visualization of images as well as network is packed into the tinavis module
\li For a user friendly usage an UI will be provided by tinaui allowing to control the above work flow.
\li tinana helps to calculate and plots histograms of network data



Documentation
===

Tina was developed on a Linux system, and most of the testing happened in this environment, it also runs under Windows.

Installation
---

TINA is basically just a bunch of python scrips which make use of other packages. You can distribute it by just copying the following folders
+ source: Here you find all the modules
+ docs: documentation striped from the source files using doxygen (this is what you are reading right now)
+ test: this folder contains some example data and data for testing
+ examples: some example scripts which use the data in test or can be adapted to your own scripts
+scripts: here you could save your own scripts, also you find some scripts here which you can use as inspiration (no data provided)
+ OCY_analysis contains the source code for the skeletonization. Minor changes from the original files from Philip Kollmannsberger have been made to pass external arguments. The file ./OCY_analysis/OCY_main_python.m is the interface to load the right data to skeletonize
+thinvox: here the binary files of thinvox are located
+ lib: a folder to put files that can not be distributed according to the BSD license
+ notebooks here you find notebooks aiming to explain TINA in more detail, as well as analyzing a collection of datasets.
You do not need to worry about the other folders for now.


In addition to these Python scripts, either matlab  or thinvox is needed for the skeletonization. If you want to use matlab, I assume you have it installed installed already including the image toolbox already.

### installing Python

TINA has only been tested with Python 2.7!

These days the easiest way of getting Python and all dependencies to run is by using [Anaconda Python](https://store.continuum.io/cshop/anaconda/) which is available for Windows, Linux and Mac. 

After installing anaconda, you still need to instal some additional libraries. 
You do this by running 
\code
conda install mayavi, networkx, scikit-image
\endcode

in a terminal. In Windows you do this in the command promt (execute cmd), or if anaconda is not the standard python on your system, you should find a special anaconda command prompt in the start menu.



Of course it should be possible to use other distributions e.g. [Enthought Canopy Express](https://store.enthought.com/) or [Python(x,y)](http://code.google.com/p/pythonxy/). Where you also need to install the libraries mentioned above.


A nice alternative to the blown up Anaconda distribution is [Miniconda](http://conda.pydata.org/miniconda.html) which comes only with some basic libraries and the package manager `conda`.


using miniconda in Linux  has previously produced the following error when  importing mayavi.mlab has previously produced the following error:
`ValueError: API 'QString' has already been set to version 1`
which can be worked around  by
\code
$ export QT_API=pyqt
$ ipython --pylab=qt
\endcode:




### To use thinvox for skeletonization

Form the [homepage](http://www.cs.princeton.edu/~min/thinvox/) "tinvox is a program that reads a 3D voxel file as produced by binvox, and applies a thinning algorithm to it. Currently it only supports the directional thinning method described by Kálmán Palágyi and Attila Kuba in Directional 3D Thinning Using 8 Subiterations, Springer-Verlag Lecture Notes in Computer Science volume 1568, pp. 325-336, 1999."

Binary files for Linux and Windows can be found on the [website] (http://www.cs.princeton.edu/~min/thinvox/). Download it and copy it to the thinvox folder. Allow execution of the program by changing permissions.
To read and write the binvox files (the ones thinvox uses) you have to download [binvox-rw-py](https://github.com/dimatura/binvox-rw-py) and put it into the lib folder.
Unfortunatly this binary links against an old library libglew1.5, for ubuntu you can download a [.deb file](https://launchpad.net/ubuntu/quantal/amd64/libglew1.5/1.5.7.is.1.5.2-1ubuntu4)
In Windows a different shared library is needed: glut32.dll.
If this library is not installed on your system, I recommend downloading freeglut binaries from [this homepage](http://www.transmissionzero.co.uk/software/freeglut-devel/) and copying the binary (32 or 64 bit) the thinvox folder and rename it to glut32.dll




Testing the Installation
----

You have to use TINA out of IPython.
For an interactive usage of the visualization, it is important to start IPython with --gui=qt attribute
\code
 ipython --gui=qt
\endcode 
from your terminal or start a corresponding command window of your windows python distribution.
The same can be achieved by typing 
\code
 %gui qt
\endcode 
within Ipython.
If you use spyder, you can set the Backend by setting
Tools -> Preferences -> IPython console -> Graphics -> Change the Backend from "Inline" to "Qt".




To manually test if the installation of the additional packages was successful and to see if the visualization works you can test
\code{.py}
import networkx
from mayavi import mlab
mlab.test_contour3d()
\endcode


There are several examples in the ./examples folder to test the installation and get familiar with its usage (see also #tinaui). The first example to run should be  `start_skeletonizatin_thinvox.py` or `start_skeletonizatin_matlab.py`.


Usage
---

TINA is not (yet) a software which offers a defined path where you chose the data and get all the results. TINA is a framework allowing to easily script such an analysis. %This allows to use it to develop complex analysis for individual data.
A standard analysis can easily be performed by adapting the examples in the ./examples directory which are also described in #tinaui. For description of the packages of TINA, see the Package tab in the menu.


For an interactive usage of Tina it is recommend to start Ipython from the source directory or, within Ipython, cd into the source directory (%this has the advantage that you can use the IPython history: cd press tab)
Scripts, such as those in the ./examples folder (your personal scripts should go to the ./scripts folder) should start with
\code{.py}
import sys
sys.path.append('../source')
\endcode


Examples can be run in IPython using
\code{.py}
cd examples #wherever your example directory is located
%run start_skeletonization_local.py
\endcode
as copy and past does not always work in terminals IPython comes with the
\code
%paste
\endcode
function to past code snippets from the clipboard

If you use an IDE like spyder, you can run code by using dedicated (play-)buttons.


Besides %this online help, IPython does not only come with code completion, which can be used to check for functions, it also allows to check the help of a particular function using the syntax
\code
function?
\endcode

For a more detailed instruction into python I recommend the following [read](http://scipy-lectures.github.io/), especially chapter 1, 2.6 and 3.4




"""
"""@package tina_main
\brief %This module provides the class handling different image images, network structures, and possible settings for visualizations, quite likely also the structure to work with a series of datasets


"""


class dataset:
    """@class dataset
%brief %This is the container of a dataset. Using tinaui the process of the analysis is tracked, and data is assigned to the right dictionaries.
when you initialize the folder within windows you can eiter user `\` or `/` as separator for the directories. if you use `\` you should use rawstrings: onion `folder=r'C:\tinafolder'` otherwise `\t` will be treated as tab.

### handling the data
All imagedatais supposed to be stored in the dataset.imgdict dictonary, networks in the dataset.networkdict dictionary.
keys for imgdict are `'raw','bin','skel','cells','mask'`. new keywords can be added as needed. The network which corresponds to the skeletonized data can ce addressed with `dataset.networkdict['initial']`, as filtering, smoothing, etc. chagnges the data instead of just adding information, it is advised (and the default setting of tinaui) to create a copy `dataset.networkdict['smooth']`
The dataset.celldict is used to store cells, including information about size, shape, orientation...
dataset.folder is the directory where the raw data is stored, and is the first thing to be assigned to the dataset.
dataset.skelfolder ist the location where the skeleton and the networks will be stored. dataset.resfolder is where the evaluated plots and exported data will be saved.
The functions set_skelfolder, set_resfolder should be used to change these locations as they create these folders in case their parent directory excists.

dataset.origin and dataset.spacing denote the relation from image indices to coordinates, dataset.extent notes if only a subvolume of the images is used. dataset.shape is the shape of the imagearrays. after adding a new image to the imagedict, dataset.set_shape(added_array.shape) should be used which raises an error if the shape of the added array differs from the preceding ones.

### keeping track of the analysis
dataset.status is used to keep a record of all the steps: how the data was loaded, which parameters were used for thresholding, skeletonization, and analysis of the network. This allows to reload a dataset using tinaui.load_status()



### visualisation
the dataset also stores a couple of information of the 3d visualization, to allow some interactive functions: dataset.fig is the figure in which the dataset is visualized, dataset.vtkdata is the data needed for the visualization of the imagedata (unfortunatly mayavi uses its own stucture).
For an interactive selection of the nodes usint dataset.activate_picker, the folloing is needed:
dataset.etubes are the tubes that visualize the edges of the network, dataset.nptsd are the node points, dataset.cell_actorsl represent the cells.
    """

    def __init__(self, folder=None, skelfolder=None, resfolder=None, remotefolder=None, dialogfolder=None, del_skelfolder=None):
        """ Constructor initializes important variables
        @param    folder a path where the data is located.
        if None is provided a file dialog is opend
        otherwise: within windows you can eiter user `\` or `/` as separator for the directories.  if you use `\` you should use rawstrings: `folder=r'C:\tinafolder'` otherwise `\t` will be treated as tab.
        @param skelfolder Path were skeleton is stored, if None, folder+'/skel' is used. to use path relative to folder, use '+skel'
        @param resfolder Path were data is stored, if None, skelfolder+'/results' is used. to use path relative to skelfolder, use '+results'
        @param remotefolder In case you use a cluster and need to copy data to a different location

        @param dialogfolder where the filedialog should open
        @param del_skelfolder If True: in case it already exists, delete skelfolder  and all files contained, if None: ask, if False: do nothing
        """

        self.imgdict = {}  # //!< here the imagedata is linked

        self.networkdict = {}  # here the network data is linked
        self.celldict = {}  # dictionary of all cells of initial Network
        self.celllist = {}  # dlist of all cells of initial Network

        self.fig = None
        self.shape = None
        self.spacing = (1, 1, 1)
        self.origin = (0, 0, 0)

        # if volume is croped out of an bigger volume %this can be used to
        # allign image and network
        self.extent = None
        self.quantified = dict()
        if folder is None:
            import Tkinter
            import tkFileDialog
            root = Tkinter.Tk()
            root.withdraw()
            if not dialogfolder:
                dialogfolder = os.getcwd()
            folder = tkFileDialog.askdirectory(
                initialdir=dialogfolder, title='choose folder with your data')

        self.folder = os.path.abspath(folder)
        self.set_skelfolder(skelfolder, del_skelfolder=del_skelfolder)
        self.set_resfolder(resfolder)

        self.remotefolder = remotefolder
        self.status = {'thresholded': None, 'skeletonized': None, 'networks': None, 'networks': {}, 'analyzed': False,
                       'cells': None, 'raw_image': None, 'mask': None, 'loaded_stacks': {}, 'saved_stacks': {}, 'simple_analysis':{},'inhomogeneity_analysis':{}, 'tinaversion': __version__}

    def set_skelfolder(self, skelfolder=None, del_skelfolder=None):
        """
        set skelfolder and check if folder exists. if not create skelfolder in case parent folder exists if it exists, delete or keep the folder dependent on 
        @param skelfolder name of the folder where skeleton, etc. is stored, if None is provided, folder+'/skel' is used.
        """
        if skelfolder is None:
            self.skelfolder = os.path.join(self.folder, 'skel', '')
        elif skelfolder[0] == '+':
            self.skelfolder = os.path.join(self.folder, skelfolder[1:], '')
        else:
            self.skelfolder = os.path.abspath(skelfolder)
        if os.path.exists(self.skelfolder):
            if del_skelfolder is None:
                while (True):
                    dels = raw_input(r'{} already exists.\n Delete existing skelfolder  and all the containing files? "'.format(self.skelfolder))
                    if dels in ['y','Y']:
                        del_skelfolder=True
                        break
                    if dels in ['n','N', '']:
                       raise Exception('Skel folder exists!')
                    print('Answer not defined, try again')
            if del_skelfolder is True:
                shutil.rmtree(self.skelfolder)
                os.mkdir(self.skelfolder)
            
        else:
            os.mkdir(self.skelfolder)
        # pdb.set_trace()

    def set_resfolder(self, resfolder=None):
        """
        set resfolder and check if folder exists. if not create resfolder in case parent folder exists
        @param resfolder name of the folder where skeleton, etc. is stored, if None is provided, self.folder+'/skel' is used.
        """
        if resfolder is None:
            self.resfolder = os.path.join(self.skelfolder, 'results', '')
        elif resfolder[0] == '+':
            self.resfolder = os.path.join(self.skelfolder, resfolder[1:], '')
        else:
            self.resfolder = os.path.abspath(resfolder)
        if not os.path.exists(self.resfolder):
            os.mkdir(self.resfolder)

    def set_shape(self, shape):
        """
        test if the added images are the same shape as all the rest
        @param shape: shape of the added image
        """
        if self.shape is None:
            self.shape = shape
            return True
        elif self.shape == shape:
            return True
        else:
            print ('trying to change shape of ocn')

            return False

    def __init_visual__(self, figure=None):
        from mayavi import mlab
        import types
        self.etubes = []
        self.nptsd = {}
        self.cell_actorsl = []
        self.vtkdata = None

        # from mayavi import mlab
        # from tvtk.api import tvtk

        if figure is None:
            self.fig = mlab.figure()
        else:
            self.fig = mlab.figure(figure)

        # these functions needs to be bound methods
        def picker_callback(self, picker):
            # pdb.set_trace()
            self.picker2 = picker
            for etube in self.etubes:
                if etube and picker.actor in etube.actor.actors:
                    print 'you picked an edge'

            for key, npts in self.nptsd.iteritems():
                if npts and picker.actor in npts.actor.actors:
                    # pdb.set_trace()
                    print 'you picked node' + str(self.networkdict[key].nodes()[self.picker2.point_id / npts.glyph.glyph_source.glyph_source.output.points.to_array().shape[0]]) + ' from network ' + key
            for cell_actors in self.cell_actorsl:
                if cell_actors and picker.actor in cell_actors.keys():
                    print 'you picked cell ' + str(cell_actors[picker.actor])

        # bind to dataset
        self.picker_callback = types.MethodType(picker_callback, self)

        def activate_picker(dataset):
            # pdb.set_trace()
            dataset.picker = dataset.fig.on_mouse_pick(self.picker_callback)
            # bind to dataset
        self.activate_picker = types.MethodType(activate_picker, self)


"""



\subsection tina_main


\subsection tinaimg
Image data comes in different file formats, %this software is supposed to load and save image slices of png and tiff maybe jpg. Raw data sometimes comes as 16bit image, which previously was not loaded correctly. further numpy allows to store an image as an .np file and matlab also has its own format, where axis might be swapped. using sample data %this test makes sure that the different filetypes produce the same result

\subsection tinaskel
Not only should be tested if matlab runs locally as well as the communication with the cluster is working, the different order of indexes of matlab and python can result in errors. the resulting network structure should be compared to the original image, maybe missing connections can be identified

\subsection tinanet
The replacement of discrete coordinates by smoothing splines as well as filtering algorithms change the network. To test if the network still corresponds to raw data  the network structure can be concerted into an image and compared to the raw data
\subsection tinacell
Besides testing if the eigenvectors are orthogonal and other values have a reasonable magnitude I wouldn't know how to test %this module

\subsection tinana
This module helps to evaluate network quantities and make plots of the data.

\subsection tinaviz
Visualizing the data will help to see if during the other steps went wrong. Mayavi also uses different offsets and order of the indices, which can go wrong

\section tinatest Testing, Examples
%This Framework comes with some sample data to test if the different modules are running as expected. These tests will help during development as well as when the Tool is set up at a new computer. in addition some of the test can be run on new data



"""
