% combine two linked nodes into one, keeping the first one
function [node,link] = combine_nodes(node,link,i,j)

% remove all links between nodes from first and second node
lk_i = find(node(i).conn == j);
lk = node(i).links(lk_i);
node(i).links(lk_i) = [];
node(i).conn(lk_i) = [];

lk2_i = find(node(j).conn == i);
lk2 = node(j).links(lk2_i);
node(j).links(lk2_i) = [];
node(j).conn(lk2_i) = [];
%stop
% assign all link voxels to first node
node(i).idx = [node(i).idx' [link(lk).point]]';

% assign all second node voxels to first node
node(i).idx = [node(i).idx' node(j).idx']';%unique([node(i).idx' node(j).idx']');
node(j).idx = unique(node(j).idx);
% assign all outgoing links of second node to first node
lk_out = node(j).links;
n_out = node(j).conn;
node(i).links = [node(i).links lk_out];
node(i).conn = [node(i).conn n_out]; 

% assign incoming links of 2nd node to first node, update links
for k=1:length(n_out)
    if(lk_out(k)>0)
        if(n_out(k)>0)
            link(lk_out(k)).n1 = n_out(k);
            link(lk_out(k)).n2 = i;
        else
            link(lk_out(k)).n2 = n_out(k);
            link(lk_out(k)).n1 = i;
        end;            
        if(n_out(k)>0)
            node(n_out(k)).conn(node(n_out(k)).conn == j) = i;
        end;
    end;        
end;
   
% remove all links from second node
node(j).conn = [];
node(j).links = [];
