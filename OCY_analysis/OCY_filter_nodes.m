function [node,link,cell] = OCY_filter_nodes(node,link,cell,cells,filterthr)
disp 'changed'
THR_DIST = filterthr; % same or shorter links will be joined to a node
THR_BRANCH = filterthr; % same or shorter branches are removed

w = size(cells,1);
l = size(cells,2);
h = size(cells,3);
% filtering: remove links from nodes
% condensation will take care of orphaned links
% reskeletonization will take care of 1-nodes and 2-nodes

wl = sum(cellfun('length',{node.links}));
wl_new = wl+1;
while(wl_new~=wl)
    wl = wl_new;   
    
    [node,link]=remove_closenodes(node,link,THR_DIST);
    [node,link,cell]=OCY_condense_network(node,link,cell);
    
     skel2 = Graph2Skel3D(node,link,w,l,h);
     [X,node,link] = Skel2Graph3D(skel2,THR_BRANCH);
     [node,link,cell] = OCY_cell_nodes(node,link,cells);
    
    % check if something has changed
    wl_new = sum(cellfun('length',{node.links}));
end;

[node,link]=remove_closenodes(node,link,THR_DIST);
[node,link,cell]=OCY_condense_network(node,link,cell);
