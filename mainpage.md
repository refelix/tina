
\mainpage 
# Introduction

Tool for Image and Network Analysis (TINA) is a software which was  written to analyze the structure of the osteocyte lacuno canalicular networks (OLCN) in osteon. To obtain and analyze the network topology, the 3D images of the network need to be skeletonized. %This is performed by an external software. Besides the preparation of the images, TINA convert these skeletonized images into a [NetworkX](https://networkx.github.io/) structure, to allow a flexible analysis using Python. %This allows to visualize the 3D organization of the Network using [mayavi](http://mayavi.sourceforge.net/). Several quantities of the network can be calculated, visualized and exported for further analysis.


# Origin 
Of course TINA can be used to analyze different images of networks, however making %this tool more universal is still work in process. The images consist of structures of different sizes. The cell network in bone consists of a dense network of thin canaliculi (a few voxel in diameter) with a few cell lacunae (much bigger) in our datasets and further blood vessels which are even bigger and can be segmented manually. Many initial parameters are adjusted for the specific data.  The focus of %this tool is the analysis of the structure of the canaliculi and so far only little analysis of the cell lacunae is implemented. While typically network analysis describes the connections of nodes via edges, for our questions we needed to treat cells separately. %This is reflected in the tool, but can be mostly ignored if images of more classical network (e.g. the network of Haversian canals) are analyzed.
\image html signall.jpg

# Work flow

\li Starting with the 3D image of the network, %this image needs to be thresholded, and cells segmented. Tools for %this as well as loading images are provided by the tinaimg module. Additional to the network images, masks are supported. All images and further data are stored in the dataset class provided by the TINA-core module.
\li The skeletonization can be performed using either [Skeleton3D](http://se.mathworks.com/matlabcentral/fileexchange/43400-skeleton3d), a matlab code developed by Philip Kollmannsberger or [thinvox](http://www.cs.princeton.edu/~min/thinvox/) by Patrick Min. In principle [any other](http://www.cs.rug.nl/svcg/Shapes/SkelBenchmark) skeletonization routine can be used, but so far only thinning algorithms were tested.
The tinaskel module is used to run the skeletonization. %This module also provides the interface the read the resulting network and convert it to a NetworkX structure
\li The network can be analyzed using the tools provided by the tinanet module
\li The cells can be analyzed using the tools provided by the tinacell module
\li A 3D visualization of images as well as network is packed into the tinavis module
\li For a user friendly usage an UI will be provided by tinaui allowing to control the above work flow. 
\li tinana helps calculates and plots histograms of network data



Documentation
===

Tina was developed on a Linux system, and most of the testing happened in this environment, it also runs under Windows.

Installation
---

TINA is basically just a bunch of python scrips which make use of other packages. You can download it (the repository) from [bitbucket](https://bitbucket.org/refelix/tina/downloads) and extract it, or use git.
Due to licences, you need to download some more things on your own. For the skeletonization you need either matlab and OCY_analysis or, as a free alternative thinvox and binvox-rw. You will find the instructions below.
To keep the repository lean, sample data needs to be downloaded seperately. The test data should be extracted to the test directory

+ source: Here you find all the modules
+ docs: documentation striped from the source files using doxygen (this is what you are reading right now)
+ test: this folder contains some example data and data for testing
+ examples: some example scripts which use the data in test or can be adapted to your own scripts
+ scripts: here you could save your own scripts, also you find some scripts here which you can use as inspiration (no data provided)
+ OCY_analysis contains the source code for the skeletonization. Minor changes from the original files from Philip Kollmannsberger have been made to pass external arguments. The file ./OCY_analysis/OCY_main_python.m is the interface to load the right data to skeletonize
+thinvox: here the binary files of thinvox are located
+ lib: a folder to put files that can not be distributed according to the BSD license
You do not need to worry about the other folders for now.


In addition to these Python scripts, either matlab  or thinvox is needed for the skeletonization. It is assumed matlab has  been installed already.


### installing Python
So far Tina has only been tested with Python 2.7.
These days the easiest way of getting Python and all dependencies to run is by using [Anaconda Python](https://store.continuum.io/cshop/anaconda/) which is available for Windows, Linux and Mac. Of course it should be possible to use other distributions e.g. [Enthought Canopy Express](https://store.enthought.com/) or [Python(x,y)](http://code.google.com/p/pythonxy/).  

A nice alternative to the blown up Anaconda distribution is [Miniconda](http://conda.pydata.org/miniconda.html) which comes only with some basic libraries and the package manager `conda`.
Using the last option importing mayavi.mlab has previously produced the following error:
`ValueError: API 'QString' has already been set to version 1`
which can be worked around  by 
\code
$ export QT_API=pyqt
$ ipython --pylab=qt
\endcode



###Linux
using Debian or Fedora, the following packages need to be installed:
\code
sudo apt-get install python2.7 ipython python-numpy python-scipy python-matplotlib python-networkx python-pandas mayavi2
\endcode


###Windows
Since there are no equally convenient installation tools available for Windows e.g. PytonXY, Enthought Canopy or Anaconda Python.


Get the Python 2.7 version of [Anaconda Python](https://store.continuum.io/cshop/anaconda/) 

install additional pakages by running 

\code
conda install mayavi, networkx, scikit-image, pandas
pip install statsmodels
\endcode


#### Alternatavly: 

You can use the free [Enthought Canopy Express](https://store.enthought.com/). After download and installation, you need to install additional libraries: In Canopy, click on tools/Package Manager and install
-   mayavi
-   networkx
-   pillow

Unfortunately by default the installation happens in the user directory and does not allow non-ascii characters within the user name. There is a workaround, also for multi user usage, but it is not straight forward. In case your username has non-ascii characters, recommend creating a new user, are try a different python distribution. 

The installation of [Python(x,y)](http://code.google.com/p/pythonxy/) unfortunately resulted in different Poblems on different Computers. If you decide to use it, make sure to additional to the standard options, during the installation, to also install the following packages:
-   ETS (this includes mayavi)
-   networkx
-   pillow

### To use thinvox for skeletonization

Form the [homepage](http://www.cs.princeton.edu/~min/thinvox/) "tinvox is a program that reads a 3D voxel file as produced by binvox, and applies a thinning algorithm to it. Currently it only supports the directional thinning method described by Kálmán Palágyi and Attila Kuba in Directional 3D Thinning Using 8 Subiterations, Springer-Verlag Lecture Notes in Computer Science volume 1568, pp. 325-336, 1999."

Binary files for Linux and Windows can be found on the [website] (http://www.cs.princeton.edu/~min/thinvox/). Download it and copy it to the thinvox folder. Allow execution of the program by changing permissions.
To read and write the binvox files (the ones thinvox uses) you have to download [binvox-rw-py](https://github.com/dimatura/binvox-rw-py) and put it into the lib folder.
Unfortunatly this binary links against an old library libglew1.5, for ubuntu you can download a [.deb file](https://launchpad.net/ubuntu/quantal/amd64/libglew1.5/1.5.7.is.1.5.2-1ubuntu4)
In Windows a different shared library is needed: glut32.dll. 
If this library is not installed on your system, I recommend downloading freeglut binaries from [this homepage](http://www.transmissionzero.co.uk/software/freeglut-devel/) and copying the binary (32 or 64 bit) the thinvox folder and rename it to glut32.dll




Testing the Installation
----

You have to use TINA out of IPython.
For an interactive usage of the visualization, it is important to start IPython with --gui=qt attribute
\code
 ipython --gui=qt
\endcode from your terminal or start a corresponding command window of your windows python distribution

In new versions of Python(x,y) starting Ipython from the pylab menu will start IPython the correct way. You should be able to use your preferred IPython console of the PythonXY Launcher, or within Spyder. In Canopy you can use either the console within the editor, or Pylab.

 
To manually test if the installation of the additional packages was successful and to see if the visualization works you can test
\code{.py}
import networkx
from mayavi import mlab
mlab.test_contour3d() 
\endcode


There are several examples in the ./examples folder to test the installation and get familiar with its usage (see also #tinaui).
The examples rely on the [testdata](https://bitbucket.org/refelix/tina/downloads). Download and extact it into the test directory within the main TINA directory



Usage
---

TINA is not (yet) a software which offers a defined path where you chose the data and get all the results. TINA is a framework allowing to easily script such an analysis. %This allows to use it to develop complex analysis for individual data.
A standard analysis can easily be performed by adapting the examples in the ./examples directory which are also described in #tinaui. For description of the packages of TINA, see the Package tab in the menu. 


For an interactive usage of Tina it is recommend to start Ipython from the source directory or, within Ipython, cd into the source directory (%this has the advantage that you can use the IPython history: cd press tab)
Scripts, such as those in the ./examples folder (your personal scripts should go to the ./scripts folder) should start with
\code{.py}
import sys
sys.path.append('../source')
\endcode


Examples can be run in IPython using
\code{.py}
cd examples #wherever your example directory is located
%run start_skeletonization_local.py
\endcode
as copy and past does not always work in terminals IPython comes with the 
\code
%paste
\endcode
function to past code snippets from the clipboard

If you use an IDE like spyder or the canopy editor, you can run code by using dedicated (play-)buttons. 
In canopy however you still need to be in the script or example folder to make the relative path working. Right clicking in the console and toggeling 'sync to editor' ensures that this is the case.


Besides %this online help, IPython does not only come with code completion, which can be used to check for functions, it also allows to check the help of a particular function using the syntax 
\code
function?
\endcode 

For a more detailed instruction into python I recommend the following [read](http://scipy-lectures.github.io/), especially chapter 1, 2.6 and 3.4


