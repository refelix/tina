var searchData=
[
  ['lim_5fregion',['lim_region',['../namespaceanalyze__ocn.html#a21bbc2265f0d11d667504b9d412cc7f4',1,'analyze_ocn']]],
  ['limit_5fvoi_5fto_5fnodes',['limit_voi_to_nodes',['../namespacetinaviz.html#ac99c5055d010314a412b2cf152e09d38',1,'tinaviz']]],
  ['lin',['lin',['../namespaceanalyze__ocn.html#a5d76b010740e50bcdbecc562feb4165f',1,'analyze_ocn']]],
  ['load_5f3dtif',['load_3dtif',['../namespacetinaimg.html#a5cff70edc72932f58bd8e5b1e8ff359f',1,'tinaimg']]],
  ['load_5finitial_5fdata',['load_initial_data',['../namespacetinaui.html#adad9fe01e99741c8252415791ee13a71',1,'tinaui']]],
  ['load_5finitialized',['load_initialized',['../namespaceanalyze__ocn.html#a05c9db5ea2b7000693344fa216a82039',1,'analyze_ocn']]],
  ['load_5fskel_5fthinvox',['load_skel_thinvox',['../namespacetinaskel.html#af72af02990bd7ac93d671350f606b0c1',1,'tinaskel']]],
  ['load_5fstack',['load_stack',['../namespacetinaimg.html#acf40dc3f6729527b066cfa218e1e5732',1,'tinaimg.load_stack()'],['../namespacetinaui.html#af8fd7aebefc5ab9eda741602323137d4',1,'tinaui.load_stack()']]],
  ['load_5fstatus',['load_status',['../namespacetinaui.html#a6174d9e4d2990879f202fd078608fcdf',1,'tinaui']]]
];
