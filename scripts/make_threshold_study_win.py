import os
import pdb
import copy

sets=[
{'folder':r'M:\BM\AndreasRoschger\FM-TH-OPTIMIERUNG\FM04C-A01\FM04C-A01-fluo','zmin':59,'zmax':200},
{'folder':r'M:\BM\AndreasRoschger\FM-TH-OPTIMIERUNG\FM04C-A05\FM04C-A05-fluo','zmin':40,'zmax':160},
{'folder':r'M:\BM\AndreasRoschger\FM-TH-OPTIMIERUNG\FM38-A01\FM38-A1-fluo','zmin':18,'zmax':195},
{'folder':r'M:\BM\AndreasRoschger\FM-TH-OPTIMIERUNG\FM38-A05\FM38-A5-fluo','zmin':17,'zmax':150},
{'folder':r'M:\BM\AndreasRoschger\FM-TH-OPTIMIERUNG\FM40-A01\FM40-A01-fluo','zmin':27,'zmax':155},
{'folder':r'M:\BM\AndreasRoschger\FM-TH-OPTIMIERUNG\FM40-A05\FM40-A05-fluo','zmin':38,'zmax':139},
{'folder':r'M:\BM\AndreasRoschger\FM-TH-OPTIMIERUNG\FM48-A01\FM48-A01-fluo','zmin':28,'zmax':180},
{'folder':r'M:\BM\AndreasRoschger\FM-TH-OPTIMIERUNG\FM48-A05\FM48-A05-fluo','zmin':46,'zmax':177},
]

params={"felix1":{'closing_iterations': 1,
 'dilation': 1,
 'erosion': 2,
 'minsize': 450,
 'ratio': False,
 'remove': True,
 'remove_thr': 0.145,
 's1': 1.2,
 's2': 1.5,
 'sc1': 3.7,
 'sc2': 8.3,
 'thin_axis': 2,
 'thin_section': 1,
 'thr': 2.0,
 'thrc': 21.4},

"felix2":{'closing_iterations': 1,
 'dilation': 2,
 'erosion': 3,
 'minsize': 250,
 'ratio': False,
 'remove': True,
 'remove_thr': 0.161,
 's1': 0.8,
 's2': 1.2,
 'sc1': 4.2,
 'sc2': 13.1,
 'thin_axis': 2,
 'thin_section': 1,
 'thr': 3.8,
 'thrc': 21.0},
 
"andreas1":{'closing_iterations': 1,
'dilation': 1,
'erosion': 3,
'minsize': 250,
'ratio': False,
'remove': True,
'remove_thr': 0.288,
's1': 0.6,
's2': 1.2,
'sc1': 8.0,
'sc2': 12.0,
'thin_axis': 2,
'thin_section': 1,
'thr': 10.0,
'thrc': 20.0},
 
'andreas2':{'closing_iterations': 1,
'dilation': 1,
'erosion': 4,
'minsize': 250,
'ratio': False,
'remove': True,
'remove_thr': 0.288,
's1': 0.6,
's2': 1.2,
'sc1': 8.0,
'sc2': 12.0,
'thin_axis': 2,
'thin_section': 1,
'thr': 14.0,
'thrc': 15.0}
 }

for set in  sets:
    nametag=set['folder'].split('\\')[-1].rsplit('-',1)[0]
    p2=copy.deepcopy(params)
    #pdb.set_trace()
    for key,par in p2.iteritems():
        
        with open(r'U:\TINA\scripts\analyze_threshold_study.py','r') as source:
            fin=os.path.join(r'U:\TINA\scripts\FA_threshold_study','analyze_threshold_study_'+nametag+key+'.py')
            if os.path.exists(fin):
                os.remove(fin)
            with open(fin,'w') as fil:
                fil.write('folder = "'+set['folder']+'"\n')
                fil.write('zmin = '+str(set['zmin'])+'\n')
                fil.write('zmax = '+str(set['zmax'])+'\n')
                fil.write('nametag = "'+nametag+'"\n')
                fil.write('thrtag = "'+key+'"\n')
                fil.write('params = {')
                #pdb.set_trace()
                while len(par.keys()):
                    #pdb.set_trace()
                    k2,p=par.popitem()
                    
                    fil.write('"'+k2+'" : '+str(p))
                    if len(par.keys()):
                        fil.write(',\n')
                    else:
                        fil.write('}\n')
               
                for l in source.readlines():
                    fil.write(l)
                    
print 'done'            
        

