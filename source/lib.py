import numpy as np
import pdb
from PIL import Image
import scipy.io as io
import os
import scipy.optimize as op
import copy
import shutil
import cPickle


def get_index_from_pos(dataset, x, y, z):
    xi = ((np.array(x) - dataset.origin[0]) /
          dataset.spacing[0]).astype(np.int).ravel()
    yi = ((np.array(y) - dataset.origin[1]) /
          dataset.spacing[1]).astype(np.int).ravel()
    zi = ((np.array(z) - dataset.origin[2]) /
          dataset.spacing[2]).astype(np.int).ravel()
    xi[xi < 0] = 0
    yi[yi < 0] = 0
    zi[zi < 0] = 0
    xi[xi >= dataset.shape[0]] = dataset.shape[0] - 1
    yi[yi >= dataset.shape[1]] = dataset.shape[1] - 1
    zi[zi >= dataset.shape[2]] = dataset.shape[2] - 1

    return xi, yi, zi


def graph_to_skel(dataset, n, cells='cells'):

    skel = np.zeros(dataset.shape, dtype=bool)
    x = []
    y = []
    z = []
    for ed in n.edges(data=True):
        x += ed[2]['x'].tolist()
        y += ed[2]['y'].tolist()
        z += ed[2]['z'].tolist()

    x = np.array(x)
    y = np.array(y)
    z = np.array(z)
    xi, yi, zi = get_index_from_pos(dataset, x, y, z)

    skel[xi.astype(int), yi.astype(int), zi.astype(int)] = 1
    if not cells == False:

        skel[dataset.imgdict[cells]] = 1
    return skel

def get_polar(X,Y,phi0=0,pos_angles=True):
    RHO= np.sqrt(X**2+Y**2)
    PHI= np.arctan2(Y,X)-phi0
    
    if not (pos_angles):
        PHI[PHI>np.pi]-=2*np.pi
        PHI[PHI<-np.pi]+=2*np.pi
    else:
        while np.sum(PHI<0):
            PHI[PHI<0]+=2*np.pi
            PHI[PHI>2*np.pi]-=2*np.pi
    return [RHO,PHI]


def get_point_of_lowest_distancevariation(x, y):
    x0 = (min(x) + max(x)) / 2
    y0 = (min(y) + max(y)) / 2
    r = np.array([x, y])
    p0 = [x0, y0]
    popt = op.fmin_bfgs(lambda r0: (
        ((r[0] - r0[0])**2 + (r[1] - r0[1])**2)**.5).std(), p0, full_output=1)
    print popt[6]
    if popt[6] == 2:
        pdb.set_trace()
    d = ((r[0] - popt[0][0])**2 + (r[1] - popt[0][1])**2)**.5

    # op.fmin(lambda r0:((r[0]-r0[0])**2+(r[1]-r0[1])**2).std(),p0)
    return popt[0], d.mean(), d.std()


def gaussp(r, r0, a, d):
    return np.abs(a) * np.exp(-(r - r0)**2 / (2 * d**2))


def cyl_to_cat(r, phi, x0, y0, phi0=0):
    x = r * np.cos(phi + phi0) + x0
    y = r * np.sin(phi + phi0) + y0
    return x, y


def sphere_to_cat(r, phi, theta, r0=np.array([0, 0, 0])):
    x = r * sin(phi) * cos(theta) + r0[0]
    y = r * cos(phi) + r0[1]
    z = r * sin(phi) * sin(theta) + r0[2]
    return x, y, z


def histogramdd(sample, bins=10, rrange=None, normed=False, weights=None, parts=1):
    try:
        # Sample is an ND-array.
        N, D = sample.shape
    except (AttributeError, ValueError):
        # Sample is a sequence of 1D arrays.
        sample = np.atleast_2d(sample).T
        N, D = sample.shape
    if parts == 1:
        w = np.histogramdd(sample, bins, rrange, normed, weights)
    else:

        s = np.linspace(0, sample.shape[0], parts + 1).astype(np.int)
        for i in range(parts):
            print i
            if weights == None:
                wh = np.histogramdd(
                    sample[s[i]:s[i + 1]], bins, rrange, normed)
            else:
                wh = np.histogramdd(
                    sample[s[i]:s[i + 1]], bins, rrange, normed, weights[s[i]:s[i + 1]])
            if i == 0:
                w = wh
            else:
                w = (w[0] + wh[0], w[1])

    return w


def add_tag(folder, tag):

    for fi in os.listdir(folder):
        if os.path.isdir(os.path.join(folder, fi)) == False:
            if tag in fi:
                continue
            if '.binvox' in fi:
                continue
            sfi = fi.split('.')
            nfi = sfi[0] + tag + '.' + sfi[-1]
            os.rename(os.path.join(folder, fi), os.path.join(folder, nfi))


def copy_res_folder(src, dst, key='', celldict=False):
    allstatus = {}
    if os.path.exists(dst):
        if raw_input('delete ' + dst) == 'y':
            shutil.rmtree(dst)
    os.mkdir(dst)
    for directory, dirnames, filenames in os.walk(src):
        for fin in filenames:
            if '.tin' in fin and key in fin:
                allstatus[os.path.join(directory, fin)]=directory
    i=0
    #pdb.set_trace()
    for statusn in allstatus.keys():
        with open(statusn, 'rb') as statusf:
            savedict= cPickle.load(statusf)
        
        os.mkdir(os.path.join(dst,str(i)))
        
        
        shutil.copytree(savedict['resfolder'], os.path.join(dst,str(i),os.path.split(savedict['resfolder'][:-1])[1]))
        shutil.copyfile(savedict['status_location'], os.path.join(dst,str(i),'status.tin'))
        if celldict:
            shutil.copyfile(savedict['status']['celldict_saved'], os.path.join(dst,str(i),'celldict.pc'))
          
        i+=1
        


def copy_res_folder2(src, dst, key='results'):
    allresults = []
    if os.path.exists(dst):
        if raw_input('delete ' + dst) == 'y':
            shutil.rmtree(dst)
    os.mkdir(dst)
    for directory, dirnames, filenames in os.walk(src):
        # print dirnames
        for fin in dirnames:
            if key in fin:
                # pdb.set_trace()
                shutil.copytree(os.path.join(directory, fin),
                                os.path.join(dst, fin))


"""
import warnings
import types
import sys
import numpy.core.numeric as _nx
from numpy.core import linspace
from numpy.core.numeric import ones, zeros, arange, concatenate, array, \
        asarray, asanyarray, empty, empty_like, ndarray, around
from numpy.core.numeric import ScalarType, dot, where, newaxis, intp, \
        integer, isscalar
from numpy.core.umath import pi, multiply, add, arctan2,  \
        frompyfunc, isnan, cos, less_equal, sqrt, sin, mod, exp, log10
from numpy.core.fromnumeric import ravel, nonzero, choose, sort, mean
from numpy.core.numerictypes import typecodes, number
from numpy.core import atleast_1d, atleast_2d
from numpy.lib.twodim_base import diag
#from _compiled_base import _insert, add_docstring
#from _compiled_base import digitize, bincount, interp as compiled_interp
#from arraysetops import setdiff1d
#from utils import deprecate
import numpy as np
def histogramdd(sample, bins=10, range=None, normed=False, weights=None):
    """
"""
    Compute the multidimensional histogram of some data.

    Parameters
    ----------
    sample : array_like
        The data to be histogrammed. It must be an (N,D) array or data
        that can be converted to such. The rows of the resulting array
        are the coordinates of points in a D dimensional polytope.
    bins : sequence or int, optional
        The bin specification:

        * A sequence of arrays describing the bin edges along each dimension.
        * The number of bins for each dimension (nx, ny, ... =bins)
        * The number of bins for all dimensions (nx=ny=...=bins).

    range : sequence, optional
        A sequence of lower and upper bin edges to be used if the edges are
        not given explicitely in `bins`. Defaults to the minimum and maximum
        values along each dimension.
    normed : bool, optional
        If False, returns the number of samples in each bin. If True, returns
        the bin density, ie, the bin count divided by the bin hypervolume.
    weights : array_like (N,), optional
        An array of values `w_i` weighing each sample `(x_i, y_i, z_i, ...)`.
        Weights are normalized to 1 if normed is True. If normed is False, the
        values of the returned histogram are equal to the sum of the weights
        belonging to the samples falling into each bin.

    Returns
    -------
    H : ndarray
        The multidimensional histogram of sample x. See normed and weights for
        the different possible semantics.
    edges : list
        A list of D arrays describing the bin edges for each dimension.

    See Also
    --------
    histogram: 1-D histogram
    histogram2d: 2-D histogram

    Examples
    --------
    >>> r = np.random.randn(100,3)
    >>> H, edges = np.histogramdd(r, bins = (5, 8, 4))
    >>> H.shape, edges[0].size, edges[1].size, edges[2].size
    ((5, 8, 4), 6, 9, 5)

    """
"""
    #pdb.set_trace()
    try:
        # Sample is an ND-array.
        N, D = sample.shape
    except (AttributeError, ValueError):
        # Sample is a sequence of 1D arrays.
        sample = atleast_2d(sample).T
        N, D = sample.shape

    nbin = empty(D, int)
    edges = D*[None]
    dedges = D*[None]
    if weights is not None:
        weights = asarray(weights)

    try:
        M = len(bins)
        if M != D:
            raise AttributeError(
                    'The dimension of bins must be equal'\
                    ' to the dimension of the sample x.')
    except TypeError:
        # bins is an integer
        bins = D*[bins]

    # Select range for each dimension
    # Used only if number of bins is given.
    if range is None:
        # Handle empty input. Range can't be determined in that case, use 0-1.
        if N == 0:
            smin = zeros(D)
            smax = ones(D)
        else:
            smin = atleast_1d(array(sample.min(0), float))
            smax = atleast_1d(array(sample.max(0), float))
    else:
        smin = zeros(D)
        smax = zeros(D)
        for i in arange(D):
            smin[i], smax[i] = range[i]

    # Make sure the bins have a finite width.
    for i in arange(len(smin)):
        if smin[i] == smax[i]:
            smin[i] = smin[i] - .5
            smax[i] = smax[i] + .5

    # Create edge arrays
    for i in arange(D):
        if isscalar(bins[i]):
            if bins[i] < 1:
                raise ValueError("Element at index %s in `bins` should be "
                                 "a positive integer." % i)
            nbin[i] = bins[i] + 2 # +2 for outlier bins
            edges[i] = linspace(smin[i], smax[i], nbin[i]-1)
        else:
            edges[i] = asarray(bins[i], float)
            nbin[i] = len(edges[i])+1  # +1 for outlier bins
        dedges[i] = np.diff(edges[i])
        if np.any(np.asarray(dedges[i]) <= 0):
            raise ValueError("""
"""
            Found bin edge of size <= 0. Did you specify `bins` with
            non-monotonic sequence?"""
""")

    nbin =  asarray(nbin)

    # Handle empty input.
    if N == 0:
        return np.zeros(nbin-2), edges

    # Compute the bin number each sample falls into.
    Ncount = {}
    for i in arange(D):
        Ncount[i] = np.digitize(sample[:,i], edges[i])
        mm=max(Ncount[i])
        #pdb.set_trace()
        if np.log2(mm)< 8:
            Ncount[i]=Ncount[i].astype(np.uint8)
        
    # Using digitize, values that fall on an edge are put in the right bin.
    # For the rightmost bin, we want values equal to the right
    # edge to be counted in the last bin, and not as an outlier.
    outliers = zeros(N, int)
    for i in arange(D):
        # Rounding precision
        mindiff = dedges[i].min()
        if not np.isinf(mindiff):
            decimal = int(-log10(mindiff)) + 6
            # Find which points are on the rightmost edge.
            on_edge = where(around(sample[:,i], decimal) == around(edges[i][-1],
                                                                   decimal))[0]
            # Shift these points one bin to the left.
            Ncount[i][on_edge] -= 1

    # Flattened histogram matrix (1D)
    # Reshape is used so that overlarge arrays
    # will raise an error.
    hist = zeros(nbin, float).reshape(-1)

    # Compute the sample indices in the flattened histogram matrix.
    ni = nbin.argsort()
    shape = []
    xy = zeros(N, int)
    for i in arange(0, D-1):
        xy += Ncount[ni[i]] * nbin[ni[i+1:]].prod()
    xy += Ncount[ni[-1]]

    # Compute the number of repetitions in xy and assign it to the
    # flattened histmat.
    if len(xy) == 0:
        return zeros(nbin-2, int), edges

    flatcount = np.bincount(xy, weights)
    a = arange(len(flatcount))
    hist[a] = flatcount

    # Shape into a proper matrix
    hist = hist.reshape(sort(nbin))
    for i in arange(nbin.size):
        j = ni.argsort()[i]
        hist = hist.swapaxes(i,j)
        ni[i],ni[j] = ni[j],ni[i]

    # Remove outliers (indices 0 and -1 for each dimension).
    core = D*[slice(1,-1)]
    hist = hist[core]

    # Normalize if normed is True
    if normed:
        s = hist.sum()
        for i in arange(D):
            shape = ones(D, int)
            shape[i] = nbin[i] - 2
            hist = hist / dedges[i].reshape(shape)
        hist /= s

    if (hist.shape != nbin - 2).any():
        raise RuntimeError(
                "Internal Shape Error")
    return hist, edges
"""
