
#import

import os
os.chdir('..')
import sys
sys.path.append('../source')
sys.path.append('../lib')
from tina_main import dataset
from matplotlib import pyplot as plt # for visualisation of imagedata
import networkx as nx
import tinaui
import tinaui as ui
import numpy as np



t1= dataset (folder=folder,skelfolder='+0710thrstudy_skel_'+nametag+'_'+thrtag,resfolder='+results'+nametag+'_'+thrtag) #initialize the dataset
tinaui.load_initial_data(t1,ftype='tif',nfilter=None,xml=True,extent=[0,1024,0,1024,zmin,zmax]) #load images with smaller extent, using the information from .xml-files  for spacing of the image data
t1.quantified['nametag']=nametag
t1.status['nametag']=nametag
t1.quantified['thrtag']=thrtag
t1.status['thrtag']=thrtag
ui.load_stack(t1,folder=t1.folder.split('-fluo')[0]+'-mask',name='mask', ftype='png',extent=t1.extent,dtype=np.uint8)

tinaui.adaptive_thresholding(t1,**params)
tinaui.find_cells(t1,dist_tr=4)
t1.imgdict['bin']*=t1.imgdict['mask']==1
tinaui.skeletonize_thinvox_local(t1)
tinaui.get_graph_local(t1)
#n=t1.networkdict['initial']
#import tinanet
#tinanet.merge_short_edges(n,1)


#
ui.replaceEdgesBySplines(t1,name='initial',copy='smooth')
ui.simple_network_analysis(t1,'initial')

ui.analyze_inhomogenity(t1,'smooth',maskname='mask',V=400,minweight=350)
ui.analyze_cells(t1,'initial')
ui.save_quantified(t1)
ui.export_network_data(t1,'initial')


ui.make_projection_images(t1)
ui.make_projection_images(t1,zmax=t1.shape[-1]/3)
ui.make_projection_images(t1,zmin=t1.shape[-1]/3,zmax=2*t1.shape[-1]/3)
ui.make_projection_images(t1,zmin=2*t1.shape[-1]/3)
ui.save_status(t1,filename='status_'+t1.skelfolder.split('\\')[-1]+'.tin',save_images=['bin','cells','skel'])
ui.add_tag_to_filenames(t1,'_'+nametag+'_'+thrtag, subfolders=False)
print 'done'
