import sys
sys.path.append('../source')
from tina_main import dataset
from matplotlib import pyplot as plt # for visualisation of imagedata
import networkx as nx
import tinaui
import tinaui as ui
import numpy as np
import os

plt.ion()
t1= dataset (folder='../test/sampledata/ocn',skelfolder='+skelall') #initialize the dataset
tinaui.load_initial_data(t1,ftype='tif',nfilter=['ch','2.'],xml=True,extent=None) #load images with smaller extent, using the information from .xml-files  for spacing of the image data
t1.origin=(0,0,0)
tinaui.adaptive_thresholding(t1)
tinaui.find_cells(t1)
tinaui.skeletonize_thinvox_local(t1)
tinaui.get_graph_local(t1)
ui.load_stack(t1,folder=os.path.join(t1.folder,'mask'),name='mask', ftype='png',extent=t1.extent,dtype=np.bool)
#n=t1.networkdict['initial']
#import tinanet
#tinanet.merge_short_edges(n,1)
ui.save_status(t1,filename='status_all')

ui.get_masked_network_edge(t1,name='initial',maskname='mask',copy='masked')
ui.replaceEdgesBySplines(t1,name='masked',copy='smooth')
ui.simple_network_analysis(t1,'smooth')

ui.analyze_inhomogenity(t1,'smooth',maskname='mask',V=400,minweight=350)
ui.analyze_cells(t1,'smooth')
ui.save_quantified(t1)
ui.export_network_data(t1,'smooth')


ui.make_projection_images(t1)
ui.make_projection_images(t1,zmax=t1.shape[-1]/3)
ui.make_projection_images(t1,zmin=t1.shape[-1]/3,zmax=2*t1.shape[-1]/3)
ui.make_projection_images(t1,zmin=2*t1.shape[-1]/3)
#ui.add_tag_to_filenames(t1,"tag", subfolders=True)
