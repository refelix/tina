import sys
sys.path.append('../source')

from tina_main import dataset
import tinaui

t1 = dataset(folder='../test/sampledata/ocn')  # initialize the dataset
# load images with smaller extent, using the information from .xml-files
# for spacing of the image data
tinaui.load_initial_data(t1, ftype='tif', nfilter=[
                         'ch', '0.'], xml=True, extent=(100, 250, 300, 500, 0, 100))

tinaui.adaptive_thresholding(t1)
tinaui.find_cells(t1)
tinaui.skeletonize_matlab(t1)
tinaui.get_graph(t1)
tinaui.save_status(t1)
