function Analyse_Osteocytes_python(filename, foldername )

diary ('matlab.log')%-------------------------------------------------------
% Filtering, automatic thresholding, segmentation and
% skeletonization of osteocyte lacuno-canalicular network
% based on confocal image stacks.
%
% Philip Kollmannsberger, last changed 02-Aug-2013
%-------------------------------------------------------

%tic

%clear all;
%close all;
dbstop if error
curdict=pwd
addpath(curdict)

addpath(strcat(curdict,'/Skeleton3D'))

addpath(strcat(curdict,'/Skel2Graph3D'))
addpath(strcat(curdict,'/bwdistsc'))
addpath(strcat(curdict,'/Plot'))
addpath(strcat(curdict,'/matlab_bgl'))
%addpath('/usr/data/nfs6/repp/TINA/OCY_analysis')
%addpath('/usr/data/nfs6/repp/TINA/OCY_analysis/Skeleton3D')
%addpath('/usr/data/nfs6/repp/TINA/OCY_analysis/Skel2Graph3D')
%addpath('/usr/data/nfs6/repp/TINA/OCY_analysis/bwdistsc')
%addpath('/usr/data/nfs6/repp/TINA/OCY_analysis/Plot')
%addpath('/usr/data/nfs6/repp/TINA/OCY_analysis/matlab_bgl')

%THR_METHOD  = 2;         % 1 = Otsu (use with ESRF images), 2 = Exponential, 3 = Topological,  

filter_nodes=0
filter_branches=4

cd(foldername)
path =foldername
cel=false
% thresholded data
load(filename);



%img=img'
if THR_METHOD  == 5
    global THR
    THR =threshold
end
bin=logical(img);


cd ([skelfolder])
w=size(bin,1);
l=size(bin,2);
h=size(bin,3);
save ('extent', 'extent')
save ('shape','w','h','l')

%stop
if(~isempty(bin))
    % fill voids
    %bin = OCY_fill_voids(bin);
    
    % Recognize cells

    
    if sum(size(cel))>2 || THR_METHOD  || 6 
            cells=cel;
    else
            
            %stop
            % Recognize cells
            cells = OCY_get_cells(bin);
    end
        
    %save cells cells
   
    disp('skel3d')
    % Compute medial axis
    skel = Skeleton3D(bin,cells);
    save skel_ini skel
    disp('skel2graph')
    % Compute network graph
    [X,node,link] = Skel2Graph3D(skel,filter_branches);
    
    % assign cells to nodes
    if sum(size(cel))>2
    [node,link,cell] = OCY_cell_nodes(node,link,cells);
    else
    cell=[];
    end;
    %stop
    % filter nodes
    if filter_nodes % filtern is threshold in voxel
        [node,link,cell] = OCY_filter_nodes(node,link,cell,cells,filter_nodes);
        disp 'filtered'
    end
    % create link matrix and assign distances
    %[node,link,cell,A] = OCY_ana_cell_links(node,link,cell,skel);
    
    % create new cleaned voxel skeleton
    skel2 = Graph2Skel3D(node,link,w,l,h);
    %stop
    save skel skel2
    save node node
    save link link
    save cell cell
    %save A A
  
    cd ..

end;
exit


