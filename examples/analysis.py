import sys
sys.path.append('../source')

from matplotlib import pyplot as plt  # for visualisation of imagedata
import networkx as nx
import tinaui as ui
import numpy as np
import os


# plt.ion()
t1 = ui.load_status(folder='../test/sampledata/ocn', load_imagedata=True)

ui.load_stack(t1, folder=os.path.join(t1.folder, 'mask'),
              name='mask', ftype='png', extent=t1.extent, dtype=np.bool)
# n=t1.networkdict['initial']
#import tinanet
# tinanet.merge_short_edges(n,1)
ui.get_masked_network_edge(t1, name='initial', maskname='mask', copy='masked')
ui.replaceEdgesBySplines(t1, name='masked', copy='smooth')
ui.simple_network_analysis(t1, 'smooth')

ui.analyze_inhomogenity(t1, 'smooth', maskname='mask', V=400, minweight=350)
ui.analyze_cells(t1, 'smooth')
ui.save_quantified(t1)
ui.export_network_data(t1, 'smooth')
save_cell_results(t1)

ui.make_projection_images(t1)
ui.make_projection_images(t1, zmax=t1.shape[-1] / 3)
ui.make_projection_images(t1, zmin=t1.shape[-1] / 3, zmax=2 * t1.shape[-1] / 3)
ui.make_projection_images(t1, zmin=2 * t1.shape[-1] / 3)
#ui.add_tag_to_filenames(t1,"tag", subfolders=True)
